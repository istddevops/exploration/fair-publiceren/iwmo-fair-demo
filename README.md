# iWmo FAIR Demo

Demo om Datagedreven en FAIR publiceren te evalueren op basis van iWmo-standaard.

Opgebouwd op basis van input uit de (legacy) BizzDesign-publicaties:

- Excel-sheets
  - Codelijsten (domeinwaarden voor datatypen)
  - Regelrapport (regels en koppeling naar bericht, klasse, element en/of datatypen)
- XSDs
  - Berichten (body, klassen, elementen en relaties tussen bericht-klassen)
  - Basisschema (datatypen)

Zie documentatie:

- Voorbereidende conversie van de data en content. Zie [documentatie Python-scripts](python/README.md).
- Templates voor de HTML-publicatie. Zie [documentatie HUGO-layouts](layouts/README.md).

Zie de [HTML-publicatie demo](https://istddevops.gitlab.io/exploration/fair-publiceren/iwmo-fair-demo) voor evaluatie-doeleinden.

