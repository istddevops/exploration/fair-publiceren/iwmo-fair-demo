/**
 * 
 * Kroki.io Client Functionility
 * 
 * Source: https://docs.kroki.io/kroki/setup/encode-diagram/#nodejs
 * 
 * NPM Package Dependecies:
 * - pako
 * - @types/pako (devDependency)
 * - buffer
 * 
 */

// Node Package Imports
import pako from 'pako';
import { Buffer } from 'buffer';

// HUGO Template Parameters
import * as params from '@params';

/**
 * Build a compressed paramater based Kroki URL for SVG
 * @param diagramType KROKI-diagram type (plantuml, ditaa, bpmn, ...)
 * @param diagramSource TEXT-source diagram
 * @returns `img src` or `object data` url
 */
export function buildKrokiUrl(diagramType: string, diagramSource: string): string {
  console.log(diagramSource);
    const data = Buffer.from(diagramSource, 'utf8');
    const compressed = pako.deflate(data, { level: 9 });
    const krokiParam = Buffer.from(compressed)
      .toString('base64') 
      .replace(/\+/g, '-').replace(/\//g, '_');
    const krokiServer = params.krokiServer ? params.krokiServer: 'https://kroki.io' ; 
    console.log('krokiServer = ' + params.krokiServer );
    return krokiServer + '/' + diagramType + '/svg/' + krokiParam;
}

/**
 * Render KROKI-diagram type
 * @param elemId 
 * @param diagramType 
 * @param diagramSource 
 * @param docsURL 
 */ 
function renderKrokiDiagram(
  elemId: string, diagramType: string, diagramSource: string, docsURL: string) {
  let parsedDiagramSource = diagramSource;

  if (diagramType == 'plantuml') {
      /* 
          Parse docsURL to make Relative Book Docs linking possible
          from generated PlantUML diagrams
      */
      parsedDiagramSource = parsedDiagramSource
          .replace(/<%= docsURL %>/g, docsURL);
  }

  const krokiUrl = buildKrokiUrl(diagramType, parsedDiagramSource);
  const outputNode = document.getElementById(elemId);
  outputNode.innerHTML = '<object data="' + krokiUrl + '"></object>';    
}

 