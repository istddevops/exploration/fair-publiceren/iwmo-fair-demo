/**
 * iStandaard Meta-Data Model Typings
 */

/**
 * iStandaard Estafette Model Bericht
 */
export interface iStdBericht {
    naam: string;
    beschrijving: string;
    klassen: IstdBerichtKlasse[];
    relaties: IstdBerichKlassetRelatie[];
    regels?: string[];
}

/**
 * iStandaard Estafette Model Bericht Klasse
 */
export interface IstdBerichtKlasse {
    naam: string;
    beschrijving: string;
    elementen: IstdBerichtKlasseElement[];
    regels?: string[];
  }

/**
 * iStandaard Estafette Model Bericht Klasse Relatie
 */
export interface IstdBerichKlassetRelatie {
    naam: string;
    parentKlasse: string;
    childKlasse: string;
    childMin: string;
    childMax: string;
}
  
/**
 * iStandaard Estafette Model Bericht Klasse Element
 */
export interface IstdBerichtKlasseElement {
    naam: string;
    beschrijving: string;
    dataType: string;
    waarde?: string;
    regels?: string[];
}