/**
 * iStandaard PlantUML Diagram Library
 */

// HUGO Template Parameters
import * as params from '@params';

// Local Imports
import {
  iStdBericht,
  IstdBerichtKlasse,
  IstdBerichKlassetRelatie,
  IstdBerichtKlasseElement
} from './istd-meta-data-model';

/**
 * Build PlantUML iStandaard Estafettemodel Bericht Diagram
 */
export function buildPlantUmliStdBerichtCmsPreviewDiagram(bericht: iStdBericht) {
  let diagram = buildPlantUmlHeader();
  diagram += plantUmlConfig();
  diagram += "package " + bericht.naam + " {\n";
  diagram += 'class "' + bericht.naam + '" as RootKlasse << Root >>\n\n';

  for (const klasse of bericht.klassen) {
    diagram += '\n' + buildPlantUmlIstdBerichtKlasse(bericht.naam, klasse);
  }

  diagram += '\n' + buildPlantUmlIstdBerichtRelaties(bericht.relaties) + '\n';
  diagram += "}\n";
  diagram += buildPlantUmlFooter();
  
  return diagram;
}

/**
 * PlantUML Header Builder
 */
export function buildPlantUmlHeader() {
    return '@startuml\n';
}

/**
 * PlantUML Rendering Settings
 */
export function plantUmlConfig() {
  let theme = '_none_';
  let skinParamHandwritten = true;

  if (params.plantUml) {
    theme = params.plantUml.theme;
    skinParamHandwritten = params.plantUml.skinparam.handwritten;
  }

  return `
!theme <%= theme %>
skinparam handwritten <%= skinParamHandwritten %>
hide methods
hide circles

`
.replace('<%= theme %>', theme)
.replace('<%= skinParamHandwritten %>', String(skinParamHandwritten));
}

/**
 * PlantUML Footer Builder
 */
export function buildPlantUmlFooter() {
  return '@enduml\n';
}

/**
 * PlantUML Package Header Builder
 */
export function buildPlantUmlPackageHeader() {
  return '@enduml\n';
}

/**
  * PlantUML iStandaard Bericht Klasse Builder
  */
export function buildPlantUmlIstdBerichtKlasse(berichtNaam: string, berichtKlasse: IstdBerichtKlasse) {
  let klasseSource = 'class "' + berichtNaam + '" ';
  klasseSource += 'as ' + berichtKlasse.naam + 'Klasse << ' + berichtKlasse.naam + 
    ' >> ' + '[[{' + berichtKlasse.beschrijving +'}]] {\n';
  klasseSource += buildPlantUmlIstdKlasseElementen(berichtKlasse.elementen);
  klasseSource += '}\n';
  return klasseSource;
}

/**
 * PlantUML iStandaard Bericht Relaties Builder
 */
export function buildPlantUmlIstdBerichtRelaties(berichtRelaties: IstdBerichKlassetRelatie[]) {
  const aggregatieTemplate = 'o-- "<%= kardinaliteit %>"';
  let relatiesSource = '';

  for (const berichtRelatie of berichtRelaties) {
    let klassenRelatie = '';

    if ((berichtRelatie.parentKlasse == 'Root') && (berichtRelatie.childKlasse == 'Header')) {
      klassenRelatie = 'HeaderKlasse "1" -o RootKlasse';
    } else {
      let kardinaliteit = berichtRelatie.childMin + ".." + berichtRelatie.childMax;
      kardinaliteit = kardinaliteit.replace('unbounded', '*');
      kardinaliteit = kardinaliteit.replace('1..1', '1');
      const aggregatieSymbool = aggregatieTemplate.replace('<%= kardinaliteit %>', kardinaliteit);
      klassenRelatie = berichtRelatie.parentKlasse + 'Klasse ' + 
        aggregatieSymbool + ' ' + berichtRelatie.childKlasse + 'Klasse';
    }

    relatiesSource += '\n' + klassenRelatie;
  }

  return relatiesSource;
}

/**
 * PlantUML iStandaard Klasse Elementen Builder
 */
export function buildPlantUmlIstdKlasseElementen(klasseElementen: IstdBerichtKlasseElement[]) {
  let elementenSource = '';

  for (const klasseElement of klasseElementen) {
    elementenSource += klasseElement.naam + ': ' + klasseElement.dataType + 
      ' [[{' + klasseElement.beschrijving +'}]]\n';
  }

  return elementenSource;
}