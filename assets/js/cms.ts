/**
 * Netlify CMS App NPM Install with HUGO JavaScript Building 
 * 
 * - https://www.netlifycms.org/docs/add-to-your-site/#installing-with-npm
 * - https://gohugo.io/hugo-pipes/js/#import-js-code-from-assets
 * - https://esbuild.github.io/content-types/#jsx
 * 
 * To use ".tsx" files add a "tsconfig.json" in the "assets/js" root directory 
 * with the "compilerOptions" settings as follows:
 *
 * - "jsx": "react",
 * - "esModuleInterop": true
 *
 */

// Import Netlify CMS to take over current page
import CMS from 'netlify-cms-app';

// Initialize the CMS object
CMS.init();

// Now the registry is available via the CMS object.
import { PreviewBerichten } from './cms-previews/cms-preview-berichten';
CMS.registerPreviewTemplate('berichten', PreviewBerichten)