/**
 * Bronnen voor op TypeScript React gebaseerde Netlify CMS Previews:
 * 
 * - https://www.netlifycms.org/docs/customization/
 * - https://beta.reactjs.org/reference/react/Component#component
 * - https://blog.logrocket.com/how-use-typescript-react-tutorial-examples/
 * - https://github.com/decaporg/decap-cms/blob/master/packages/netlify-cms-core/index.d.ts
 * 
 */

// Node Package Imports
import React from 'react';
import { PreviewTemplateComponentProps } from 'netlify-cms-core';

// Local Imports
import { iStdBericht } from '../istd-meta-data-model';
import { buildPlantUmliStdBerichtCmsPreviewDiagram } from '../istd-plantuml-diagram';
import { buildKrokiUrl } from '../kroki';

/**
 * Netlify CMS Preview React Component voor iStandaard Bericht weergave in PlantUML
 */
export class PreviewBerichten extends React.Component<PreviewTemplateComponentProps> {
  render() {
    // Get Bericht Entry Data
    const entry = this.props.entry;
    const bericht: iStdBericht = {
      naam: entry.getIn(['data', 'naam']),
      beschrijving: entry.getIn(['data', 'beschrijving']),
      klassen: [],
      relaties: []
    };

    // Get Klassen Entry Data
    for (const klasseEntry of this.props.widgetsFor('klassen')) {
      const klasseIndex = bericht.klassen.push({
        naam: klasseEntry.getIn(['data', 'naam']),
        beschrijving: klasseEntry.getIn(['data', 'beschrijving']),
        elementen: []
      }) - 1;

      // Get Klasse Elementen Entry Data
      for (const elementEntry of klasseEntry.getIn(['data', 'elementen'])) {        
        bericht.klassen[klasseIndex].elementen.push({
          naam: elementEntry.get('naam'),
          beschrijving: elementEntry.get('beschrijving'),
          dataType: elementEntry.get('dataType'),
        });
      }

    }

    // Get Klassen Entry Data
    for (const relatieEntry of this.props.widgetsFor('relaties')) {
      bericht.relaties.push({
        naam: relatieEntry.getIn(['data', 'naam']),
        parentKlasse: relatieEntry.getIn(['data', 'parentKlasse']),
        childKlasse: relatieEntry.getIn(['data', 'childKlasse']),
        childMin: relatieEntry.getIn(['data', 'childMin']),
        childMax: relatieEntry.getIn(['data', 'childMax'])
      });
    }

    // Render PlantUML Diagram
    const diagram = buildPlantUmliStdBerichtCmsPreviewDiagram(bericht);
    const krokiUrl = buildKrokiUrl('plantuml', diagram);
    
    return <object data={krokiUrl}></object>
  }
}
