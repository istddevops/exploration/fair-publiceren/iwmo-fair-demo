# Changelog

Alle belangrijke wijzigingen aan dit project zullen hier bijgehouden worden.

Het formaat is gebaseerd op [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) en past [Semantic Versioning](https://semver.org/spec/v2.0.0.html) toe.

## [Unreleased]

### Toegevoegd

- Unittests voor [import_bizz_xslx.py](src/import_bizz_xslxs.py)
- Unittests voor [istd_klassen.py](src/istd_klassen.py)
- Unittests voor [yaml_utils.py](src/yaml_utils.py)
- Validatie istandaard-naar-hugo voor feature [valideren samenhang in data/validatie_regelrapport.feature](features/valideren%20samenhang%20in%20data/validatie_regelrapport.feature)
- Controle regelrapport: wanneer een regel niet wordt gebruikt in een bericht(klasse) en / of element, dan foutmelding.
- Wiki generatie via api i.p.v. genereren mappen die handmatig geplaatst moeten worden

## [1.3.8] - 2023-03-21

### Opgelost

- Code quality issues

## [1.3.7] - 2023-03-20

### Opgelost

- Testscenario's feature files die eigenlijk unittests zijn verplaatst naar unittests

## [1.3.6] - 2023-03-17

### Gewijzigd
- Generen van de living documentation argument afhandeling gebruikt moderne wijze ipv deprecated wijze.

## [1.3.5] - 2023-03-16

### Gewijzigd
- Job ```validate istandaard before publish``` alleen draaien, wanneer het de daily of handmatige pipeline betreft, òf wanneer in de push / merge request ook daadwerkelijk een (of meer) XSD en / of excel files zitten.

## [1.3.4] - 2023-03-16

### Gewijzigd
- In de beschrijving van de living documentation functionaliteit werd nog de "oude" mappenstructuur gebruikt. Omgezet naar de nieuwe mappenstructuur.

## [1.3.3] - 2023-03-08

### Gewijzigd
- Printen van strings in YAML; wanneer de string uit meerdere regels bestaat, deze _altijd_ splitsen naar een array van strings
- Elke (eventueel naar regel gesplitste) string wordt omringd door ```'```  
  
_Dus_: ```string a = "Dit is de eerste regel!\nEn dit de tweede :)"``` wordt in YAML weergegeven als:  
  
```
a:
- 'Dit is de eerste regel!'
- 'En dit de tweede :)'
```

## [1.3.2] - 2023-03-08

### Opgelost
- Exception in ```__main__._write_yaml_output```

### Toegevoegd
- Unittests voor ```__main__._write_yaml_output```

## [1.3.1] - 2023-03-07

### Opgelost
- Linting errors opgelost

## [1.3.0] - 2023-03-07

### Toegevoegd
- Naamgeving van de YAML output sturen via attributen in de klasses.

### Opgelost
- De pre-publish stap zet de output in de folder ```yaml-data``` in plaats van ```data```.
- Er werd geen nieuwe regel geprint na het in YAML plaatsen van een klasse / attribuut / ...

## [1.2.4] - 2023-03-03

### Opgelost
- Alleen de folder waarin yaml bestand moet komen aanmaken, wanneer deze nog niet bestaat.

## [1.2.3] - 2023-03-03

### Opgelost
- Folder waarin yaml bestand moet komen aanmaken.

## [1.2.2] - 2023-03-03

### Gewijzigd
- Unittest ```__main__``` meer gestructureerd

### Toegevoegd
- Unittest ```__main__``` t.b.v. exporteren YAML

## [1.2.1] - 2023-03-02

### Gewijzigd
- Unittest method hernoemd

## [1.2.0] - 2023-03-02

### Opgelost
- Scenario `` importeer_xsd.feature.De technische informatie over de berichtstandaard dient te worden overgenomen in het IStdBericht object```, variant ```standaard``` verplaatst van feature naar unittest

## [1.1.9] - 2023-02-22

### Toegevoegd

- Coverage results toegevoegd, zodat deze beschikbaar zijn voor (toekomstige) merge requests

## [1.1.8] - 2023-02-22

### Opgelost

- Coverage xml report staat in de working folder, niet in ```coverage``` folder

## [1.1.7] - 2023-02-22

### Opgelost

- De namen van de .coverage bestanden gelijk getrokken met de artifact namen...

## [1.1.6] - 2023-02-22

### Opgelost

- Code coverage report ook daadwerkelijk aanroepen in CI/CD pipeline...

## [1.1.5] - 2023-02-22

### Gewijzigd

- Alleen members opnemen in de output YAML wanneer de waarde ook daadwerkelijk gezet is. _Een lege string telt ook als een gezette waarde._

### Toegevoegd

- Wanneer de iStandaard valide is, zorgen dat de YAML files klaargezet worden in de folder ```data```:
  - Berichten (XSD) worden in de folder ```data/bericht``` geplaatst.
  - Regelrapport items (excel) worden in de folder ```data/regel``` geplaatst.
    - Uitgangspunten (code begint met ```UP```) worden in de folder ```data/regel/Uitgangspunt``` geplaatst.
    - Bedrijfsregels (code begint met ```OP```) worden in de folder ```data/regel/Bedrijfsregel``` geplaatst.
    - Technische regels (code begint met ```TR```) worden in de folder ```data/regel/Technische regel``` geplaatst.
    - Invulinstructies (code begint met ```IV```) worden in de folder ```data/regel/Invulinstructie``` geplaatst.
    - Constraints (code begint met ```CS```) worden in de folder ```data/regel/Constraint``` geplaatst.
    - Condities (code begint met ```CD```) worden in de folder ```data/regel/Conditie``` geplaatst.
  - Codelijsten items (excel) worden in de folder ```data/codelijst``` geplaatst.
- Code coverage rapportage op basis van de unittests en behave tests :)

## [1.1.4] - 2023-02-22

### Opgelost

- Bugfix: basisschema*.xsd moet niet gevalideerd worden als een "gewone" xsd.

## [1.1.3] - 2023-02-22

### Opgelost

- Bugfix: controle of map zoals opgegeven in --istandaard-input-folder bestaat

## [1.1.2] - 2023-02-22

### Opgelost

- Validatie van de hele set verplaatst naar pre-publish.

## [1.1.1] - 2023-02-22

### Opgelost

- Nieuwe CI/CD configuratie staat niet in de juiste folder.

## [1.1.0] - 2023-02-22

### Toegevoegd

- Nieuwe stap in CI/CD pipeline; deze gaat alle xsd's in de "xsd folder" valideren. Wanneer deze allemaal correct zijn, dan als vervolgstap deze omzetten naar YAML.

## [1.0.3] - 2023-02-22

### Opgelost

- isort error opgelost voor ci/validate_istandaard_files.py

## [1.0.2] - 2023-02-22

### Opgelost

- Fixed linting errors; no functional changes.

## [1.0.1] - 2023-02-22

### Opgelost

- Bug in unittest test_setup_arguments: er werd nog getest tegen 5 argumenten, waar het er nu 6 zijn.

## [1.0.0] - 2023-02-22

### Toegevoegd

- Verwerkte berichten vertalen naar YAML

## [0.12.6] - 2023-02-22

### Gewijzigd

- Module ```src.istd_klassen``` gerefactored naar ```src.istandaard.classes```

## [0.12.5] - 2023-02-22

### Opgelost

- Verbeterde foutafhandeling in CI/CD job

## [0.12.4] - 2023-02-22

### Opgelost

- De CI/CD job laten falen wanneer er fouten geconstateerd zijn; hierbij het foutrapport tonen

## [0.12.3] - 2023-02-22

### Opgelost

- Bugfix: foutrapport niet via de "standaard" foutafhandeling van argparser laten lopen, maar als exit message meegeven.
- Ophalen van beschrijving die meerdere regels beslaat uit de annotaties geeft fouten

### Toegevoegd

- Unittest voor methode die de beschrijving uit de annotaties haalt

## [0.12.2] - 2023-02-22

### Opgelost

- Bugfix: de nieuw opgebouwde argumenten vanuit sys.argv zorgt voor fouten wanneer er __wel__ netjes via de argparser argumenten zijn (zoals bij unittests).

## [0.12.1] - 2023-02-22

### Opgelost

- vanuit sys.argv opgebouwde argumentstring dan ook daadwerkelijk gebruiken...

## [0.12.0] - 2023-02-22

### Toegevoegd

- wanneer er om wat voor reden dan ook geen argumenten gevonden worden, dan vanuit ```sys.argv``` (old-skool) alsnog de argumenten lezen.

## [0.11.9] - 2023-02-22

### Toegevoegd

- istandaard_naar_hugo laat ook zien hoe die is aangeroepen.

## [0.11.8] - 2023-02-22

### Opgelost

- alleen stderr dumpen wanneer daar ook daadwerkelijk data op staat

## [0.11.7] - 2023-02-22

### Opgelost

- bugfix bij afdrukken van argumenten

## [0.11.6] - 2023-02-22

### Opgelost

- stderr werd niet goed opgepakt

### Gewijzigd

- istandaard_naar_hugo starten via subprocess.run

## [0.11.5] - 2023-02-22

### Gewijzigd

- foutrapport file genereren vanuit redirect stderr, ipv hard wegschrijven vanuit main

## [0.11.4] - 2023-02-22

### Opgelost

- ```--verbose``` vlag stond bij verkeerde programma...

## [0.11.3] - 2023-02-22

### Toegevoegd

- Upload package more verbose

## [0.11.2] - 2023-02-22

### Opgelost

- Vage foutcode in CI/CD job

### Toegevoegd

- Unittest voor dumpen van .md foutrapport als artifact

## [0.11.1] - 2023-02-22

### Opgelost

- Falende unittesten opgelost

## [0.11.0] - 2023-02-22

### Toegevoegd

- Foutrapport (indien aanwezig) als .md file opslaan; aansturing via argument --foutrapport

## [0.10.1] - 2023-02-22

### Opgelost

- Fout bij regelrapport zorgde ervoor dat alle ander gevonden fouten niet meer gerapporteerd werden.

## [0.10.0] - 2023-02-22

### Toegevoegd

- Foutrapportage voor regelrapport

## [0.9.6] - 2023-02-22

### Opgelost

- Dubbele spatie in foutmelding

## [0.9.5] - 2023-02-22

### Gewijzigd

- Foutrapportage als .md opmaken

### Opgelost

- Wanneer er meerdere istandaard XSD's gewijzigd zijn, werden deze steeds als eigen --istandaard doorgegeven. Dit moet niet; het moet zijn --istandaard xsd \[xsd2 xsd3 ...\]
- Dump van (dictionary weergave van) het xsd uit de foutrapportage gehaald.

## [0.9.4] - 2023-02-22

### Opgelost

- Globale lijst met foutcodes legen aan start van main module; hiermee voorkomen we dat wanneer er meerdere calls op dezelfde instantie gedaan worden, dit leidt tot false negatives.

## [0.9.3] - 2023-02-22

### Opgelost

- Foutieve constructie in loop om vast te stellen of er al een fout is opgenomen in de te gooien IStdException

## [0.9.2] - 2023-02-22

### Opgelost

- Verzamelde fouten daadwerkelijk als IStdException gooien

## [0.9.1] - 2023-02-22

### Opgelost

- Falende test: "valideren verzamelde data.importeer_xsd.XSD zoals gegenereerd door Bizzdesigner vertalen naar IStdBericht object.Een complexType dient gebruikt te worden in minimaal een element"

## [0.9.0] - 2023-02-22

### Gewijzigd

- De gehele XSD wordt gevalideerd en validatiefouten worden in een foutrapport opgenomen; dit in plaats van vorige "fout voor fout" rapportage

## [0.8.1] - 2023-02-21

### Opgelost

- Type-of in argumentnaam.

## [0.8.0] - 2023-02-21

### Toegevoegd

- istandaard_naar_hugo verwerkt ook regelrapport excel file
- istandaard_naar_hugo geeft ook de filename terug waarin een fout geconstateerd is

## [0.7.11] - 2023-02-21

### Opgelost

- istandaard_naar_hugo kan niet goed omgaan met de argument string.

## [0.7.10] - 2023-02-21

### Opgelost

- Code quality issues

## [0.7.9] - 2023-02-21

### Gewijzigd

- Rechtstreekse aanroep van __main__.main, met de juiste argumenten.

## [0.7.7] - 2023-02-21

### Opgelost

- AttributeError bij aanroepen istandaard_naar_hugo zonder argumenten

## [0.7.6] - 2023-02-21

### Opgelost

- AttributeError bij aanroepen istandaard_naar_hugo zonder argumenten

## [0.7.5] - 2023-02-21

### Gewijzigd

- Aanroep van istandaard_naar_hugo module via python

## [0.7.4] - 2023-02-20

### Opgelost

- Foutief gebruik van print bij afdrukken command line parameters

## [0.7.3] - 2023-02-20

### Opgelost

- Foutief gebruik van print bij afdrukken command line parameters

## [0.7.2] - 2023-02-20

### Toegevoegd

- Afdrukken van parameters bij aanroep van istandaard_naar_hugo

## [0.7.1] - 2023-02-20

### Opgelost

- Foutieve aanroep van git diff

## [0.7.0] - 2023-02-20

### Gewijzigd

- De validatie van gewijzigde XSD's wordt nu ondersteund. Hierbij worden alleen de daadwerkelijk gewijzigde xsd's (niet zijnde basisschema) gevalideerd.

## [0.6.2] - 2023-02-17

### Opgelost

- Starten behave in CI/CD werkte niet door fout in YAML configuratie.

## [0.6.1] - 2023-02-17

### Opgelost

- Niet geimplementeerde scenario's (met tag ```@not_implemented```) werden alsnog uitgevoerd. Nu opgelost in before_feature en before_scenario hooks.

## [0.6.0] - 2023-02-17

### Opgelost

- Niet geimplementeerde scenario's (met tag ```@not_implemented```) worden niet uitgevoerd door behave; dit voorkomt false negatives

## [0.5.24] - 2023-02-16

### Opgelost

- Wiki generatie: escape groter dan teken in beschrijving van feature / scenario, omdat dit anders gezien wordt als comment in comment.

## [0.5.23] - 2023-02-16

### Opgelost

- Wiki generatie: wel structuur, maar pagina inhoud werd niet getoond

## [0.5.22] - 2023-02-16

### Opgelost

- Wiki generatie: diverse opmaak zaken gefixt aan de gegenereerde .md files

## [0.5.21] - 2023-02-16

### Opgelost

- Wiki generatie: resultaat badge alleen ophalen wanneer ```result``` aanwezig is en daarbinnen ```status``` aanwezig is

## [0.5.20] - 2023-02-16

### Opgelost

- Wiki generatie: output filename werd als folder aangemaakt...

## [0.5.19] - 2023-02-16

### Opgelost

- Wiki generatie: output folder aanmaken als deze nog niet bestaat
- Wiki generatie: als geen beschrijving aanwezig is bij feature of scenario, dan deze niet proberen op te halen

## [0.5.18] - 2023-02-16

### Opgelost

- Wiki generatie: behave.json output file niet openen als UTF-16, maar UTF-8 file.

## [0.5.17] - 2023-02-16

### Gewijzigd

- Wiki generatie: zorgen dat de mappenstructuur uit de features folder ook zichtbaar is in de wiki

## [0.5.16] - 2023-02-14

### Opgelost

- Terugdraaien wijziging in pad naar resultaten test feature files

## [0.5.15] - 2023-02-14

### Opgelost

- Behave installeren t.b.v. uitvoeren functionele testen

## [0.5.14] - 2023-02-14

### Opgelost

- Bugfixes in pad naar resultaten test feature files -> genereer losse output file t.b.v. living documentation

## [0.5.13] - 2023-02-14

### Opgelost

- Bugfixes in pad naar resultaten test feature files

## [0.5.12] - 2023-02-14

### Opgelost

- Bugfixes, die zorgen dat het genereren van de living documentation nu ook _echt_ gaat starten.

### Toegevoegd

- Zorgen dat de gegenereerde living documentation ook als artifact te downloaden zijn, zodat ze handmatig aan de wiki kunnen worden toegevoegd
- Zorgen dat de subfolders waarin de features zitten ook worden meegenomen.

## [0.5.11] - 2023-02-14

### Opgelost

- Living documentatie generatie gaat nu starten, ongeacht of de testen van de featurefiles geslaagd zijn of niet. _Falende testen blijven zorgen voor een falende pipeline natuurlijk..._

## [0.5.10] - 2023-02-14

### Opgelost

- De CI/CD pipeline moet ook afgaan wanneer een feature file wordt gecommit voor de stap "test_feature_files"

## [0.5.9] - 2023-02-14

### Opgelost

- Falende test ```valideren samenhang in data.validatie_regelrapport.Stel vast dat het regelrapport valide is``` ([testrapport](https://gitlab.com/istddevops/exploration/fair-publiceren/iwmo-fair-demo/-/pipelines/776096469/test_report)) scenario ```Een regelcode dient gebruikt te worden binnen een of meer berichtdefinities``` 

## [0.5.8] - 2023-02-10

### Gewijzigd

- Resultaat van aanroep issues API tonen in job output

### Toegevoegd

- Validatie istandaard-naar-hugo voor feature [valideren samenhang in data/validatie_regelrapport.feature](features/valideren%20samenhang%20in%20data/validatie_regelrapport.feature)
- Controle regelrapport: wanneer regel(s) meermalen voorkomen op tabblad "Regels", dan foutmelding.

## [0.5.7] - 2023-02-10

### Opgelost

- Bug in aanroep curl t.b.v. issues API

## [0.5.6] - 2023-02-09

### Gewijzigd

- Functionaliteiten in mappenstructuur geplaatst zodat ze beter beheersbaar en vindbaar worden

### Toegevoegd

- Automatisch testen functionaliteit ```Stel vast dat het regelrapport valide is```

## [0.5.5] - 2023-02-08

### Gewijzigd

- Zorgen dat isort en black elkaar niet bijten

## [0.5.4] - 2023-02-08

### Gewijzigd

- Quality controle fouten ook echo-en naar job output

## [0.5.3] - 2023-02-08

### Gewijzigd

- De bash scripts zijn minder verbose.

## [0.5.2] - 2023-02-08

### Opgelost

- Type-o in code_quality_*.sh scripts verholpen.

## [0.5.1] - 2023-02-08

### Opgelost

- Stderr output werd niet juist afgevangen, waardoor ten onrechte issues aangemaakt zouden worden.

## [0.5.0] - 2023-02-03

### Gewijzigd

- Ontwerprichtlijnen verplaatst naar eigen feature

## [0.4.10] - 2023-02-03

### Opgelost

- urlencode title en description middels juiste python module

## [0.4.9] - 2023-02-03

### Opgelost

- urlencode title en description

## [0.4.8] - 2023-02-03

### Opgelost

- curl eerst installeren op image

## [0.4.7] - 2023-02-03

### Opgelost

- qa draaien op python image

## [0.4.6] - 2023-02-03

### Opgelost

- Eerst de packages installeren die nodig zijn voor qa

## [0.4.5] - 2023-02-03

### Gewijzigd

- De verschillende code quality controles naar bash files verplaatst

## [0.4.4] - 2023-02-03

### Opgelost

- Foutief pad in de code_quality step, waardoor geen issues gerapporteerd kunnen worden

## [0.4.3] - 2023-02-02

### Toegevoegd

- Verder unittests voor [import_bizz_xsds.py](src/import_bizz_xsds.py)
- Automatische code quality report via gitlab

### Opgelost

- Bugs geconstateerd met unittests

### Gewijzigd

- unittesten worden afgetrapt in de 'test' stage van de pipeline

### Verwijderd

- Gitlab code quality check.

## [0.4.2] - 2023-02-01

### Toegevoegd

- Verder unittests voor [import_bizz_xsds.py](src/import_bizz_xsds.py)
- Betere controle op gevuld zijn verplichte argumenten

### Opgelost

- Bugs geconstateerd met unittests

## [0.4.1] - 2023-01-31

### Toegevoegd

- Verder unittests voor [import_bizz_xsds.py](src/import_bizz_xsds.py)

### Gewijzigd

- job '''linters''' hernoemd naar '''code quality''' zodat de unittest resultaten beter terug te vinden / te plaatsen zijn in gitlab
- job '''test import berichtdefinitie''' hernoemd naar '''functionaliteiten''' zodat de feature file resultaten beter terug te vinden / te plaatsen zijn in gitlab

### Verwijderd

- Niet gebruikte "requirements*.txt" files verwijderd; deze staan immers in [pyproject.toml](pyproject.toml)

## [0.4.0] - 2023-01-31

### Opgelost

- Unittest van _valideer_datatypes_gebruik gebruikte xml als fake input data, waar dit de output van xmltodict moet zijn...

### Gewijzigd

- Unittesten uitvoeren met pytest, testresultaten tonen in gitlab

## [0.3.1] - 2023-01-31

### Opgelost

- pyhumps wordt niet automatisch geinstalleerd
- test dependencies stonden als hoofd dependencies aangeduid

## [0.3.0] - 2023-01-31

### Toegevoegd

- Unittests voor [import_bizz_xsds.py](src/import_bizz_xsds.py)
- Unittests voor [__main__.py](src/__main__.py)
- Aftrappen unittests uit folder [unittests](src/unittests/) vanuit CI/CD

### Gewijzigd

- [__main__.py](src/__main__.py) beter opgezet, zodat deze robuuster (en te unittestten) is
- [import_bizz_xsds.py](src/import_bizz_xsds.py)
  - Robuustere opzet van lezen aangeboden xsd file
  - Foutmelding wanneer aangeboden xsd file wel valide xml is, maar geen '''xs:schema''' tag bevat
  - Naamgeving "protected" methods gecorrigeerd
  - Methods die eigenlijk meer dingen doen opgesplitst naar meerdere methods voor betere onderhoudbaarheid en testbaarheid
