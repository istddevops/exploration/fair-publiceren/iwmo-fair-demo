"""Behave hooks"""
# pylint:disable=[W0613]


def skip_not_implemented(target):
    """Skip target"""

    if "not_implemented" in target.tags:
        target.skip("Marked with @not_implemented")
        return


def before_feature(context, feature):
    """Setup testing a feature"""

    skip_not_implemented(feature)


def before_scenario(context, scenario):
    """Setup testing a scenario"""

    skip_not_implemented(scenario)
