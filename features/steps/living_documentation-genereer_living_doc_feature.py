"""Testen om de feature genereer_living_doc automatisch te testen."""

# pylint: disable=[E0611, W0613]

from behave import given, then, when


@given("er is een bestand met extentie .feature aanwezig")
@given("er zijn twee bestanden met extentie .feature aanwezig")
@given("in het bestand met extentie .feature staan een aantal scenario's")
@when("de living documentation wordt opgebouwd")
def dummy(context):
    """Do nothing"""
    assert True


@given("een bestand met extentie .feature staat in de map '{param}'")
@given(
    "in dit bestand met extentie .feature staat een functionaliteit met de "
    "naam '{param}'"
)
@given("het andere bestand met extentie .feature staat in de map '{param}'")
def dummy_one_param(context, param):
    """Do nothing"""
    assert True


@then("is in de output folder '{param1}' een subfolder '{param2}' aanwezig")
@then("is in de subfolder '{param1}' het markdown bestand '{param2}' aanwezig")
@then(
    "heeft het markdown bestand '{param1}' een attribuut title met de waarde "
    "'{param2}'"
)
def dummy_two_params(context, param1, param2):
    """Do nothing"""
    assert True
