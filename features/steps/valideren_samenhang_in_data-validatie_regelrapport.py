"""Testen om de feature "Stel vast dat het regelrapport valide is" te
testen."""

from copy import deepcopy
from typing import List
from unittest.mock import patch

from behave import given, then, when  # pylint:disable=[E0611]
from nose.tools import assert_equal

from src import import_bizz_xslxs


class FakeCell:
    """Mock excel cel object."""

    def __init__(self, value):
        self._value = value

    def _set_value(self, value):
        self._value = value

    def _get_value(self):
        return self._value

    value = property(fget=_get_value, fset=_set_value)


_FAKE_WORKBOOK: dict = {
    "Voorpagina": [],
    "Regels per bericht(element)": [
        [
            FakeCell("Bericht"),
            FakeCell("Berichtklasse"),
            FakeCell(r"Berichtelement\Datatype"),
            FakeCell(r"CDT-element\Datatype"),
            FakeCell(r"CDT-element\Datatype"),
            FakeCell("Sleutelelement"),
            FakeCell("Codelijst"),
            FakeCell("Regel"),
            FakeCell("Regelcode"),
            FakeCell("Retourcode"),
        ],
    ],
    "Regels": [
        [
            FakeCell("Type"),
            FakeCell("Regelcode"),
            FakeCell("Titel"),
            FakeCell("Documentatie"),
            FakeCell("XSD restrictie"),
            FakeCell("Retourcode"),
            FakeCell("Niveau"),
        ],
    ],
}


@given("een regelrapport")
def init_workbook(context):
    """Initialiseer de mock van het excel werkboek"""
    context.fake_workbook = deepcopy(_FAKE_WORKBOOK)


@given(
    "regelcode '{var1}' komt op geen enkele andere regel van tabblad '{var2}'"
    + " voor"
)
def do_nothing_two_variables(context, var1, var2):  # pylint:disable=[W0613]
    """Dummy method voor stappen waarvoor we niets hoeven te doen."""


@given(
    "regelcode '{regelcode}' is gedefinieerd op regel {regelnummer} van "
    + "tabblad '{tabblad}'"
)
@given(
    "regelcode '{regelcode}' is nogmaals gedefinieerd op regel {regelnummer} "
    + "van tabblad '{tabblad}'"
)
def regelcode_gedefinieerd_op_tabblad(
    context, regelcode, regelnummer, tabblad
):  # pylint:disable=[W0613]
    """Mock een rij met een bepaalde regelcode op gespecificeerd tabblad"""

    # Eerst fake regels genereren voor genoemd tabblad:
    # 1. Genereer array van dict, met steeds één key "value" en een dummy
    #    waarde
    # 2. Bewaar de positie van de dict met value "Regelcode"
    # 3. Genereer {regelnummer}-/-1 array items (2.):
    #    - Regelcode = "Dummy<x>" waarbij x gelijk is aan huidige regelnummer
    # 4. Voeg de gegenereerde array (4.) toe aan _fake_workbook
    # 5. Genereer nog één array van dict, en zet de regelcode op {regelcode}

    # 1. Genereer array van dict, met steeds één key "value" en een dummy
    #    waarde
    header_rij: List[FakeCell] = context.fake_workbook[tabblad][0]
    blauwdruk: List[FakeCell] = deepcopy(header_rij)
    for blauwdruk_item in blauwdruk:
        blauwdruk_item.value = ""
    # 2. Bewaar de positie van de dict met value "Regelcode"
    regelcode_kolom_nummer: int = 0
    for regelcode_kolom_nummer, item in enumerate(header_rij):
        if "Regelcode" == item.value:
            break
    # 3. Genereer {regelnummer}-/-1 array items:
    #    - Regelcode = "Dummy<x>" waarbij x gelijk is aan huidige regelnummer
    for rij in range(
        len(context.fake_workbook[tabblad]), (int(regelnummer) - 1)
    ):
        nieuwe_rij: List[FakeCell] = deepcopy(blauwdruk)
        nieuwe_rij[regelcode_kolom_nummer].value = f"Dummy{rij}"
        # 4. Voeg de gegenereerde array (4.) toe aan _fake_workbook
        context.fake_workbook[tabblad].append(nieuwe_rij)
    # 5. Genereer nog één array van dict, en zet de regelcode op {regelcode}
    doel_rij = deepcopy(blauwdruk)
    doel_rij[regelcode_kolom_nummer].value = regelcode
    context.fake_workbook[tabblad].append(doel_rij)


@given(
    "regelcode '{regelcode}' komt niet voor in kolom '{kolom}' van tabblad"
    + " '{tabblad}'"
)
def regelcode_niet_gebruikt(
    context, regelcode, kolom, tabblad
):  # pylint:disable=[W0613]
    """Mock tabblad waarop {regelcode} niet voorkomt in kolom {kolom}"""

    # We doen niets, want in _FAKE_WORKBOOK staan alleen maar lege werksheets


@given(
    "regelcode '{regelcode}' komt voor in kolom '{kolom}' van tabblad "
    + "'{tabblad}'"
)
def regelcode_gebruikt(context, regelcode, kolom, tabblad):
    """Mock tabblad waarop {regelcode} wel voorkomt in kolom {kolom}"""

    # 1. Genereer array van dict, met steeds één key "value" en een dummy
    #    waarde
    # 2. Bewaar de positie van de dict met value "{kolom}"
    # 3. Genereer één array van dict, en zet de regelcode op {regelcode}

    # 1. Genereer array van dict, met steeds één key "value" en een dummy
    #    waarde
    header_rij: List[FakeCell] = context.fake_workbook[tabblad][0]
    blauwdruk: List[FakeCell] = deepcopy(header_rij)
    for blauwdruk_item in blauwdruk:
        blauwdruk_item.value = ""
    # 2. Bewaar de positie van de dict met value "{kolom}"
    regelcode_kolom_nummer: int = 0
    for regelcode_kolom_nummer, item in enumerate(header_rij):
        if kolom == item.value:
            break

    # 5. Genereer één array van dict, en zet de regelcode op {regelcode}
    doel_rij = deepcopy(blauwdruk)
    doel_rij[regelcode_kolom_nummer].value = regelcode
    context.fake_workbook[tabblad].append(doel_rij)


@when("het regelrapport gevalideerd wordt")
def valideer_regelrapport(context):
    """Valideer het gemockte regelrapport"""

    context.caught_exception = None
    with patch.object(
        import_bizz_xslxs, "load_workbook", return_value=context.fake_workbook
    ):
        try:
            regels = import_bizz_xslxs.import_regels_ws("dummy", "Regels")
            import_bizz_xslxs.import_regels_per_bericht_element_ws(
                "dummy", "Regels per bericht(element)", regels
            )
        except Exception as caught_exception:  # pylint:disable=[W0718]
            context.caught_exception = caught_exception


@then(
    "faalt het valideren van het regelrapport met de melding '{foutmelding}'"
)
def assert_faalt_met_melding(context, foutmelding):
    """Stel vast dat {context.caught_exception} de melding {foutmelding}
    bevat"""

    assert_equal(str(context.caught_exception), foutmelding)


@then(
    "faalt het valideren van het regelrapport niet met de melding "
    + "'{foutmelding}'"
)
def assert_faalt_niet_met_melding(context, foutmelding):
    """Stel vast dat {context.caught_exception} NIET de melding {foutmelding}
    bevat"""

    assert str(context.caught_exception) != foutmelding
