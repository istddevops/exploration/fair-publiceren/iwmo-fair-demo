# language: nl

Functionaliteit: Genereer living documentation
    Op basis van alle beschreven functionaliteiten in de feature files zal een static site worden gegenereerd met hierin de living documentation van het DgPi product. 
    Wanneer de scenario's ook automatisch getest worden, zullen ook de testresultaten getoond worden.

    De static site generator die gebruikt wordt is Hugo. We maken gebruik van het [Hugo Book Theme](https://github.com/alex-shpak/hugo-book)

    Er wordt een menustructuur opgebouwd die er als volgt uit ziet:
    Functionaliteiten
      V Functionaliteit 1
        Scenario 1
        Scenario 2
        Scenario 3
      > Functionaliteit 2

    Dit menu wordt aan de linkerzijde van het scherm getoond

Scenario: Voor elk bestand met extentie .feature is een menu-item aanwezig
    Er wordt gezocht binnen deze repository, niet over eventuele andere repositories van het Zorginstituut heen. Het [genereren van het menu](https://github.com/alex-shpak/hugo-book#menu)
    werkt, zoals beschreven, op basis van de mappenstructuur in de folder content. Submappen zijn menu items, onderliggende mappen en / of bestanden zijn submenu items

    Gegeven er zijn twee bestanden met extentie .feature aanwezig

        Stel een bestand met extentie .feature staat in de map '/features'
            En in dit bestand met extentie .feature staat een functionaliteit met de naam 'Test functionaliteit 1'
        Maar het andere bestand met extentie .feature staat in de map '/features/living documentation'
            En in dit bestand met extentie .feature staat een functionaliteit met de naam 'Compleet andere functionaliteit'

    Als de living documentation wordt opgebouwd
        Dan is in de output folder 'content' een subfolder 'Test functionaliteit 1' aanwezig
        En is in de output folder 'content' een subfolder 'Compleet andere functionaliteit' aanwezig

Abstract Scenario: Voor elk scenario in een bestand met extentie .feature is een menu-item aanwezig
    Een bestand met extentie .feature bevat een of meerdere scenario's. Elk scenario krijgt een eigen pagina, welke zichtbaar is in het [menu](https://github.com/alex-shpak/hugo-book#menu).

    Gegeven er is een bestand met extentie .feature aanwezig
        Stel een bestand met extentie .feature staat in de map '/features'
        En in dit bestand met extentie .feature staat een functionaliteit met de naam 'Waarheden als een koe'
        
        Stel in het bestand met extentie .feature staan een aantal scenario's
        | scenario |
        | Een bal dient altijd rond te zijn |
        | Een wiel dient altijd rond te zijn |
        | Een vuur dient altijd licht te geven |
    
    Als de living documentation wordt opgebouwd
        Dan is in de output folder 'content' een subfolder 'Waarheden als een koe' aanwezig
        En is in de subfolder 'Waarheden als een koe' het markdown bestand '<bestandsnaam>' aanwezig
        En heeft het markdown bestand '<bestandsnaam>' een attribuut title met de waarde '<titel>'

    Voorbeelden:
    | bestandsnaam | titel |
    | een-bal-dient-altijd-rond-te-zijn.md | Een bal dient altijd rond te zijn |
    | een-wiel-dient-altijd-rond-te-zijn.md | Een wiel dient altijd rond te zijn |
    | een-vuur-dient-altijd-licht-te-geven.md | Een vuur dient altijd licht te geven |        