# language: nl
Functionaliteit: Stel vast dat het informatiemodel voldoet aan de ontwerprichtlijnen
    Team standaardisatieadvies heeft een set ontwerprichtlijnen opgeleverd waaraan iStandaarden dienen te voldoen.
    Door deze ontwerprichtlijnen voor elke iStandaard af te dwingen, wordt gezorgd dat alle iStandaarden op een gelijke
    wijze opgezet zijn. Hierdoor is het voor gebruikers (softwareleveranciers, functioneel beheerders ketenpartijen, ...)
    eenvoudiger om iStandaarden te gebruiken.

@not_implemented
Scenario: In een berichtklasse dienen een of meer gegevenselementen opgenomen te zijn
    (2017030627 - v6 - Ontwerprichtlijnen iStandaarden; Hoofdstuk 7)
    # To do: Er is één uitzondering hierop in de huidige iStandaarden; nl de Header is een lege berichtklasse.

    Gegeven een berichtstandaard waarbij een berichtklasse met de naam "Test" is opgenomen
        Stel in de berichtklasse met de naam "Test" staan geen gegevenselementen opgenomen
            En de naam van de berichtklasse is niet gelijk aan "Header"
            # deze toegevoegd ivm uitzondering; werkt dat zo??
        Als deze berichtstandaard gevalideerd wordt tegen de ontwerprichtlijnen
        Dan faalt de validatie met de foutmelding "Berichtklasse 'Test' bevat geen gegevenselementen"

        Stel in de berichtklasse met de naam "Test" staat een gegevenselement met de naam "DgPi" opgenomen
            En de berichtklasse met de naam "Test" voldoet aan alle overige ontwerprichtlijnen
        Als deze berichtstandaard gevalideerd wordt tegen de ontwerprichtlijnen
        Dan slaagt de validatie van de berichtklasse met de naam "Test"

@not_implemented
Scenario: Elk gegevenselement in een berichtklasse is gespecificeerd in een logisch of composite datatype
    (2017046912 - v1c - Inrichting informatiemodel iStandaarden incl checklist)

    Gegeven een berichtklasse met de naam "Klasse" waarbij een gegevenselement met de naam "Element" is opgenomen
        Stel voor het gegevenselement met de naam "Element" is geen logisch datatype aangegeven
            En voor het gegevenselement met de naam "Element" is geen composite datatype aangegeven
        Als deze berichtklasse gevalideerd wordt tegen de ontwerprichtlijnen
        Dan faalt de validatie met de foutmelding "Gegevenselement 'Element' van de berichtklasse 'Klasse' bevat geen logisch of composite datatype"

        Stel voor gegevenselement met de naam "Element" is een logisch datatype aangegeven
            En het gegevenselement met de naam "Element" voldoet aan alle overige ontwerprichtlijnen
        Als dit gegevenselement gevalideerd wordt tegen de ontwerprichtlijnen
        Dan slaagt de validatie van het gegevenselement met de naam "Element"

        Stel voor een gegevenselement met de naam "Element" is een composite datatype aangegeven
            En het gegevenselement met de naam "Element" voldoet aan alle overige ontwerprichtlijnen
        Als dit gegevenselement gevalideerd wordt tegen de ontwerprichtlijnen
        Dan slaagt de validatie van het gegevenselement met de naam "Element"