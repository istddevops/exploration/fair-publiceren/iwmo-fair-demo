# language: nl

Functionaliteit: Stel vast dat het regelrapport valide is
    Aan de regels uit het regelrapport worden een aantal eisen gesteld. Deze functionaliteit valideert het regelrapport
    tegen genoemde eisen.

Scenario: Een regelcode dient uniek te zijn binnen de gedefinieerde regels
    We willen dat een regelcode slechts éénmaal gedefinieerd wordt. Een regelcode mag wel meermalen gebruikt worden in
    diverse berichtklassen en / of onderliggende berichtelementen. 

    Gegeven een regelrapport
    Stel regelcode 'CS001' is gedefinieerd op regel 2 van tabblad 'Regels'
        Maar regelcode 'CS001' is nogmaals gedefinieerd op regel 20 van tabblad 'Regels'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport met de melding 'Regelcode "CS001" is meermalen gedefinieerd op tabblad "Regels".'

    Gegeven een regelrapport
    Stel regelcode 'CS002-a' is gedefinieerd op regel 2 van tabblad 'Regels'
        Maar regelcode 'CS002-a' is nogmaals gedefinieerd op regel 20 van tabblad 'Regels'
    En regelcode 'CS002-b' is gedefinieerd op regel 21 van tabblad 'Regels'
        Maar regelcode 'CS002-b' is nogmaals gedefinieerd op regel 30 van tabblad 'Regels'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport met de melding 'Regelcodes "CS002-a, CS002-b" zijn meermalen gedefinieerd op tabblad "Regels".'

    Gegeven een regelrapport
    Stel regelcode 'CS003' is gedefinieerd op regel 2 van tabblad 'Regels'
        En regelcode 'CS003' komt op geen enkele andere regel van tabblad 'Regels' voor
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport niet met de melding 'Regelcode "CS003" is meermalen gedefinieerd op tabblad "Regels".'

Scenario: Een regelcode dient gebruikt te worden binnen een of meer berichtdefinities
    We willen dat een regelcode ook daadwerkelijk gebruikt wordt in berichtdefinitie(s).

    Gegeven een regelrapport
    Stel regelcode 'CS004' is gedefinieerd op regel 2 van tabblad 'Regels'
        Maar regelcode 'CS004' komt niet voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport met de melding 'Regelcode "CS004" wordt niet gebruikt in enig bericht(element).'

    Gegeven een regelrapport
    Stel regelcode 'CS005-a' is gedefinieerd op regel 2 van tabblad 'Regels'
        Maar regelcode 'CS005-a' komt niet voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    En regelcode 'CS005-b' is gedefinieerd op regel 2 van tabblad 'Regels'
        Maar regelcode 'CS005-b' komt niet voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport met de melding 'Regelcodes "CS005-a, CS005-b" worden niet gebruikt in enig bericht(element).'
        
    Gegeven een regelrapport
    Stel regelcode 'CS006' is gedefinieerd op regel 2 van tabblad 'Regels'
        En regelcode 'CS006' komt voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport niet met de melding 'Regelcode "CS006" wordt niet gebruikt in enig bericht(element).'

@not_implemented
Scenario: Een uitgangspunt dient niet gebruikt te worden binnen enige berichtdefinitie
    Uitgangspunten mogen niet gebruikt worden aan een specifieke berichtdefinitie; dit zijn de basisafspraken die horen bij de betreffende iStandaard.

    Gegeven een regelrapport
    Stel uitgangspunt 'UP001' is gedefinieerd op regel 2 van tabblad 'Regels'
        En uitgangspunt 'UP001' komt voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport met de melding 'Uitgangspunt "UP001" wordt gebruikt in een bericht(element); dit is niet toegestaan.'

    Gegeven een regelrapport
    Stel uitgangspunt 'UP002-a' is gedefinieerd op regel 2 van tabblad 'Regels'
        En uitgangspunt 'UP002-a' komt voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    En uitgangspunt 'UP002-b' is gedefinieerd op regel 2 van tabblad 'Regels'
        En uitgangspunt 'UP002-b' komt voor in kolom 'Regelcode' van tabblad 'Regels per bericht(element)'
    Als het regelrapport gevalideerd wordt
        Dan faalt het valideren van het regelrapport met de melding 'Uitgangspunten "UP002-a, UP002-b" worden gebruikt in een bericht(element); dit is niet toegestaan.'
