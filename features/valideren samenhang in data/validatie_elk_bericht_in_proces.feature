# language: nl

Functionaliteit: Stel vast dat de procesmodellen volledig zijn
    Aan de procesmodellen van een release van een informatiestandaard worden een aantal eisen gesteld. Deze functionaliteit valideert de procesmodellen
    tegen genoemde eisen.

@not_implemented
Scenario: Een berichtcode moet minimaal 1 keer voorkomen in de procesmodellen
    We willen niet dat een bericht niet los hangt, maar dat er altijd een relatie is tussen een bericht en een proces.
    [Uitleg bij validatieregel](https://gitlab.com/istddevops/dgpi-docs/-/blob/main/Uitleg%20bij%20feature%20files/validatie_elk_bericht_in_proces.md)

    #Goedsituatie - goedsituaties definieren we alleen als we ook een verwerkingsverslag willen
    Gegeven de berichtdefinities
    Stel bericht met berichtcode 'WMO301' komt voor
        En berichtcode met berichtcode 'WMO301' komt voor in de procesmodellen
    Als de procesmodellen gevalideerd worden
        Dan slaagt het valideren van de procesmodellen met de melding 'Alle berichten zijn gedefinieerd in de procesmodellen.'
    
    #Foutsituatie - foutsituaties dikken we in als ze vaker voorkomen
    Gegeven de berichtdefinities
    Stel bericht met berichtcode 'WMO301' komt voor
        Maar berichtcode met berichtcode 'WMO301' komt niet voor in de procesmodellen
    Als de procesmodellen gevalideerd worden
        Dan faalt het valideren van de procesmodellen met de melding 'Bericht(en) met berichtcode {1 of meerdere codes} komt/komen niet voor in de procesmodellen.'