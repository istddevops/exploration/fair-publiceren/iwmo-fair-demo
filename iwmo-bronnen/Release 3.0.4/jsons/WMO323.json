{
  "xs:schema": {
    "@xmlns:xs": "http://www.w3.org/2001/XMLSchema",
    "@xmlns:iwmo": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
    "@xmlns:wmo323": "http://www.istandaarden.nl/iwmo/3_0/wmo323/schema",
    "@targetNamespace": "http://www.istandaarden.nl/iwmo/3_0/wmo323/schema",
    "@elementFormDefault": "qualified",
    "xs:import": {
      "@namespace": "http://www.istandaarden.nl/iwmo/3_0/basisschema/schema",
      "@schemaLocation": "basisschema.xsd"
    },
    "xs:annotation": {
      "xs:appinfo": {
        "iwmo:standaard": "iwmo",
        "iwmo:bericht": "wmo323",
        "iwmo:release": "3.0",
        "iwmo:BerichtXsdVersie": "1.0.2",
        "iwmo:BerichtXsdMinVersie": "1.0.0",
        "iwmo:BerichtXsdMaxVersie": "1.0.2",
        "iwmo:BasisschemaXsdVersie": "1.2.0",
        "iwmo:BasisschemaXsdMinVersie": "1.0.0",
        "iwmo:BasisschemaXsdMaxVersie": "1.2.0"
      }
    },
    "xs:element": {
      "@name": "Bericht",
      "@type": "wmo323:Root"
    },
    "xs:complexType": [
      {
        "@name": "Root",
        "xs:annotation": {
          "xs:documentation": "Bericht voor declaratie Wmo-hulp."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Header",
              "@type": "wmo323:Header"
            },
            {
              "@name": "Declaratie",
              "@type": "wmo323:Declaratie"
            }
          ]
        }
      },
      {
        "@name": "Header",
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "BerichtCode",
              "xs:annotation": {
                "xs:documentation": "Code ter identificatie van een soort bericht."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtCode",
                  "xs:pattern": {
                    "@value": "484"
                  }
                }
              }
            },
            {
              "@name": "BerichtVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een major release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtVersie",
                  "xs:pattern": {
                    "@value": "3"
                  }
                }
              }
            },
            {
              "@name": "BerichtSubversie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een minor release van een iStandaard."
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_BerichtSubversie",
                  "xs:pattern": {
                    "@value": "0"
                  }
                }
              }
            },
            {
              "@name": "Afzender",
              "@type": "iwmo:LDT_AgbCode",
              "xs:annotation": {
                "xs:documentation": "Identificerende code van een aanbieder van zorg of ondersteuning."
              }
            },
            {
              "@name": "Ontvanger",
              "@type": "iwmo:LDT_Gemeente",
              "xs:annotation": {
                "xs:documentation": "Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of ondersteuning."
              }
            },
            {
              "@name": "BerichtIdentificatie",
              "@type": "iwmo:CDT_BerichtIdentificatie",
              "xs:annotation": {
                "xs:documentation": "Naam of nummer en dagtekening ter identificatie van een totale aanlevering."
              }
            },
            {
              "@name": "XsdVersie",
              "@type": "iwmo:CDT_XsdVersie",
              "xs:annotation": {
                "xs:documentation": "Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het heenbericht."
              }
            }
          ]
        }
      },
      {
        "@name": "Declaratie",
        "xs:annotation": {
          "xs:documentation": "Gegevens over de ingediende declaratie."
        },
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "DeclaratieNummer",
              "xs:annotation": {
                "xs:documentation": "Unieke identificatie van een declaratie"
              },
              "xs:simpleType": {
                "xs:restriction": {
                  "@base": "iwmo:LDT_IdentificatieDeclaratie",
                  "xs:pattern": {
                    "@value": "([a-zA-Z0-9])+"
                  }
                }
              }
            },
            {
              "@name": "DeclaratiePeriode",
              "@type": "iwmo:CDT_GeslotenPeriode",
              "xs:annotation": {
                "xs:documentation": "Eerste en laatste dag van een administratieve periode waarop de gegevensuitwisseling betrekking heeft."
              }
            },
            {
              "@name": "DeclaratieDagtekening",
              "@type": "iwmo:LDT_Datum",
              "xs:annotation": {
                "xs:documentation": "Datum van ondertekening van een declaratie door de declarant."
              }
            },
            {
              "@name": "TotaalIngediendBedrag",
              "@type": "iwmo:CDT_TotaalBedragMetDC",
              "xs:annotation": {
                "xs:documentation": "Het bedrag dat in zijn totaliteit gedeclareerd is."
              }
            },
            {
              "@name": "Clienten",
              "@type": "wmo323:Clienten"
            }
          ]
        }
      },
      {
        "@name": "Clienten",
        "xs:sequence": {
          "xs:element": {
            "@name": "Client",
            "@type": "wmo323:Client",
            "@maxOccurs": "unbounded"
          }
        }
      },
      {
        "@name": "Client",
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "Bsn",
              "@type": "iwmo:LDT_BurgerServicenummer",
              "xs:annotation": {
                "xs:documentation": "Een door de overheid toegekend identificerend nummer in het kader van het vereenvoudigen van het contact tussen overheid en burgers en het verminderen van de administratieve lasten."
              }
            },
            {
              "@name": "Prestaties",
              "@type": "wmo323:Prestaties"
            }
          ]
        }
      },
      {
        "@name": "Prestaties",
        "xs:sequence": {
          "xs:element": {
            "@name": "Prestatie",
            "@type": "wmo323:Prestatie",
            "@maxOccurs": "unbounded"
          }
        }
      },
      {
        "@name": "Prestatie",
        "xs:sequence": {
          "xs:element": [
            {
              "@name": "ProductReferentie",
              "@type": "iwmo:CDT_Referentie",
              "xs:annotation": {
                "xs:documentation": "Identificerende referentie van de declaratie van een zorg - of ondersteuningsprestatie."
              }
            },
            {
              "@name": "ToewijzingNummer",
              "@type": "iwmo:LDT_Nummer",
              "xs:annotation": {
                "xs:documentation": "Identificerend nummer van de opdracht om een zorg - of ondersteuningsproduct te leveren, zoals vastgesteld door de gemeente."
              }
            },
            {
              "@name": "ProductCategorie",
              "@type": "iwmo:LDT_ProductCategorie",
              "xs:annotation": {
                "xs:documentation": "Gecodeerde aanduiding van hoofdcategorieen van producten in het kader van de Wet maatschappelijke ondersteuning (Wmo)."
              }
            },
            {
              "@name": "ProductCode",
              "@type": "iwmo:LDT_ProductCode",
              "xs:annotation": {
                "xs:documentation": "Gecodeerde aanduiding van producten in het kader van de Wet maatschappelijke ondersteuning (Wmo)."
              }
            },
            {
              "@name": "ProductPeriode",
              "@type": "iwmo:CDT_GeslotenPeriode",
              "xs:annotation": {
                "xs:documentation": "Periode binnen de declaratie-/factuurperiode waarop het product feitelijk is of wordt uitgevoerd."
              }
            },
            {
              "@name": "GeleverdVolume",
              "@type": "iwmo:LDT_Volume",
              "xs:annotation": {
                "xs:documentation": "Hoeveelheid verricht product in een prestatieperiode."
              }
            },
            {
              "@name": "Eenheid",
              "@type": "iwmo:LDT_Eenheid",
              "xs:annotation": {
                "xs:documentation": "De eenheid waarin de zorg/behandeling wordt uitgedrukt."
              }
            },
            {
              "@name": "ProductTarief",
              "@type": "iwmo:LDT_Bedrag",
              "@minOccurs": "0",
              "xs:annotation": {
                "xs:documentation": "Tarief van een individueel product."
              }
            },
            {
              "@name": "IngediendBedrag",
              "@type": "iwmo:CDT_BedragMetDC",
              "xs:annotation": {
                "xs:documentation": "Het bedrag dat gedeclareerd wordt."
              }
            }
          ]
        }
      }
    ]
  }
}
