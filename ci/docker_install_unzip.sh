#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git in case it isn't available in the Docker image
apt-get update -yqq
apt-get install git -yqq

apt-get install unzip -yqq