#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get upgrade -yqq
apt-get install pip -yqq

# Update pip in case of newer version
pip install --upgrade pip

# Install nose
pip install nose
pip install behave
pip install isort
pip install black
pip install flake8
pip install pylint
pip install xmltodict
