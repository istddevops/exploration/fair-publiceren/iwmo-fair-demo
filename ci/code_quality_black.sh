#!/bin/bash

curdir="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
source "$curdir/functions_issue_api.sh"

ERROR="$(black --check --diff . 2>&1 > /dev/null)"
echo "$ERROR"
if [ -n "$ERROR" ]; then
    response=$(maak_issue ${CI_JOB_TOKEN} ${CI_PROJECT_ID} "${CI_COMMIT_SHA} - black issue ${CI_COMMIT_BRANCH}" "black reported error for commit ${CI_COMMIT_SHA} on branch ${CI_COMMIT_BRANCH} commited by author ${CI_COMMIT_AUTHOR}\n=================================\n${ERROR}")
    echo "$response"
fi
