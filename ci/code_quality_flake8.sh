#!/bin/bash

curdir="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
source "$curdir/functions_issue_api.sh"

ERROR="$(flake8 --statistics . 2>&1 > /dev/null)"
echo "$ERROR"
if [ -n "$ERROR" ]; then
    response=$(maak_issue ${CI_JOB_TOKEN} ${CI_PROJECT_ID} "${CI_COMMIT_SHA} - flake8 issue ${CI_COMMIT_BRANCH}" "flake8 reported error for commit ${CI_COMMIT_SHA} on branch ${CI_COMMIT_BRANCH} commited by author ${CI_COMMIT_AUTHOR}\n=================================\n${ERROR}")
    echo "$response"
fi
