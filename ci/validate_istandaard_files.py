"""Roept istandaard_naar_hugo aan, waarbij alleen de relevante files uit de
commitset gebruikt worden."""

import subprocess
import sys
from typing import List

print(f"sys.argv[1] type is {type(sys.argv[1])}", file=sys.stdout, flush=True)
print(f"{sys.argv[1]}")
print(f"{sys.argv[1].splitlines()}")
if isinstance(sys.argv[1], str):
    params: List[str] = sys.argv[1].splitlines()
elif isinstance(sys.argv[1], List[str]):
    params: List[str] = sys.argv[1]
else:
    raise AttributeError(
        f"Meegegeven argument heeft niet ondersteund type {type(sys.argv[1])}"
    )

arguments = ["istandaard_naar_hugo"]
istandaarden = []

for param in params:
    print(f"Processing argument {param}...", file=sys.stdout, flush=True)
    PARAM = ""
    param_items: List[str] = param.split("/")
    if param.endswith(".xsd"):
        if param_items[-1].startswith("basisschema"):
            arguments.append(f'--basisschema "{param}"')
            print(
                f"...added as argument {arguments[-1]}",
                file=sys.stdout,
                flush=True,
            )
        else:
            istandaarden.append(param)
            print("...added to istandaarden list")
    elif param.endswith(".xlsx"):
        if param_items[-1].startswith("codelijsten"):
            arguments.append(f'--codelijsten "{param}"')
            print(
                f"...added as argument {arguments[-1]}",
                file=sys.stdout,
                flush=True,
            )
        else:
            arguments.append(f'--berichtregels "{param}"')
            print(
                f"...added as argument {arguments[-1]}",
                file=sys.stdout,
                flush=True,
            )

if 0 < len(istandaarden):
    ARGUMENTS_STR = "--istandaard"
    for istandaard in istandaarden:
        ARGUMENTS_STR = f'{ARGUMENTS_STR} "{istandaard}"'
    arguments.append(ARGUMENTS_STR)
    print(f"...added as argument {arguments[-1]}", file=sys.stdout, flush=True)

PROCESS = " ".join(arguments)
print(f"calling {PROCESS}...", file=sys.stdout, flush=True)
subprocess.call(PROCESS, shell=True)
