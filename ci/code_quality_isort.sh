#!/bin/bash

curdir="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
source "$curdir/functions_issue_api.sh"

ERROR="$(isort . --check-only --profile black --multi-line 3 --diff --line-length=79 2>&1 > /dev/null )"
echo "$ERROR"
if [ -n "$ERROR" ]; then
    response=$(maak_issue ${CI_JOB_TOKEN} ${CI_PROJECT_ID} "${CI_COMMIT_SHA} - isort issue ${CI_COMMIT_BRANCH}" "isort reported error for commit ${CI_COMMIT_SHA} on branch ${CI_COMMIT_BRANCH} commited by author ${CI_COMMIT_AUTHOR}\n=================================\n${ERROR}")
    echo "$response"
fi
