#!/bin/bash

# Function declarations
maak_issue() {
    local token="$1"
    local projectid="$2"
    local title="$3"
    local description="$4"
    local querystring=$(urlencode_title_and_description "$title" "$description")
    local API_URL="https://gitlab.com/api/v4/projects/${projectid}/issues"
    local api_url_querystring="$API_URL?$querystring"

    local response=$(curl --request POST --header 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $token' "$api_url_querystring")
    echo "$response"
}

urlencode_title_and_description() {
    local title="$1"
    local description="$2"
    python -c "import urllib.parse, sys; print(urllib.parse.urlencode({ 'title': sys.argv[1], 'description': sys.argv[2]}))" \
        "$title" "$description"
}