#!/bin/bash

curdir="$(dirname -- "$(readlink -f "${BASH_SOURCE}")")"
source "$curdir/functions_issue_api.sh"

ERROR="$(pylint ./**/*.py 2>&1 > /dev/null)"
echo "$ERROR"
if [ -n "$ERROR" ]; then
    response=$(maak_issue ${CI_JOB_TOKEN} ${CI_PROJECT_ID} "${CI_COMMIT_SHA} - pylint issue ${CI_COMMIT_BRANCH}" "pylint reported error for commit ${CI_COMMIT_SHA} on branch ${CI_COMMIT_BRANCH} commited by author ${CI_COMMIT_AUTHOR}\n=================================\n${ERROR}")
    echo "$response"
fi
