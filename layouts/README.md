# Generatie van **HTML-demo**

## Doel

De **HUGO Layout-templates** in deze folder zijn bedoeld om te onderzoeken hoe vanuit gestandaardiseerde **YAML-data** en **Markdown-content** en een **SSG**(Statische Site Generator) een **HTML-publicatie** kan worden samengesteld.

## Overzicht Layout-templates

De layout-templates voor deze verkenning zijn per onderliggende **folder**:

- [Uitbreidingen op het **IstdDevops Template**](partials/docs)
- [**iStandaard** demo Publicatie Templates](partials/istd)

# Gebruikte componenten uit het Eco-systeem

Dit zijn voorlopige keuzes maar wel volledig herbruibare componenten die binnen het `Ecosysteem` veel worden toegepast, breed worden ondersteund en op lange termijn duurzaam vanuit `Open Standaarden` zijn samengesteld.*

## SSG HUGO

Het [HUGO-raamwerk](https://gohugo.io) is voor deze demo basis gebruikt. De vormgeving maakt gebruik van het [IstdDevops Template](https://gitlab.com/istddevops/shared/gohugo/gohugo-book-template) dat momenteel voor de *KIK-V publicaties* worden toegepast. Dit *Maatwerk Template* is ontwikkeld bovenop een *kopie* van het [HUGO Book Theme](https://github.com/alex-shpak/hugo-book).

Voor de presentatie in **UML-diagrammen** is de vormgeving van de HTML-site demo gebouwd op basis van het [kroki! ecosysteem](https://kroki.io).
