"""Importeert de behave resultaten (json) en zet deze om naar .md files"""
import json
import os
import sys
from argparse import ArgumentParser


def _setup_arguments(parser: ArgumentParser) -> ArgumentParser:
    """Setup command line arguments"""
    parser.add_argument("input_json")
    parser.add_argument("output_folder")

    return parser


def _process_argv_arguments() -> str:
    """Process command line arguments, using "old-skool" sys.argv array"""
    result = ""
    for argv in sys.argv[1:]:
        if argv.startswith("--"):
            result = f"{result} {argv}"
        else:
            result = f'{result} "{argv}"'
    return result


def main(arg_strings=None):
    """Main function for CLI"""

    new_arg_strings = arg_strings
    if None is arg_strings and len(sys.argv) > 1:
        new_arg_strings = _process_argv_arguments()
        print(
            "...took arguments from sys.argv: "
            + f"now called with '{new_arg_strings}'"
        )
    arg_strings = new_arg_strings

    parser: ArgumentParser = ArgumentParser(
        description="Vertaal behave json output naar markdown files"
    )
    _setup_arguments(parser)
    options = parser.parse_args(arg_strings)

    with open(options.input_json, encoding="UTF-8") as behave_json_file:
        behave_results = json.load(behave_json_file)
        for feature in behave_results:
            feature_name: str = feature["name"]
            feature_location: str = feature["location"]
            if "description" in feature:
                feature_description = feature["description"]
            else:
                feature_description = ""  # pylint:disable=[C0103]

            markdown = ""  # pylint:disable=[C0103]
            for feature_description_line in feature_description:
                feature_description_line = feature_description_line.replace(
                    ">", r"\>"
                )
                markdown = f"{markdown}> {feature_description_line}  \n"
            markdown = f"{markdown}\n[[_TOC_]]\n"

            for scenario in feature["elements"]:
                scenario_name = scenario["name"]
                if "description" in scenario:
                    scenario_description = scenario["description"]
                else:
                    scenario_description = ""  # pylint:disable=[C0103]
                markdown = f"{markdown}\n## {scenario_name}  \n\n"
                for scenario_description_line in scenario_description:
                    scenario_description_line = (
                        scenario_description_line.replace(">", r"\>")
                    )
                    markdown = f"{markdown}> {scenario_description_line}  \n"
                markdown = f"{markdown}\n"

                for step in scenario["steps"]:
                    step_keyword = step["keyword"]
                    step_name = step["name"]
                    step_regel = f"**{step_keyword}** {step_name}"
                    step_status = "unknown"  # pylint:disable=[C0103]
                    if "result" in step:
                        if "status" in step["result"]:
                            step_status = step["result"]["status"]

                    STEP_RESULT_COLOR = ""
                    STEP_RESULT_MESSAGE = ""
                    match step_status:
                        case "passed":
                            STEP_RESULT_COLOR = "brightgreen"
                            STEP_RESULT_MESSAGE = "&#x2714;"
                        case "failed":
                            STEP_RESULT_COLOR = "red"
                            STEP_RESULT_MESSAGE = "X"
                        case _:
                            STEP_RESULT_COLOR = "lightgrey"
                            STEP_RESULT_MESSAGE = "%3F"
                    BADGE_FRAGMENT = "https://img.shields.io/badge/"
                    BADGE_PARAM = f"-{STEP_RESULT_MESSAGE}-{STEP_RESULT_COLOR}"
                    BADGE_URL = f"{BADGE_FRAGMENT}{BADGE_PARAM}"
                    BADGE = f"![{STEP_RESULT_MESSAGE}]({BADGE_URL})"
                    markdown = f"{markdown}{BADGE}  {step_regel}  \n"

            feature_location_parts = feature_location.split("/")
            feature_location_path = ""  # pylint:disable=[C0103]
            for item in feature_location_parts[1:-1]:
                feature_location_path = f"{feature_location_path}{item}/"

            feature_filename = f"{feature_name.replace(' ', '-')}.md"
            feature_file_folder = (
                f"{options.output_folder}/{feature_location_path}"
            ).replace(" ", "-")
            feature_file_path = f"{feature_file_folder}{feature_filename}"

            if not os.path.exists(feature_file_folder):
                os.makedirs(feature_file_folder, exist_ok=True)
            with open(
                f"{feature_file_path}", mode="w+", encoding="UTF-8"
            ) as markdown_file:
                markdown_file.write(markdown)


if __name__ == "__main__":
    print("__main__", flush=True)
    print(f"{str(sys.argv)}", flush=True)
    main()
