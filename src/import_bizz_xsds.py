"""iStandaard BizzDesign Output XSD-bestand (XLM-bericht schema) conversie naar
Pyhon Class Formaat (istd_klassen)

Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubli-
ceerde XSDs

Auteur: A. Haldar (Onno)"""

from typing import Dict, List, Tuple
from xml.parsers.expat import ExpatError

# package imports
import xmltodict

# local imports
from .istandaard.classes import (
    IStdAggregatieRelatie,
    IStdBasisSchema,
    IStdBericht,
    IStdDataType,
    IStdElement,
    IStdException,
    IStdKlasse,
    XsdRestriction,
)


def xml_file_to_dict(xml_file_path: str):
    """Geeft XML-bestand terug als Python Dict-structuur terug."""
    xml_lines = ""
    with open(xml_file_path, encoding="UTF-8") as xml_file:
        for xml_line in xml_file.readlines():
            xml_lines = xml_lines + xml_line + "\n"
    return xmltodict.parse(xml_lines)


def get_xml_dict_type(xml_part: dict, part_key):
    """Geeft xml part dict-type terug."""
    return str((type(xml_part[part_key])))


def get_xml_object_value(xml_object: dict, namespace: str, element: str):
    """Geeft xml string-waarde waarde terug. Wanneer het element met gegeven
    namespace niet aanwezig is in het xml_object, dan wordt None
    teruggegeven"""
    # Bugfix: wanneer er geen namespace is, dan deze ook negeren
    key = ""
    if namespace is not None:
        key = namespace + ":" + element
    else:
        key = element

    # Improvement: wanneer het element niet gevonden wordt, dan None teruggeven
    if key in xml_object:
        return xml_object[key]

    return None


def get_doc_str(xs_object: dict) -> str:
    """Geeft string met Annotatie Documentatie Regel(s) van Xs-Object
    terug."""

    if "xs:annotation" not in xs_object:
        return None
    if "xs:documentation" not in xs_object["xs:annotation"]:
        return None

    if isinstance(xs_object["xs:annotation"]["xs:documentation"], List):
        return "\n".join(xs_object["xs:annotation"]["xs:documentation"])

    return xs_object["xs:annotation"]["xs:documentation"]


def __merge_relatie(relaties: IStdAggregatieRelatie, relatie_naam: str = ""):
    """Indien relatie_naam nog niet in de dictionary relaties aanwezig is, dan
    wordt relatie_naam toegevoegd."""

    # Refactor: relaties wordt een dictionary i.p.v. een "platte" array, zodat
    #           we de elementen op key kunnen benaderen i.p.v. op index

    if relatie_naam not in relaties:
        relatie = IStdAggregatieRelatie(relatie_naam)
        relaties[relatie_naam] = relatie


def __add_klasse_element(
    bericht: IStdBericht,
    klasse_naam: str,
    klasse_beschrijving: str = None,
    element_naam: str = None,
):
    """Voeg (Klasse) Element toe aan IStdBericht-Instantie."""

    # Refactor: geen "platte" array, maar een dictionary
    klasse = None
    if klasse_naam in bericht.klassen:
        klasse = bericht.klassen[klasse_naam]
    else:
        klasse = IStdKlasse(klasse_naam)
        klasse.beschrijving = klasse_beschrijving
        bericht.klassen[klasse_naam] = klasse

    # Nu het element toevoegen. We gaan er vanuit dat in een complexType
    # sequence elke elementnaam uniek is.
    klasse.elementen[element_naam] = IStdElement(element_naam)


def _verwerk_complex_type(
    xs_complex_type: dict, app_info: dict
) -> IStdDataType:
    """Verwerk een complexType uit de XSD"""

    if None is xs_complex_type or 0 == len(xs_complex_type):
        raise ValueError("xs_complex_type is niet gevuld")

    if app_info is not None and "bericht" in app_info:
        bericht = app_info["bericht"].lower()
        type_naam = f"{bericht}:{xs_complex_type['@name']}"
    else:
        type_naam = f"{xs_complex_type['@name']}"

    result = IStdDataType(type_naam)
    if (
        "xs:annotation" in xs_complex_type
        and "xs:documentation" in xs_complex_type["xs:annotation"]
    ):
        result.beschrijving = get_doc_str(xs_complex_type)
    result.is_complex = True

    return result


def _verwerk_en_voeg_complex_type_toe(
    xs_complex_type: dict, app_info: dict, datatypen: Dict[str, IStdDataType]
):
    """Verwerk het complexType en voeg deze toe aan de lijst met
    IStdDataType"""

    complex_type = _verwerk_complex_type(xs_complex_type, app_info)
    datatypen[complex_type.naam] = complex_type


def _verwerk_simple_type(xs_simple_type: dict, app_info: dict) -> IStdDataType:
    """Verwerk een simpleType uit de XSD"""

    if 0 == len(app_info):
        raise ValueError("app_info bevat geen items")
    if "bericht" not in app_info:
        raise ValueError("app_info bevat geen item 'bericht'")

    if "@name" not in xs_simple_type:
        raise IStdException("xs:simpleType zonder name attribuut")

    bericht = app_info["bericht"].lower()
    type_naam = bericht + ":" + xs_simple_type["@name"]

    result = IStdDataType(type_naam)
    if (
        "xs:annotation" in xs_simple_type
        and "xs:documentation" in xs_simple_type["xs:annotation"]
    ):
        result.beschrijving = get_doc_str(xs_simple_type)

    if "xs:restriction" in xs_simple_type:
        result.restriction = XsdRestriction()
        xs_restriction = xs_simple_type["xs:restriction"]
        if "@base" in xs_restriction:
            result.restriction.basis_type = xs_restriction["@base"]
        elements: dict = {
            "xs:maxLength": "max_lengte",
            "xs:minLength": "min_lengte",
            "xs:minInclusive": "min_waarde",
            "xs:maxInclusive": "max_waarde",
            "xs:pattern": "patroon",
        }
        for element_key, attribuut in elements.items():
            if (
                element_key in xs_restriction
                and "@value" in xs_restriction[element_key]
            ):
                setattr(
                    result.restriction,
                    attribuut,
                    xs_restriction[element_key]["@value"],
                )

    return result


def _verwerk_en_voeg_simple_type_toe(
    xs_simple_type: dict,
    app_info: dict,
    datatypen: Dict[str, IStdDataType],
    foutrapport: List[str],
):
    """Verwerk het simpleType en voeg deze toe aan de lijst met IStdDataType"""

    try:
        simple_type = _verwerk_simple_type(xs_simple_type, app_info)
        datatypen[simple_type.naam] = simple_type
    except IStdException as caught:
        foutrapport.append(f"{str(caught)}")


def _verwerk_element(
    xs_element: dict, datatypes: Dict[str, IStdDataType]
) -> IStdElement:
    """Verwerkt een element uit de XSD"""

    if 0 == len(xs_element):
        raise ValueError("xs_element bevat geen items")
    if "@name" not in xs_element:
        raise IStdException("xs_element bevat geen attribuut '@name'")

    result = IStdElement(xs_element["@name"])
    if (
        "xs:annotation" in xs_element
        and "xs:documentation" in xs_element["xs:annotation"]
    ):
        result.beschrijving = get_doc_str(xs_element)

    # Wanneer het 'type' attribuut in het element aanwezig is, dan
    # het juiste IStdDataType zetten. Anders overslaan.
    if "@type" in xs_element:
        if xs_element["@type"].startswith("xs:"):
            result.datatype = xs_element["@type"]
        else:
            if xs_element["@type"] in datatypes:
                result.datatype = datatypes[xs_element["@type"]].naam
                result.is_subklasse = datatypes[xs_element["@type"]].is_complex
            else:
                result.datatype = xs_element["@type"]

    if "xs:pattern" in xs_element:
        result.patroon = xs_element["xs:pattern"]["@value"]
    if "xs:minInclusive" in xs_element:
        result.min_waarde = xs_element["xs:minInclusive"]["@value"]
    if "xs:maxInclusive" in xs_element:
        result.max_waarde = xs_element["xs:maxInclusive"]["@value"]
    if "@minOccurs" in xs_element:
        result.min_occurs = xs_element["@minOccurs"]
    if "@maxOccurs" in xs_element:
        result.max_occurs = xs_element["@maxOccurs"]

    return result


def _voeg_elementen_aan_complex_type_toe(
    xs_complex_type: dict,
    datatypes: Dict[str, IStdDataType],
    app_info: dict,
    foutrapport: List[str],
):
    """Voeg alle onderliggende elementen aan een complexType toe"""

    if "@name" not in xs_complex_type:
        raise IStdException("Geen 'name' attribuut gevonden in complexType")

    if "bericht" in app_info:
        bericht = app_info["bericht"].lower()
        type_naam = f"{bericht}:{xs_complex_type['@name']}"
    else:
        type_naam = f"{xs_complex_type['@name']}"

    if type_naam not in datatypes:
        raise IStdException(f"Geen datatype '{type_naam}' gevonden")

    complex_type = datatypes[type_naam]

    if (
        "xs:sequence" not in xs_complex_type
        or None is xs_complex_type.get("xs:sequence")
        or (
            "xs:sequence" in xs_complex_type
            and None is xs_complex_type["xs:sequence"].get("xs:element")
        )
    ):
        foutrapport.append(
            f"complexType '{xs_complex_type['@name']}' bevat geen "
            + "gegevenselementen"
        )
        # We gaan hieronder verder de xs:element children uit de xs:sequence
        # verwerken; dat heeft geen zin wanneer deze er niet zijn -> return
        return

    if isinstance(xs_complex_type["xs:sequence"]["xs:element"], dict):
        # Slechts 1 xs:element item onder xs:sequence
        xs_element = xs_complex_type["xs:sequence"]["xs:element"]
        try:
            element = _verwerk_element(xs_element, datatypes)
            complex_type.elementen[element.naam] = element
        except IStdException as caught:
            foutrapport.append(f"{str(caught)}")
    else:
        # Meerdere xs:element items onder xs:sequence
        for xs_element in xs_complex_type["xs:sequence"]["xs:element"]:
            try:
                element = _verwerk_element(xs_element, datatypes)
                complex_type.elementen[element.naam] = element
            except IStdException as caught:
                foutrapport.append(f"{str(caught)}")


def _voeg_elementen_aan_complex_types_toe(
    xs_schema: dict,
    datatypes: Dict[str, IStdDataType],
    app_info: dict,
    foutrapport: List[str],
):
    """Voeg alle onderliggende sequence/elementen toe aan alle complexType
    voorkomens"""

    if "xs:complexType" in xs_schema:
        if isinstance(xs_schema["xs:complexType"], dict):
            # Slechts één complexType
            xs_complex_type = xs_schema["xs:complexType"]
            try:
                _voeg_elementen_aan_complex_type_toe(
                    xs_complex_type, datatypes, app_info, foutrapport
                )
            except IStdException as caught:
                foutrapport.append(f"{str(caught)}")
        else:
            # Meerdere complexTypes
            for xs_complex_type in xs_schema["xs:complexType"]:
                try:
                    _voeg_elementen_aan_complex_type_toe(
                        xs_complex_type, datatypes, app_info, foutrapport
                    )
                except IStdException as caught:
                    foutrapport.append(f"{str(caught)}")


def _verwerk_data_typen(
    xs_schema: dict, app_info: dict, foutrapport: List[str]
) -> Dict[str, IStdDataType]:
    """Verwerk alle complex- en simple types uit de XSD"""

    result: Dict[str, IStdDataType] = {}
    if "xs:complexType" in xs_schema:
        if isinstance(xs_schema["xs:complexType"], dict):
            _verwerk_en_voeg_complex_type_toe(
                xs_schema["xs:complexType"], app_info, result
            )
        else:
            for xs_complex_type in xs_schema["xs:complexType"]:
                _verwerk_en_voeg_complex_type_toe(
                    xs_complex_type, app_info, result
                )

    if "xs:simpleType" in xs_schema:
        if isinstance(xs_schema["xs:simpleType"], dict):
            _verwerk_en_voeg_simple_type_toe(
                xs_schema["xs:simpleType"], app_info, result, foutrapport
            )
        else:
            for xs_simple_type in xs_schema["xs:simpleType"]:
                _verwerk_en_voeg_simple_type_toe(
                    xs_simple_type, app_info, result, foutrapport
                )

    _voeg_elementen_aan_complex_types_toe(
        xs_schema, result, app_info, foutrapport
    )

    return result


def _verwerk_datatype_elementen(
    datatype: IStdDataType,
    klasse: IStdKlasse,
    relaties: dict,
    datatypen: Dict[str, IStdDataType],
):
    """Verwerk alle elementen uit het datatype tot hetzij attributen, hetzij
    subklassen van de klasse"""

    if isinstance(datatype.elementen, dict):
        element: IStdElement = list(datatype.elementen.items())[0][1]

        if not element.is_subklasse:
            _verwerk_klasse_attribuut(element, klasse)
        else:
            _verwerk_subklasse_element(element, klasse, relaties, datatypen)
    else:
        for item in datatype.elementen:
            element: IStdElement = item
            if not element.is_subklasse:
                _verwerk_klasse_attribuut(element, klasse)
            else:
                _verwerk_subklasse_element(
                    element, klasse, relaties, datatypen
                )


def _verwerk_subklasse_element(
    element: IStdElement,
    bovenliggend: IStdKlasse,
    relaties: Dict[str, IStdAggregatieRelatie],
    datatypen: Dict[str, IStdDataType],
):
    """Verwerk een element die een subklasse van de bovenliggende klasse is"""

    subklasse = IStdKlasse(element.naam)
    bovenliggend.subklassen[element.naam] = subklasse

    # Relatie vastleggen
    relatie_naam = bovenliggend.naam + "_" + subklasse.naam
    relatie = IStdAggregatieRelatie(relatie_naam)
    relatie.child_min = element.min_occurs
    relatie.child_max = element.max_occurs
    relatie.parent_klasse = bovenliggend.naam
    relatie.child_klasse = element.naam
    relaties[relatie_naam] = relatie

    if element.datatype in datatypen:
        subklasse.datatype = datatypen[element.datatype]
        # Geef deze subklasse tenslotte haar attributen / subklasses
        _verwerk_datatype_elementen(
            subklasse.datatype, subklasse, relaties, datatypen
        )


def _verwerk_klasse_attribuut(element: IStdElement, klasse: IStdKlasse):
    """Verwerk een element wat een attribuut van de klasse is"""

    klasse.elementen[element.naam] = element


def _verwerk_bericht_documentatie(xs_schema: dict, bericht: IStdBericht):
    """Verwerk de beschrijving van het bericht uit de root annotatie"""

    if "xs:annotation" in xs_schema:
        if "xs:documentation" in xs_schema["xs:annotation"]:
            bericht.beschrijving = get_doc_str(xs_schema)


def _verwerk_root_element(
    xs_schema: dict,
    datatypes: Dict[str, IStdDataType],
    bericht: IStdBericht,
):
    """Verwerk het root element uit de berichtdefinitie"""

    if "xs:element" not in xs_schema:
        raise IStdException(
            "Er dient een root element in de berichtdefinitie opgenomen te "
            + "zijn"
        )
    root_element = xs_schema["xs:element"]
    if isinstance(root_element, list):
        raise IStdException(
            "Er dient slechts een root element in de berichtdefinitie "
            + "opgenomen te zijn"
        )

    root_klasse = IStdKlasse(root_element["@name"])
    if "xs:annotation" in root_element:
        if "xs:documentation" in root_element["xs:annotation"]:
            root_klasse.beschrijving = get_doc_str(root_element)

    bericht.klassen[root_element["@name"]] = root_klasse

    relaties: IStdAggregatieRelatie = {}

    xs_root_element_data_type = root_element["@type"]
    if -1 == xs_root_element_data_type.find("xs:"):
        if not {} == datatypes:
            if not root_element["@type"].startswith(bericht.naam.lower()):
                type_naam = f"{bericht.naam.lower()}:{root_element['@type']}"
            else:
                type_naam = root_element["@type"]

            if type_naam not in datatypes:
                raise IStdException(
                    f"Geen complex- of simpleType '{type_naam}' aangetroffen"
                )

            datatype: IStdDataType = datatypes[type_naam]

            root_klasse.datatype = datatype

            # Als het datatype een beschrijving heeft, dan deze overnemen in de
            # root klasse, TENZIJ de root klasse al een beschrijving heeft
            if not root_klasse.beschrijving:
                root_klasse.beschrijving = datatype.beschrijving

            # En nu alle elementen die onder het datatype hangen toevoegen aan
            # de root_klasse, hetzij als attribuut, hetzij als subklasse
            _verwerk_datatype_elementen(
                datatype, root_klasse, relaties, datatypes
            )

        else:
            raise NotImplementedError(
                "Types uit basisschema zijn nu nog niet ondersteund"
            )


def _verwerk_istandaard_xsd(xsd: str, foutrapport: List[str]) -> IStdBericht:
    """Verwerkt een XSD tot een berichtdefinitie"""

    if not xsd:
        raise ValueError("xsd not set")

    xml_dict_object = xmltodict.parse(xsd)

    if "xs:schema" not in xml_dict_object:
        raise ValueError(
            "No <xs:schema> element found in xsd\n"
            + "===================================\n"
            + f"{xsd}"
        )

    return _verwerk_berichtdefinitie(xml_dict_object["xs:schema"], foutrapport)


def _valideer_datatype_gebruik(
    xs_schema: dict, data_type_naam: str, niet_gevonden_datatypes: list
) -> bool:
    """Controleer dat data_type_naam ook daadwerkelijk gebruikt wordt in een
    of meer elementen.
    Retourneert False wanneer dit niet zo is, en voegt data_type_naam dan toe
    aan niet_gevonden_datatypes."""

    xsd_string = str(xs_schema)
    find_string_short = (
        f"'@type': '{data_type_naam[data_type_naam.find(':') + 1:]}'"
    )
    find_string_complete = f"'@type': '{data_type_naam}'"
    found_short = xsd_string.find(find_string_short)
    found_complete = xsd_string.find(find_string_complete)
    if -1 == found_short and -1 == found_complete:
        niet_gevonden_datatypes.append(data_type_naam)
        return False
    return True


def _valideer_datatypes_gebruik(
    xs_schema: dict, datatypes: dict, foutrapport: List[str]
):
    """Controleer dat alle datatypes welke expliciet definieerd zijn in de
    berichtstandaard ook daadwerkelijk gebruikt worden in een of meer
    elementen."""

    niet_gevonden_datatypes = []
    for key in datatypes:
        _valideer_datatype_gebruik(xs_schema, key, niet_gevonden_datatypes)

    if len(niet_gevonden_datatypes) > 0:
        fout = str(niet_gevonden_datatypes)
        foutrapport.append(
            "Deze gedefinieerde complexTypen worden niet gebruikt in deze "
            + f"berichtstandaard; {fout}"
        )


def _maak_bericht(app_info: dict, foutrapport: List[str]) -> IStdBericht:
    """Maakt een IStdBericht instantie aan op basis van de data uit app_info"""

    result = IStdBericht(app_info.get("bericht"))
    result.standaard = app_info.get("standaard")
    result.release = app_info.get("release")
    result.bericht_xsd_versie = app_info.get("BerichtXsdVersie")
    result.bericht_xsd_min_versie = app_info.get("BerichtXsdMinVersie")
    result.bericht_xsd_max_versie = app_info.get("BerichtXsdMaxVersie")
    result.basisschema_xsd_versie = app_info.get("BasisschemaXsdVersie")
    result.basisschema_xsd_min_versie = app_info.get("BasisschemaXsdMinVersie")
    result.basisschema_xsd_max_versie = app_info.get("BasisschemaXsdMaxVersie")
    if "bericht" not in app_info or "standaard" not in app_info:
        foutrapport.append(
            "Technische informatie items 'bericht' en 'standaard' zijn niet "
            + "beide aanwezig."
        )

    return result


def _verwerk_berichtdefinitie(
    xs_schema: dict, foutrapport: List[str]
) -> IStdBericht:
    """Verwerk de berichtdefinitie uit de XSD"""

    app_info = _verwerk_appinfo(xs_schema)
    bericht: IStdBericht = _maak_bericht(app_info, foutrapport)
    _verwerk_bericht_documentatie(xs_schema, bericht)
    datatypes = _verwerk_data_typen(xs_schema, app_info, foutrapport)

    _valideer_datatypes_gebruik(xs_schema, datatypes, foutrapport)

    try:
        _verwerk_root_element(xs_schema, datatypes, bericht)
    except IStdException as caught:
        foutrapport.append(f"{str(caught)}")

    return bericht


def __verwerk_element_type(
    xs_element_key: str,
    xs_element_item: dict,
    bericht: IStdBericht,
    klasse_naam: str,
    klasse_beschrijving: str,
):
    """Verwerk een element met een type-aanduiding"""
    relatie_prefix = bericht.naam.lower()
    element_prefix = bericht.standaard.lower()
    xs_element_type = str(xs_element_item["@type"])
    xs_element_name = str(xs_element_item["@name"])

    if xs_element_type.startswith(relatie_prefix + ":"):
        # Een Relatie-Element heeft een Type-prefix met de
        # <Bericht-naam> (bijv. "iwmo305:...")
        # Refactor: bericht.relaties is geen "platte" array
        #           maar een dictionary, dus zoeken op key
        #           i.p.v. op index
        __merge_relatie(bericht.relaties, xs_element_name)
        parent_klasse = klasse_naam
        bericht.relaties[xs_element_name].parent_klasse = parent_klasse
        if parent_klasse == "Root":
            # Root-Klasse bevat alleen directe relaties met
            # de Header en een inhoudelijke Klasse
            bericht.relaties[xs_element_key].child_klasse = xs_element_name
        if "@minOccurs" in xs_element_item:
            bericht.relaties[xs_element_name].child_min = xs_element_item[
                "@minOccurs"
            ]

    elif xs_element_type.startswith(element_prefix + ":"):
        # Een Element zonder vaste waarde heeft een
        # Type-prefix met de <Standaard> (bijv. "iwmo:...")
        # Refactor: we hebben de indexes van de klasse
        # resp. element niet meer nodig, omdat het geen
        # "platte" array meer is, maar een dictionary
        __add_klasse_element(
            bericht,
            klasse_naam,
            klasse_beschrijving,
            xs_element_name,
        )

        if "xs:annotation" in xs_element_item:
            # Refactor: klassen is geen "platte" array,
            #           maar een dictionary.
            bericht.klassen[klasse_naam].elementen[
                xs_element_name
            ].beschrijving = get_doc_str(xs_element_item)
        else:
            print("Waarschuwing - Geen xs:annotation gevonden!")
            # Refactor: klassen is geen "platte" array
            #           maar een dictionary
            print(f"bericht.klasse: {bericht.klassen[klasse_naam].naam}")
            print(f"bericht.klasse.element: {str(xs_element_item)}")

        # Refactor: klassen is geen "platte" array maar een
        #           dictionary
        bericht.klassen[klasse_naam].elementen[
            xs_element_name
        ].datatype = xs_element_type.split(":")[1]
    else:
        # Deze situatie is onbekend??
        print(f'Onbekend Element Type in Bericht : "{bericht.naam}"')
        print(f'<xs:element type+"{xs_element_type}">')


def __verwerk_sequence(
    xs_sequence: dict,
    bericht: IStdBericht,
    klasse_naam: str,
    klasse_beschrijving: str,
):
    """Verwerk een xs:sequence, en voeg de elementen etc. hieronder toe aan
    het ComplexType waarin de sequence is opgenomen."""
    for seq_key in xs_sequence:
        # Een Sequence kan meerdere (type => list) of 1 Element (dict) be-
        # vatten.
        # Refactor: we gebruiken de len method om vast te stellen of er 1
        #           of meer xs:element items zijn
        if isinstance(xs_sequence["xs:element"], list):
            # Meerdere Elementen in de Sequence
            for xs_element_key in xs_sequence[seq_key]:
                xs_element_item = xs_sequence[seq_key][xs_element_key]
                xs_element_name = str(xs_element_item["@name"])

                if "@type" in xs_element_item:
                    # Een Klasse-Element heeft een Type-aanduiding
                    __verwerk_element_type(
                        xs_element_key,
                        xs_element_item,
                        bericht,
                        klasse_naam,
                        klasse_beschrijving,
                    )
                else:
                    # Een Element met een vaste Waarde heeft GEEN
                    # Type-aanduiding
                    xs_simple_type = xs_element_item["xs:simpleType"]
                    xs_restriction = xs_simple_type["xs:restriction"]
                    xs_datatype = str(xs_restriction["@base"])
                    # Refactor: klassen is geen "platte" array maar een
                    #           dictionary. We werken dus niet meer met de
                    #           indexes maar met de dictionary key
                    __add_klasse_element(
                        bericht,
                        klasse_naam,
                        klasse_beschrijving,
                        xs_element_name,
                    )
                    bericht.klassen[klasse_naam].elementen[
                        xs_element_name
                    ].beschrijving = get_doc_str(xs_element_item)
                    bericht.klassen[klasse_naam].elementen[
                        xs_element_name
                    ].datatype = xs_datatype.split(":")[1]
        else:
            __add_klasse_element(
                bericht,
                klasse_naam,
                klasse_beschrijving,
                xs_sequence["xs:element"]["@name"],
            )
            relatie_naam = klasse_naam
            __merge_relatie(bericht.relaties, relatie_naam)
            relatie_element = xs_sequence["xs:element"]
            bericht.relaties[relatie_naam].child_klasse = relatie_element[
                "@name"
            ]
            if "@maxOccurs" in relatie_element:
                bericht.relaties[relatie_naam].child_max = relatie_element[
                    "@maxOccurs"
                ]


def _verwerk_appinfo(schema: dict) -> dict:
    """Verwerk de items zoals opgenomen in de xs:annotation/xs:appinfo
    elementen van het XSD schema"""

    result = {}
    # Bugfix: als er geen xs:annotation met daaronder een xs:appinfo bestaat in
    #         de XSD, dan kunnen we dat niet importeren
    if "xs:annotation" in schema and "xs:appinfo" in schema["xs:annotation"]:
        app_info = schema["xs:annotation"]["xs:appinfo"]
        # Bugfix: de namespace van de items in xs:annotation/xs:appinfo moet
        #         niet de eerstvolgende VALUE, maar het eerstvolgende deel van
        #         de KEY tot de dubbele punt, zijn.
        namespaces = app_info.keys()
        namespaces_iterator = iter(namespaces)
        namespace = next(namespaces_iterator)
        try:
            namespace = namespace[0 : namespace.index(":")]  # noqa: E203
        except ValueError:
            namespace = None

        result["bericht"] = get_xml_object_value(
            app_info, namespace, "bericht"
        )
        result["standaard"] = get_xml_object_value(
            app_info, namespace, "standaard"
        )
        result["release"] = get_xml_object_value(
            app_info, namespace, "release"
        )
        result["BerichtXsdVersie"] = get_xml_object_value(
            app_info, namespace, "BerichtXsdVersie"
        )
        result["BerichtXsdMinVersie"] = get_xml_object_value(
            app_info, namespace, "BerichtXsdMinVersie"
        )
        result["BerichtXsdMaxVersie"] = get_xml_object_value(
            app_info, namespace, "BerichtXsdMaxVersie"
        )
        result["BasisschemaXsdVersie"] = get_xml_object_value(
            app_info, namespace, "BasisschemaXsdVersie"
        )
        result["BasisschemaXsdMinVersie"] = get_xml_object_value(
            app_info, namespace, "BasisschemaXsdMinVersie"
        )
        result["BasisschemaXsdMaxVersie"] = get_xml_object_value(
            app_info, namespace, "BasisschemaXsdMaxVersie"
        )

    return result


def import_bericht_xsd(bericht_xsd_file_path: str) -> IStdBericht:
    """Importeert XSD-Bericht als IstdBericht-Class."""

    with open(bericht_xsd_file_path, encoding="UTF-8") as xml_file:
        xml_lines = ""
        for xml_line in xml_file.readlines():
            xml_lines = xml_lines + xml_line + "\n"

        if 0 == len(xml_lines):
            raise IStdException(f"{bericht_xsd_file_path} is leeg")

        try:
            foutrapport: List[str] = []
            result: IStdBericht = _verwerk_istandaard_xsd(
                xml_lines, foutrapport
            )
            if len(foutrapport) > 0:
                builder: str = (
                    f"# {bericht_xsd_file_path}  \n\n"
                    "De volgende designfouten zijn geconstateerd  "
                )
                for fout in foutrapport:
                    builder = f"{builder}\n- {fout}  "

                raise IStdException(builder)
            return result
        except ExpatError as xml_error:
            raise IStdException(
                f"{bericht_xsd_file_path}:\nInvalide xml op regel "
                + f"{xml_error.lineno} en kolom {xml_error.offset}\n"
                + f"==============================\n{xml_lines}"
            ) from xml_error


def add_complex_type(xs_complex_type: dict, data_typen: IStdDataType = None):
    """Voeg op basis van een xsComplexType een IstdDataType-Instantie
    toe (met de Elementen)."""

    # Voorkomen pylint melding W0102.
    # Zie http://pylint-messages.wikidot.com/messages:w0102
    data_typen = data_typen or []

    data_typen.append(IStdDataType(str(xs_complex_type["@name"])))
    last_data_type = len(data_typen) - 1

    if "xs:annotation" in xs_complex_type:
        data_typen[last_data_type].beschrijving = get_doc_str(xs_complex_type)

    # Mapping van waarden in de Klasse-Sequence
    xs_seq = xs_complex_type["xs:sequence"]

    for seq_key in xs_seq:
        # Een Sequence kan meerdere (type => list)
        # of 1 Element bevatten (type => collections.OrderedDict)
        dict_type = get_xml_dict_type(xs_seq, seq_key)
        if dict_type == "<class 'list'>":
            # Meerdere Elementen in de Sequence
            for xs_element in xs_seq[seq_key]:
                xs_element_name = str(xs_element["@name"])
                data_typen[last_data_type].elementen.append(
                    IStdElement(xs_element_name)
                )
                last_element = len(data_typen[last_data_type].elementen) - 1

                if "@type" in xs_element:
                    # Element zonder restrictie
                    xs_element_data_type = str(xs_element["@type"])
                else:
                    # Element met restrictie
                    xs_simple_type = xs_element["xs:simpleType"]
                    xs_restriction = xs_simple_type["xs:restriction"]
                    xs_data_type = str(xs_restriction["@base"])
                    xs_element_data_type = xs_data_type

                    if "xs:pattern" in xs_restriction:
                        data_typen[last_data_type].elementen[
                            last_element
                        ].waarde = str(xs_restriction["xs:pattern"]["@value"])
                    elif "xs:maxInclusive" in xs_restriction:
                        data_typen[last_data_type].elementen[
                            last_element
                        ].maxWaarde = str(
                            xs_restriction["xs:maxInclusive"]["@value"]
                        )

                data_typen[last_data_type].elementen[
                    last_element
                ].dataType = xs_element_data_type.split(":")[1]

                if "xs:annotation" in xs_element:
                    data_typen[last_data_type].elementen[
                        last_element
                    ].beschrijving = get_doc_str(xs_element)

                if "@minOccurs" in xs_element:
                    data_typen[last_data_type].elementen[
                        last_element
                    ].verplicht = (
                        str(xs_element["@minOccurs"]) != "0"
                    )  # pylint:disable=[C0325]

        elif dict_type == "<class 'collections.OrderedDict'>":
            # Een Sequence met 1 Element is een Klasse-Relatie Element
            print("en Sequence met 1 Element")
            print(xs_seq[seq_key])


def add_simple_type(xs_simple_type: dict, data_typen: IStdDataType = None):
    """Voeg op basis van een xsSimpleType een IstdDataType-Instantie toe."""

    # Voorkomen pylint melding W0102.
    # Zie http://pylint-messages.wikidot.com/messages:w0102
    data_typen = data_typen or []
    data_typen.append(IStdDataType(str(xs_simple_type["@name"])))
    last_data_type = len(data_typen) - 1

    if "xs:annotation" in xs_simple_type:
        data_typen[last_data_type].beschrijving = get_doc_str(xs_simple_type)

    xs_restriction = xs_simple_type["xs:restriction"]
    xs_restriction_base: str = xs_restriction["@base"]
    data_typen[last_data_type].basisType = xs_restriction_base.split(":")[1]

    if "xs:minLength" in xs_restriction:
        data_typen[last_data_type].minLengte = xs_restriction["xs:minLength"][
            "@value"
        ]
    if "xs:maxLength" in xs_restriction:
        data_typen[last_data_type].maxLengte = xs_restriction["xs:maxLength"][
            "@value"
        ]
    if "xs:minInclusive" in xs_restriction:
        data_typen[last_data_type].minWaarde = xs_restriction[
            "xs:minInclusive"
        ]["@value"]
    if "xs:maxInclusive" in xs_restriction:
        data_typen[last_data_type].maxWaarde = xs_restriction[
            "xs:maxInclusive"
        ]["@value"]
    if "xs:pattern" in xs_restriction:
        data_typen[last_data_type].waarde = xs_restriction["xs:pattern"][
            "@value"
        ]


def import_basisschema_xsd(
    basisschema_xsd_file_path: str,
) -> Tuple[IStdBasisSchema, List[IStdDataType]]:
    """Importeert XSD-basisschema als IstdDataType-Class Lijst."""

    # Lees Basisschema-gegevens in Dict-structuur Object
    data_typen: IStdDataType = []
    xml_dict_object = xml_file_to_dict(basisschema_xsd_file_path)

    # Lees XSD-Schema
    schema = xml_dict_object["xs:schema"]

    # Converteer XSD-AppInfo waarden
    app_info = _verwerk_appinfo(schema)
    basis_schema = IStdBasisSchema("")
    if len(app_info) > 0:
        basis_schema.standaard = app_info["standaard"]
        basis_schema.release = app_info["release"]
        basis_schema.xsd_versie = app_info["BasisschemaXsdVersie"]
        basis_schema.xsd_min_versie = app_info["BasisschemaXsdMinVersie"]
        basis_schema.xsd_max_versie = app_info["BasisschemaXsdMaxVersie"]

    for xs_complex_type in schema["xs:complexType"]:
        add_complex_type(xs_complex_type, data_typen)

    for xs_simple_type in schema["xs:simpleType"]:
        add_simple_type(xs_simple_type, data_typen)

    return basis_schema, data_typen
