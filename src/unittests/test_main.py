"""Unittests for the main module of the istandaard_naar_hugo package"""

# pylint:disable=[W0212]

import os
import unittest
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Dict, List
from unittest.mock import mock_open, patch

from nose.tools import assert_equal, assert_raises, assert_true

from src import __main__
from src.istandaard.classes import IStdBericht, IStdCodeLijst, IStdRegel


def _return_false(dummy=None):  # pylint:disable=[W0613]
    return False


def _return_true(dummy=None):  # pylint:disable=[W0613]
    return True


r_false = _return_false
r_true = _return_true


class ProcessRegelrapportTestCase(unittest.TestCase):
    """Unittest die zich richten op de functionaliteit waarmee een regelrapport
    verewrkt wordt"""


class ProcessIStandaardTestCase(unittest.TestCase):
    """Unittests die zich richten op de functionaliteit waarmee
    een berichtstandaard of een map met berichtstandaarden verwerkt wordt"""

    def test_check_xsd_file(self):
        """Controleer dat het meegegeven pad naar de xsd file, goed werkt"""

        # Eerst faken dat het pad niet bestaat
        with patch.multiple(Path, exists=r_false, is_file=r_true):
            with assert_raises(FileNotFoundError):
                __main__._check_xsd_file("mock")

        # Dan faken dat het pad wel bestaat, maar dat het geen bestand is
        with patch.multiple(Path, exists=r_true, is_file=r_false):
            with assert_raises(FileNotFoundError):
                __main__._check_xsd_file("mock")

        # Tenslotte faken dat het pad bestaat EN het een bestand is. Dan gaan
        # we testen met bestandsnamen die al dan niet eindigen op .xsd (.XSD)
        with patch.multiple(Path, exists=r_true, is_file=r_true):
            with assert_raises(ValueError):
                __main__._check_xsd_file("mock")
            with assert_raises(ValueError):
                __main__._check_xsd_file("mock.txt")
            __main__._check_xsd_file("mock.xsd")
            __main__._check_xsd_file("mock.XSD")
            __main__._check_xsd_file("mock.XsD")

    def test_verwerk_istandaard(self):
        """Controleer dat import_bericht_xsd wordt aangeroepen voor elk voor-
        komen van istandaard argument"""

        with patch.multiple(Path, exists=r_true, is_file=r_true):
            with patch.object(__main__, "import_bericht_xsd") as mocked_import:
                __main__.main("--istandaard blah.xsd")
                mocked_import.assert_called_once_with("blah.xsd")

                mocked_import.reset_mock()
                __main__.main("--istandaard blah.xsd blah_two.xsd")
                assert_equal(mocked_import.call_count, 2)

                mocked_import.reset_mock()
                __main__.main("--regels b")
                mocked_import.assert_not_called()


class FoutrapportTestCase(unittest.TestCase):
    """Unittests die zich richten op het genereren van een foutrapport"""

    def test_foutrapport(self):
        """Controleer dat, wanneer een of meer fouten in foutrapport zijn
        opgenomen, dit leidt tot een SystemExit."""

        def _mock_foutrapport(*args):  # pylint:disable=[W0613]
            __main__.foutrapport.append("Mocked foutrapport")

        with patch.multiple(Path, exists=r_true, is_file=r_true):
            with patch.object(
                __main__, "import_bericht_xsd", side_effect=_mock_foutrapport
            ):
                with assert_raises(SystemExit) as context:
                    __main__.main("--istandaard blah.xsd")
                assert_equal(2, context.exception.code)


class YamlOutputTestCase(unittest.TestCase):
    """Unittests die zich richten op het exporteren van de berichtstandaard in
    YAML formaat"""

    def test_transformeer_naar_yaml_aangeroepen(self):
        """Controleer dat, wanneer yaml-output-folder is gezet, voor elk
        te transformeren object eenmaal transformeer_object_naar_yaml wordt
        aangeroepen."""

        with patch.multiple(Path, exists=r_true, is_file=r_true):
            with patch("os.makedirs"):
                # Eerst de berichten testen
                fake_berichten: List[IStdBericht] = []
                fake_regels: List[IStdRegel] = []
                fake_codelijsten: List[IStdCodeLijst] = []
                with patch.object(
                    __main__, "transformeer_object_naar_yaml"
                ) as transform_yaml:
                    with patch("builtins.open", mock_open()):
                        fake_berichten = [IStdBericht("Fake")]
                        with patch.object(
                            __main__,
                            "_check_and_process_istandaarden",
                            return_value=fake_berichten,
                        ):
                            __main__.main(
                                "--istandaard Fake.xsd --yaml-output-folder "
                                + "blah"
                            )
                            transform_yaml.assert_called_once_with(
                                fake_berichten[0]
                            )

                            transform_yaml.reset_mock()

                        fake_berichten = [
                            IStdBericht("Fake1"),
                            IStdBericht("Fake2"),
                            IStdBericht("Fake3"),
                        ]
                        with patch.object(
                            __main__,
                            "_check_and_process_istandaarden",
                            return_value=fake_berichten,
                        ):
                            # Nu testen, dat als er meerdere berichten zijn,
                            # voor # elk bericht ook een keer
                            #  transformeer_object_naar_yaml wordt aangeroepen.
                            istandaard_arg: str = "--istandaard"
                            for fake in fake_berichten:
                                istandaard_arg = (
                                    f'{istandaard_arg} "{fake}.xsd"'
                                )
                            arg = f"{istandaard_arg} --yaml-output-folder blah"
                            __main__.main(arg)
                            assert_equal(
                                len(fake_berichten), transform_yaml.call_count
                            )
                            transform_yaml.reset_mock()

                        fake_regels = [IStdRegel(), IStdRegel(), IStdRegel()]
                        with patch.object(
                            __main__,
                            "_check_and_process_berichtregels",
                            return_value=fake_regels,
                        ):
                            with patch.object(
                                __main__,
                                "_geef_subfolder_voor_regel",
                                return_value="fake-folder",
                            ):
                                arg = (
                                    '--berichtregels "blah.xslx" '
                                    + "--yaml-output-folder blah"
                                )
                                __main__.main(arg)
                                assert_equal(
                                    len(fake_regels), transform_yaml.call_count
                                )
                                transform_yaml.reset_mock()

                        fake_codelijsten = [
                            IStdCodeLijst("FakeLijst", None),
                            IStdCodeLijst("FakeLijst1", None),
                            IStdCodeLijst("FakeLijst2", None),
                        ]
                        with patch.object(
                            __main__,
                            "_check_and_process_codelijst",
                            return_value=fake_codelijsten,
                        ):
                            arg = (
                                '--codelijsten "blah.xslx" '
                                + "--yaml-output-folder blah"
                            )
                            __main__.main(arg)
                            assert_equal(
                                len(fake_codelijsten),
                                transform_yaml.call_count,
                            )
                            transform_yaml.reset_mock()

    def test_geef_subfolder_voor_regel(self):
        """Controleer dat, aan de hand van de code van de regel, de juiste sub-
        folder gegenereerd wordt waarin de YAML voor de regel geplaatst moet
        worden"""

        testcases: Dict(str) = {
            "UP": "Uitgangspunt",
            "OP": "Bedrijfsregel",
            "TR": "Technische regel",
            "IV": "Invulinstructie",
            "CS": "Constraint",
            "CD": "Conditie",
        }

        for code, expected in testcases.items():
            fake_regel: IStdRegel = IStdRegel()
            fake_regel.code = f"{code}0001"
            assert_equal(
                expected, __main__._geef_subfolder_voor_regel(fake_regel)
            )

    def test_write_yaml_output(self):
        """Controleer de werking van het daadwerkelijk wegschrijven van de
        gegenereerde YAML."""

        # Wanneer er geen folder(s) in het pad naar de outputfile staan, dan
        # mag er geen call plaatsvinden om de folder aan te maken (want het
        # moet in de huidige folder gebeuren).
        with patch.object(os, "makedirs", autospec=True) as tester:
            with patch("builtins.open", mock_open()):
                __main__._write_yaml_output(
                    "a: a\nb:\n- Item 1\n- Item 2", "test.yml"
                )
                tester.assert_not_called()

        # Wanneer er wel folder(s) in het pad naar de outputfile staan EN de
        # folder(s) bestaan (mocken), dan mag er geen call plaatsvinden om de
        # folder aan te maken (want die bestaat al)
        with patch.object(os, "makedirs", autospec=True) as tester:
            with patch.object(os.path, "exists", return_value=True):
                with patch("builtins.open", mock_open()):
                    __main__._write_yaml_output(
                        "a: a\nb:\n- Item 1\n- Item 2", "blah/testing/test.yml"
                    )
                    tester.assert_not_called()

        # Wanneer er wel folder(s) in het pad naar de outputfile staan maar de
        # folder(s) bestaan niet (mocken), dan moet er een call plaatsvinden om
        # de folder aan te maken.
        with patch.object(os, "makedirs", autospec=True) as tester:
            with patch.object(os.path, "exists", return_value=False):
                with patch("builtins.open", mock_open()):
                    __main__._write_yaml_output(
                        "a: a\nb:\n- Item 1\n- Item 2", "blah/testing/test.yml"
                    )
                    tester.assert_called_once_with("blah/testing")


class ArgumentsTestCase(unittest.TestCase):
    """Unittests die zich richten op de validiteit van de meegegeven
    argumenten"""

    def test_einde_indien_geen_argumenten(self):
        """Testen dat wanneer er geen argumenten worden meegegeven, er met -1
        afgesloten wordt"""

        with assert_raises(SystemExit) as context:
            __main__.main("")
        assert_equal(context.exception.code, -1)

    # pylint:disable=[W0212]
    def test_setup_arguments(self):
        """Testen dat de method _setup_arguments tot gevolg heeft dat uit-
        sluitend de juiste 6 argumenten zijn toegevoegd aan de parser:
        --istandaard OF --istandaard-input-folder
        --basisschema
        --codelijsten
        --berichtregels
        --regels
        --yaml-output-folder"""

        parser = ArgumentParser(
            prog="test_setup_arguments",
        )
        __main__._setup_arguments(parser)
        args: dict = parser._option_string_actions
        # Eerst de -h en --help opties verwijderen; die krijg je standaard
        # "cadeau"
        if "-h" in args or "--help" in args:
            del args["-h"]
            del args["--help"]

        assert_equal(len(args), 7)
        assert_true("--istandaard" in args, "--istandaard niet aangetroffen")
        assert_true(
            "--istandaard-input-folder" in args,
            "--istandaard-input-folder niet aangetroffen",
        )
        assert_true("--basisschema" in args, "--basisschema niet aangetroffen")
        assert_true("--codelijsten" in args, "--codelijsten niet aangetroffen")
        assert_true(
            "--berichtregels" in args, "--berichtregels niet aangetroffen"
        )
        assert_true("--regels" in args, "--regels niet aangetroffen")
        assert_true(
            "--yaml-output-folder" in args,
            "--yaml-output-folder niet aangetroffen",
        )

    @patch(
        "argparse.ArgumentParser.parse_args",
        return_value=Namespace(
            istandaard=None,
            istandaard_input_folder=None,
            basisschema=None,
            codelijsten=None,
            berichtregels=None,
            regels=None,
        ),
    )
    # pylint:disable=[W0613]
    def test_check_arguments(self, mocked_arguments):
        """Controleer dat het programma netjes stopt wanneer er geen input-
        argumenten zijn meegegeven bij de aanroep, maar ook netjes doorloopt
        wanneer een of meer argumenten zijn meegegeven"""

        test_parser = ArgumentParser(
            "test_check_arguments_no_arguments_provided"
        )
        __main__._setup_arguments(test_parser)

        with assert_raises(SystemExit) as context:
            __main__._check_arguments(test_parser.parse_args(""))
        assert_equal(context.exception.code, -1)

        # Nu testen met steeds 1 argument gevuld; er moet geen exception
        # optreden.
        # Let op: parse_args doet hier niets, we gebruiken de mock!
        items = [
            "istandaard",
            "basisschema",
            "codelijsten",
            "berichtregels",
            "regels",
        ]
        for item in items:
            setattr(mocked_arguments.return_value, item, "test")
            __main__._check_arguments(test_parser.parse_args(""))

            for item2 in items:
                if item2 != item:
                    setattr(mocked_arguments.return_value, item2, "test")
                    __main__._check_arguments(test_parser.parse_args(""))

                    for item3 in items:
                        if item3 != item2 and item3 != item:
                            setattr(
                                mocked_arguments.return_value, item3, "test"
                            )
                            __main__._check_arguments(
                                test_parser.parse_args("")
                            )

                            for item4 in items:
                                if (
                                    item4 != item3
                                    and item4 != item2
                                    and item4 != item
                                ):
                                    setattr(
                                        mocked_arguments.return_value,
                                        item4,
                                        "test",
                                    )
                                    __main__._check_arguments(
                                        test_parser.parse_args("")
                                    )

                                    for item5 in items:
                                        if (
                                            item5 != item4
                                            and item5 != item3
                                            and item5 != item2
                                            and item5 != item
                                        ):
                                            setattr(
                                                mocked_arguments.return_value,
                                                item5,
                                                "test",
                                            )
                                            __main__._check_arguments(
                                                test_parser.parse_args("")
                                            )

                                            setattr(
                                                mocked_arguments.return_value,
                                                item5,
                                                None,
                                            )

                                    setattr(
                                        mocked_arguments.return_value,
                                        item4,
                                        None,
                                    )

                            setattr(mocked_arguments.return_value, item3, None)

                    setattr(mocked_arguments.return_value, item2, None)

            setattr(mocked_arguments.return_value, item, None)

    def test_istandaard_icm_istandaard_input_folder(self):
        """Testen dat er netjes een foutmelding gegeven wordt wanneer zowel
        --istandaard alsook --istandaard-input-folder gevuld zijn"""
        # Testen dat er een foutmelding wordt gegeven, wanneer zowel
        # --istandaard als --istandaard-input-folder gezet zijn
        with assert_raises(SystemExit) as context:
            __main__.main(
                '--istandaard "blah.xsd" '
                + '--istandaard-input-folder "/fake/folder"'
            )
        assert_equal(context.exception.code, 2)
