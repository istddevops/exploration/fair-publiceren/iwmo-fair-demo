"""Unittests voor het genereren van markdown o.b.v. behave json output"""


import unittest
from argparse import ArgumentParser

from nose.tools import assert_equal

from src.livingdoc import create_markdown_for_features


class TransformeerJsonNaarMarkdown(unittest.TestCase):
    """Testcase over het transformeren van behave json naar markdown"""

    def test_arguments(self):
        """Unittest waarmee wordt getest dat de argumenten op de juiste wijze
        worden gebruikt"""

        json_input: str = "fake.json"
        output_folder: str = "./fake"

        testee = create_markdown_for_features._setup_arguments(
            ArgumentParser()
        )
        options = testee.parse_args([json_input, output_folder])
        assert hasattr(options, "input_json")
        assert hasattr(options, "output_folder")
        assert_equal(options.input_json, json_input)
        assert_equal(options.output_folder, output_folder)
