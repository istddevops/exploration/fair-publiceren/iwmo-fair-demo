"""Unittests for import_bizz_xsds module of the istandaard_naar_hugo package"""

import unittest
from typing import Dict, List
from unittest.mock import DEFAULT, mock_open, patch
from xml.parsers.expat import ExpatError

import xmltodict
from nose.tools import assert_equal, assert_raises

from src import import_bizz_xsds
from src.istandaard.classes import (
    IStdBericht,
    IStdDataType,
    IStdElement,
    IStdException,
)


def throw_istd_exception(*args):
    """Dummy test helper, die een IStdException gooit"""
    raise IStdException()


# pylint:disable=[W0212]
class BerichtDefinitieTestCase(unittest.TestCase):
    """Unittests die zich richten op het verwerken van een berichtdefinitie
    XSD naar een IStdBericht klasse"""

    def test_get_doc_str(self):
        """Unittest voor get_doc_str method

        Testen dat de methode er netjes mee omgaat wanneer
        1.  Er geen documentation annotation is
        1.a Er is geen annotation
        1.b Er is wel een annotation, maar geen documentation
        2.  Er een documentation annotation is met 1 regel
        3.  Er een documentation annotation is met meer regels:
        3.a verspreid over meerdere documentation elementen
        3.b in een documentation element verspreid over meerdere regels"""

        # 1.  Er geen documentation annotation is
        # 1.a Er is geen annotation
        fake_type = {"@name": "Fake"}
        assert_equal(None, import_bizz_xsds.get_doc_str(fake_type))

        # 1.b Er is wel een annotation, maar geen documentation
        fake_type = {"@name": "Fake", "xs:annotation": {}}
        assert_equal(None, import_bizz_xsds.get_doc_str(fake_type))

        # 2.  Er een documentation annotation is met 1 regel
        expected = "Fake documentation"
        fake_type = {
            "@name": "Fake",
            "xs:annotation": {"xs:documentation": expected},
        }
        assert_equal(expected, import_bizz_xsds.get_doc_str(fake_type))

        # 3.  Er een documentation annotation is met meer regels:
        # 3.a verspreid over meerdere documentation elementen
        expected = "Fake documentation, line 1\nFake documentation, line 2"
        fake_type = {
            "@name": "Fake",
            "xs:annotation": {"xs:documentation": expected.split("\n")},
        }
        assert_equal(expected, import_bizz_xsds.get_doc_str(fake_type))

        # 3.b in een documentation element verspreid over meerdere regels
        expected = "Fake documentation, line 1\nFake documentation, line 2"
        fake_type = {
            "@name": "Fake",
            "xs:annotation": {"xs:documentation": expected},
        }
        assert_equal(expected, import_bizz_xsds.get_doc_str(fake_type))

    def test_import_bericht_xsd(self):
        """Unittest voor import_bericht_xsd method"""

        # Testen dat de methode er netjes mee omgaat wanneer de XSD file niet
        # geopend kan worden, geen valide XSD is, etc.

        # 1. File bestaat niet
        with assert_raises(FileNotFoundError):
            import_bizz_xsds.import_bericht_xsd("/fake")

        # 2. File is geen valide xsd bestand
        with patch(
            "builtins.open",
            mock_open(read_data="Dit is geen XML, maar platte tekst"),
        ):
            with assert_raises(IStdException):
                import_bizz_xsds.import_bericht_xsd("dummy")

        # 3. File is leeg
        with patch("builtins.open", mock_open(read_data="")):
            with assert_raises(IStdException):
                import_bizz_xsds.import_bericht_xsd("dummy")

        # 4. File is gevuld met een XSD wordt afgetest middels de testauto-
        #    matisering die hoort bij de feature files.

        # 5. Vaststellen dat, wanneer foutrapport gevuld teruggegeven wordt
        #    door _verwerk_istandaard_xsd, dit leidt tot een IStdException
        # pylint:disable=[W0613]
        def _side_effect_mock(dummy: str, foutrapport: List[str]):
            foutrapport.append("Mocked fout")

        with patch.object(
            import_bizz_xsds,
            "_verwerk_istandaard_xsd",
            side_effect=_side_effect_mock,
        ):
            with patch(
                "builtins.open",
                mock_open(
                    read_data="""<?xml version="1.0"?>
<xs:schema><xs:element name="Mock" /></xs:schema>"""
                ),
            ):
                with assert_raises(IStdException):
                    import_bizz_xsds.import_bericht_xsd("mock.xsd")

    def test_verwerk_istandaard_xsd(self):
        """Unittest voor _verwerk_istandaard_xsd method"""

        # Testen dat de methode een ExpatError geeft wanneer er invalide
        # xml ingestopt wordt
        with assert_raises(ExpatError):
            import_bizz_xsds._verwerk_istandaard_xsd("Dit is geen XML", [])

        with assert_raises(ExpatError):
            import_bizz_xsds._verwerk_istandaard_xsd(
                "<opentag>Maar we vergeten de sluittag", []
            )

        # Testen dat de methode een ArgumentError geeft wanneer er een lege
        # string als input gegeven wordt
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_istandaard_xsd(None, [])
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_istandaard_xsd("", [])

        # Testen dat de methode een ValueError geeft wanneer er wel geldige
        # xml in gaat, maar deze geen xs:schema element bevat
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_istandaard_xsd(
                """<?xml version="1.0"?>
            <root>Dit is geldige xml, maar geen schema</root>""",
                [],
            )

        # Testen dat deze methode _verwerk_berichtdefinitie aanroept wanneer
        # er geldige XML in gestopt is
        with patch.object(
            import_bizz_xsds, "_verwerk_berichtdefinitie"
        ) as mocked_import:
            # flake8: noqa(E501)
            import_bizz_xsds._verwerk_istandaard_xsd(
                """<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:iwmo="http://www.istandaarden.nl/iwmo/3_0/basisschema/schema"
           xmlns:wmo301="http://www.istandaarden.nl/iwmo/3_0/wmo301/schema"
           targetNamespace="http://www.istandaarden.nl/iwmo/3_0/wmo301/schema"
           elementFormDefault="qualified">
    <xs:import namespace="http://www.istandaarden.nl/iwmo/3_0/basisschema/schema"
               schemaLocation="basisschema.xsd" />
    <xs:element name="Tester" type="xs:string" />
</xs:schema>""",
                [],
            )
            mocked_import.assert_called_once()

    def test_maak_bericht(self):
        """Unittest voor __maak_bericht method"""

        # Vaststellen dat de properties uit de appinfo correct overgenomen
        # worden in de IStdBericht instantie

        app_info: dict = {
            "bericht": "bericht_test",
            "standaard": "standaard_test",
            "release": "release_test",
            "BerichtXsdVersie": "BerichtXsdVersie_test",
            "BerichtXsdMinVersie": "BerichtXsdMinVersie_test",
            "BerichtXsdMaxVersie": "BerichtXsdMaxVersie_test",
            "BasisschemaXsdVersie": "BasisschemaXsdVersie_test",
            "BasisschemaXsdMinVersie": "BasisschemaXsdMinVersie_test",
            "BasisschemaXsdMaxVersie": "BasisschemaXsdMaxVersie_test",
        }

        foutrapport: List[str] = []
        testable: IStdBericht = import_bizz_xsds._maak_bericht(
            app_info, foutrapport
        )

        assert_equal(testable.naam, "bericht_test")
        assert_equal(testable.standaard, "standaard_test")
        assert_equal(testable.release, "release_test")
        assert_equal(testable.bericht_xsd_versie, "BerichtXsdVersie_test")
        assert_equal(
            testable.bericht_xsd_min_versie,
            "BerichtXsdMinVersie_test",
        )
        assert_equal(
            testable.bericht_xsd_max_versie,
            "BerichtXsdMaxVersie_test",
        )
        assert_equal(
            testable.basisschema_xsd_versie,
            "BasisschemaXsdVersie_test",
        )
        assert_equal(
            testable.basisschema_xsd_min_versie,
            "BasisschemaXsdMinVersie_test",
        )
        assert_equal(
            testable.basisschema_xsd_max_versie,
            "BasisschemaXsdMaxVersie_test",
        )

        # Vaststellen dat, wanneer de bericht en / of standaard niet gevuld
        # is, # dit in het foutrapport is opgenomen
        del foutrapport[:]
        del app_info["bericht"]
        testable = import_bizz_xsds._maak_bericht(app_info, foutrapport)
        assert_equal(1, len(foutrapport))
        assert_equal(
            "Technische informatie items 'bericht' en 'standaard' zijn niet "
            + "beide aanwezig.",
            foutrapport[0],
        )

        del foutrapport[:]
        del app_info["standaard"]
        testable = import_bizz_xsds._maak_bericht(app_info, foutrapport)
        assert_equal(1, len(foutrapport))
        assert_equal(
            "Technische informatie items 'bericht' en 'standaard' zijn niet "
            + "beide aanwezig.",
            foutrapport[0],
        )

        del foutrapport[:]
        app_info["bericht"] = "test"
        testable = import_bizz_xsds._maak_bericht(app_info, foutrapport)
        assert_equal(1, len(foutrapport))
        assert_equal(
            "Technische informatie items 'bericht' en 'standaard' zijn niet "
            + "beide aanwezig.",
            foutrapport[0],
        )

        del foutrapport[:]
        del app_info["bericht"]
        app_info["standaard"] = "test"
        testable = import_bizz_xsds._maak_bericht(app_info, foutrapport)
        assert_equal(1, len(foutrapport))
        assert_equal(
            "Technische informatie items 'bericht' en 'standaard' zijn niet "
            + "beide aanwezig.",
            foutrapport[0],
        )

    def test_verwerk_appinfo(self):
        """Unittest voor _verwerk_appinfo method"""

        def create_appinfo_skeleton() -> dict:
            return {
                "bericht": None,
                "standaard": None,
                "release": None,
                "BerichtXsdVersie": None,
                "BerichtXsdMinVersie": None,
                "BerichtXsdMaxVersie": None,
                "BasisschemaXsdVersie": None,
                "BasisschemaXsdMinVersie": None,
                "BasisschemaXsdMaxVersie": None,
            }

        # Testen dat als er geen xs:annotation is, er ook geen resultaten
        # zijn
        testable: dict = import_bizz_xsds._verwerk_appinfo({})
        assert_equal(len(testable), 0)
        testable = import_bizz_xsds._verwerk_appinfo({"test": "novalues"})
        assert_equal(len(testable), 0)

        # Testen dat als er een xs:annotation is, maar geen xs:appinfo, er ook
        # geen resultaten zijn
        testable = import_bizz_xsds._verwerk_appinfo({"xs:annotation": "blah"})
        assert_equal(len(testable), 0)

        # Testen dat als er xs:appinfo is, maar hierin alleen "niet herkenbare"
        # items staan, de dictionary uitsluitend gevuld is met "None" items
        testable = import_bizz_xsds._verwerk_appinfo(
            {"xs:annotation": {"xs:appinfo": {"niet-herkend-item": "blah"}}}
        )
        expected_output = create_appinfo_skeleton()
        assert_equal(testable, expected_output)

        # Testen dat als er xs:appinfo is, en hierin herkenbare items staan, de
        # dictionary juist gevuld is
        test_input = create_appinfo_skeleton()
        test_input["bericht"] = "bericht_test"
        test_input["standaard"] = "standaard_test"
        test_input["release"] = "release_test"
        test_input["BerichtXsdVersie"] = "BerichtXsdVersie_test"
        test_input["BerichtXsdMinVersie"] = "BerichtXsdMinVersie_test"
        test_input["BerichtXsdMaxVersie"] = "BerichtXsdMaxVersie_test"
        test_input["BasisschemaXsdVersie"] = "BasisschemaXsdVersie_test"
        test_input["BasisschemaXsdMinVersie"] = "BasisschemaXsdMinVersie_test"
        test_input["BasisschemaXsdMaxVersie"] = "BasisschemaXsdMaxVersie_test"
        expected_output = test_input
        testable = import_bizz_xsds._verwerk_appinfo(
            {"xs:annotation": {"xs:appinfo": test_input}}
        )
        assert_equal(testable, expected_output)
        # Nu met namespace
        expected_output = test_input
        test_input = {
            "tst:bericht": "bericht_test",
            "tst:standaard": "standaard_test",
            "tst:release": "release_test",
            "tst:BerichtXsdVersie": "BerichtXsdVersie_test",
            "tst:BerichtXsdMinVersie": "BerichtXsdMinVersie_test",
            "tst:BerichtXsdMaxVersie": "BerichtXsdMaxVersie_test",
            "tst:BasisschemaXsdVersie": "BasisschemaXsdVersie_test",
            "tst:BasisschemaXsdMinVersie": "BasisschemaXsdMinVersie_test",
            "tst:BasisschemaXsdMaxVersie": "BasisschemaXsdMaxVersie_test",
        }
        testable = import_bizz_xsds._verwerk_appinfo(
            {"xs:annotation": {"xs:appinfo": test_input}}
        )
        assert_equal(testable, expected_output)

    def test_valideer_datatypes_gebruik(self):
        """Unittest voor _valideer_datatypes_gebruik"""

        # Vaststellen dat wanneer een of meerdere key(s) uit de dictionary
        # niet als waarde van een xs:element type attribuut is opgenomen dit
        # netjes in de foutrapportage gemeld wordt
        # - Eerst testen met één niet gevonden item
        fake_datatypes: dict = {"dummy_key": 1}
        fake_xsd: dict = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
	<xs:element name="Bericht" type="xs:string"></xs:element>
</xs:schema>
"""
        )
        foutrapport: List[str] = []
        import_bizz_xsds._valideer_datatypes_gebruik(
            fake_xsd, fake_datatypes, foutrapport
        )
        assert_equal(1, len(foutrapport))

        # - Dan nog testen met meerdere niet gevonden items
        fake_datatypes = {
            "dummy_key": 1,
            "dummy_key_two": 2,
            "iwmo:non_existant": 3,
        }
        del foutrapport[:]
        import_bizz_xsds._valideer_datatypes_gebruik(
            fake_xsd, fake_datatypes, foutrapport
        )
        # Er wordt 1 melding gerapporteerd met hierin alle ontbrekende
        # datatypes
        assert_equal(1, len(foutrapport))

        # - Tenslotte nog testen met een niet gevonden, en een wel gevonden
        #   item
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
	<xs:element name="Bericht" type="Root"></xs:element>
    <xs:complexType name="Root">
		<xs:sequence>
			<xs:element name="Header" type="xs:string"></xs:element>
			<xs:element name="Client" type="xs:string"></xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
"""
        )
        fake_datatypes = {
            "dummy_key": 1,
            "Root": 2,
        }
        del foutrapport[:]
        import_bizz_xsds._valideer_datatypes_gebruik(
            fake_xsd, fake_datatypes, foutrapport
        )
        # Er wordt 1 melding gerapporteerd met hierin alle ontbrekende
        # datatypes
        assert_equal(1, len(foutrapport))

        # Tenslotte vaststellen dat wanneer alle datatypes gebruikt zijn, er
        # GEEN rapportage gedaan wordt (en geen exception trouwens)
        fake_datatypes = {
            "Root": 1,
        }
        del foutrapport[:]
        import_bizz_xsds._valideer_datatypes_gebruik(
            fake_xsd, fake_datatypes, foutrapport
        )
        assert_equal(0, len(foutrapport))

    def test_verwerk_root_element(self):
        """Unittest voor _verwerk_root_element

        Verwerk het root element (xs:element) uit het schema.
        1. Zonder root element, verwachten we een IStdException
        2. Meerdere root elementen, verwachten we een IStdException
        3. Eén root element, maar met een niet standaard datatype welke niet
           in deze xsd gespecificeerd is, verwachten we een IStdException
        4. Eén root element, maar geen datatypes in deze xsd gespecificeerd,
           verwachten we een NotImplementedException
        5. Eén root element met type zoals in deze xsd gespecificeerd, zonder
           beschrijving
        6. Eén root element met type zoals in deze xsd gespecificeerd, met
           beschrijving"""

        # 1. schema met alleen typen.
        # pylint: disable=[C0103]
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
	<xs:complexType name="Root">
		<xs:annotation>
			<xs:documentation>Bericht voor de toewijzing van Wmo-ondersteuning aan een aanbieder.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Header" type="Header"></xs:element>
			<xs:element name="Client" type="xs:string"></xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Header">
		<xs:sequence>
			<xs:element name="BerichtCode">
				<xs:annotation>
					<xs:documentation>Code ter identificatie van een soort bericht.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="414"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="BerichtVersie">
				<xs:annotation>
					<xs:documentation>Volgnummer van de formele uitgifte van een major release van een iStandaard.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="3"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="BerichtSubversie">
				<xs:annotation>
					<xs:documentation>Volgnummer van de formele uitgifte van een minor release van een iStandaard.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="Afzender" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of ondersteuning.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Ontvanger" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identificerende code van een aanbieder van zorg of ondersteuning.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="BerichtIdentificatie" type="xs:string">
				<xs:annotation>
					<xs:documentation>Naam of nummer en dagtekening ter identificatie van een totale aanlevering.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="XsdVersie" type="xs:string">
				<xs:annotation>
					<xs:documentation>Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het heenbericht.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        fake_datatypes: Dict[str, IStdDataType] = {
            "fake:Root": IStdDataType("Root"),
            "fake:Header": IStdDataType("Header"),
        }
        fake_bericht: IStdBericht = IStdBericht("Fake")
        with assert_raises(IStdException) as context:
            import_bizz_xsds._verwerk_root_element(
                fake_xsd, fake_datatypes, fake_bericht
            )
        assert_equal(
            str(context.exception),
            "Er dient een root element in de berichtdefinitie opgenomen te zijn",
        )
        del fake_bericht

        # 2. Meerdere root elementen, verwachten we een IStdException
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root_1" type="Root" />
    <xs:element name="Root_2" type="xs:string" />
	<xs:complexType name="Root">
		<xs:annotation>
			<xs:documentation>Bericht voor de toewijzing van Wmo-ondersteuning aan een aanbieder.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Header" type="Header"></xs:element>
			<xs:element name="Client" type="xs:string"></xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Header">
		<xs:sequence>
			<xs:element name="BerichtCode">
				<xs:annotation>
					<xs:documentation>Code ter identificatie van een soort bericht.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="414"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="BerichtVersie">
				<xs:annotation>
					<xs:documentation>Volgnummer van de formele uitgifte van een major release van een iStandaard.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="3"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="BerichtSubversie">
				<xs:annotation>
					<xs:documentation>Volgnummer van de formele uitgifte van een minor release van een iStandaard.</xs:documentation>
				</xs:annotation>
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:pattern value="0"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:element>
			<xs:element name="Afzender" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of ondersteuning.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Ontvanger" type="xs:string">
				<xs:annotation>
					<xs:documentation>Identificerende code van een aanbieder van zorg of ondersteuning.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="BerichtIdentificatie" type="xs:string">
				<xs:annotation>
					<xs:documentation>Naam of nummer en dagtekening ter identificatie van een totale aanlevering.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="XsdVersie" type="xs:string">
				<xs:annotation>
					<xs:documentation>Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet voor het opstellen van het heenbericht.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        fake_bericht: IStdBericht = IStdBericht("Fake")
        with assert_raises(IStdException) as context:
            import_bizz_xsds._verwerk_root_element(
                fake_xsd, fake_datatypes, fake_bericht
            )
        assert_equal(
            str(context.exception),
            "Er dient slechts een root element in de berichtdefinitie opgenomen"
            + " te zijn",
        )
        del fake_bericht

        # 3. Eén root element, maar met een niet standaard datatype welke niet
        #    in deze xsd gespecificeerd is, verwachten we een IStdException
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root_2" type="Header" />
	<xs:complexType name="Root">
		<xs:annotation>
			<xs:documentation>Bericht voor de toewijzing van Wmo-ondersteuning aan een aanbieder.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Header" type="Header"></xs:element>
			<xs:element name="Client" type="xs:string"></xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        fake_datatypes: Dict[str, IStdDataType] = {
            "fake:Root": IStdDataType("Root"),
        }
        fake_bericht: IStdBericht = IStdBericht("Fake")
        with assert_raises(IStdException) as context:
            import_bizz_xsds._verwerk_root_element(
                fake_xsd, fake_datatypes, fake_bericht
            )
        assert_equal(
            str(context.exception),
            "Geen complex- of simpleType 'fake:Header' aangetroffen",
        )
        del fake_bericht

        # 4. Eén root element, maar geen datatypes in deze xsd gespecificeerd,
        #    verwachten we een NotImplementedException
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root_2" type="Header" />
</xs:schema>"""
        )["xs:schema"]
        fake_datatypes = {}
        fake_bericht: IStdBericht = IStdBericht("Fake")
        with assert_raises(NotImplementedError) as context:
            import_bizz_xsds._verwerk_root_element(
                fake_xsd, fake_datatypes, fake_bericht
            )
        assert_equal(
            str(context.exception),
            "Types uit basisschema zijn nu nog niet ondersteund",
        )
        del fake_bericht

        # 5. Eén root element met type zoals in deze xsd gespecificeerd, zonder
        #    beschrijving
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root_2" type="Root" />
	<xs:complexType name="Root">
		<xs:annotation>
			<xs:documentation>Bericht voor de toewijzing van Wmo-ondersteuning aan een aanbieder.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Header" type="Header"></xs:element>
			<xs:element name="Client" type="xs:string"></xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        fake_datatypes: Dict[str, IStdDataType] = {
            "fake:Root": IStdDataType("Root"),
        }
        fake_bericht: IStdBericht = IStdBericht("Fake")
        with patch.object(
            import_bizz_xsds, "_verwerk_datatype_elementen", autospec=True
        ) as mocked_method:
            import_bizz_xsds._verwerk_root_element(
                fake_xsd, fake_datatypes, fake_bericht
            )
            assert_equal(len(fake_bericht.klassen), 1)
            assert_equal(fake_bericht.klassen["Root_2"].naam, "Root_2")
            assert_equal(
                fake_bericht.klassen["Root_2"].datatype,
                fake_datatypes["fake:Root"],
            )
            mocked_method.assert_called_once()
        del fake_bericht

        # 6. Eén root element met type zoals in deze xsd gespecificeerd, met
        #    beschrijving
        beschrijving = "De beschrijving van het Root_2 element"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root_2" type="Root">
        <xs:annotation>
            <xs:documentation>{beschrijving}</xs:documentation>
        </xs:annotation>
    </xs:element>
	<xs:complexType name="Root">
		<xs:annotation>
			<xs:documentation>Bericht voor de toewijzing van Wmo-ondersteuning aan een aanbieder.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Header" type="Header"></xs:element>
			<xs:element name="Client" type="xs:string"></xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        fake_bericht: IStdBericht = IStdBericht("Fake")
        with patch.object(
            import_bizz_xsds, "_verwerk_datatype_elementen", autospec=True
        ) as mocked_method:
            import_bizz_xsds._verwerk_root_element(
                fake_xsd, fake_datatypes, fake_bericht
            )
            assert_equal(
                fake_bericht.klassen["Root_2"].beschrijving, beschrijving
            )
            mocked_method.assert_called_once()
        del fake_bericht

    def test_verwerk_bericht_documentatie(self):
        """Unittest voor _verwerk_bericht_documentatie

        1. Er is wel een xs:annotation met xs:documentation aanwezig onder
           xs:schema -> deze beschrijving moet zijn overgenomen in
           IStdBericht.beschrijving
        2. Er is geen xs:annotation aanwezig onder xs:schema ->
           IStdBericht.beschrijving moet niet gevuld zijn
        3. Er is wel een xs:annotation aanwezig onder xs:schema, maar geen
           xs:documentation -> IStdBericht.beschrijving moet niet gevuld zijn
        """

        # 1.
        fake_beschrijving = "Dit is een beschrijving van het bericht."
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:documentation>{fake_beschrijving}</xs:documentation>
        <xs:appinfo><fake_info>Test</fake_info></xs:appinfo>
    </xs:annotation>
</xs:schema>"""
        )["xs:schema"]
        testee: IStdBericht = IStdBericht("fake")
        import_bizz_xsds._verwerk_bericht_documentatie(fake_xsd, testee)
        assert_equal(testee.beschrijving, fake_beschrijving)
        del testee

        # 2.
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified" />"""
        )["xs:schema"]
        testee: IStdBericht = IStdBericht("fake")
        import_bizz_xsds._verwerk_bericht_documentatie(fake_xsd, testee)
        assert not testee.beschrijving
        del testee

        # 3.
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo><fake_info>Test</fake_info></xs:appinfo>
    </xs:annotation>
</xs:schema>"""
        )["xs:schema"]
        testee: IStdBericht = IStdBericht("fake")
        import_bizz_xsds._verwerk_bericht_documentatie(fake_xsd, testee)
        assert not testee.beschrijving
        del testee

    def test_verwerk_data_typen(self):
        """Unittest voor _verwerk_data_typen

        1. Eén xs:complexType opgenomen in het xs:schema, geen
           xs:simpleType opgenomen
        2. Meerdere xs:complexType opgenomen in het xs:schema, geen
           xs:simpleType opgenomen
        3. Eén xs:complexType opgenomen in het xs:schema, één xs:simpleType
           opgenomen
        4. Eén xs:complexType opgenomen in het xs:schema, meerdere
           xs:simpleType opgenomen
        5. Meerdere xs:complexType opgenomen in het xs:schema, één
           xs:simpleType opgenomen
        6. Meerdere xs:complexType opgenomen in het xs:schema, meerdere
           xs:simpleType opgenomen
        7. Geen xs:complexType opgenomen in het xs:schema, één xs:simpleType
           opgenomen
        8. Geen xs:complexType opgenomen in het xs:schema, meerdere
           xs:simpleType opgenomen

        We mocken het toevoegen van elementen uit een complexType aan het
        resulterende datatype, dat wordt een losse unittest.
        Ook mocken we het daadwerkelijk interpreteren van een complex- en
        simpleType, ook die testen we in losse unittests. Wel willen we zien
        dat de interpretatie wordt aangeroepen wanneer en zo vaak als nodig."""

        # 1. Eén xs:complexType opgenomen in het xs:schema, geen
        #    xs:simpleType opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="Root" />
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            mocked_methods[
                "_verwerk_en_voeg_complex_type_toe"
            ].assert_called_once()
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()
            mocked_methods[
                "_verwerk_en_voeg_simple_type_toe"
            ].assert_not_called()

        # 2. Meerdere xs:complexType opgenomen in het xs:schema, geen
        #    xs:simpleType opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="Root" />
    <xs:complexType name="Header" />
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            assert_equal(
                mocked_methods["_verwerk_en_voeg_complex_type_toe"].call_count,
                2,
            )
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()
            mocked_methods[
                "_verwerk_en_voeg_simple_type_toe"
            ].assert_not_called()

        # 3. Eén xs:complexType opgenomen in het xs:schema, één xs:simpleType
        #    opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="Root" />
    <xs:simpleType name="Header">
        <xs:restriction base="xs:string">
            <xs:pattern value="test"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            mocked_methods[
                "_verwerk_en_voeg_complex_type_toe"
            ].assert_called_once()
            mocked_methods[
                "_verwerk_en_voeg_simple_type_toe"
            ].assert_called_once()
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()

        # 4. Eén xs:complexType opgenomen in het xs:schema, meerdere
        #    xs:simpleType opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="Root" />
    <xs:simpleType name="Header">
        <xs:restriction base="xs:string">
            <xs:pattern value="test"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="AnderType">
        <xs:restriction base="xs:string">
            <xs:pattern value="test twee"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            mocked_methods[
                "_verwerk_en_voeg_complex_type_toe"
            ].assert_called_once()
            assert_equal(
                mocked_methods["_verwerk_en_voeg_simple_type_toe"].call_count,
                2,
            )
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()

        # 5. Meerdere xs:complexType opgenomen in het xs:schema, één
        #    xs:simpleType opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="Root" />
    <xs:complexType name="Client" />
    <xs:simpleType name="Header">
        <xs:restriction base="xs:string">
            <xs:pattern value="test"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            assert_equal(
                mocked_methods["_verwerk_en_voeg_complex_type_toe"].call_count,
                2,
            )
            mocked_methods[
                "_verwerk_en_voeg_simple_type_toe"
            ].assert_called_once()
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()

        # 6. Meerdere xs:complexType opgenomen in het xs:schema, meerdere
        #    xs:simpleType opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="Root" />
    <xs:complexType name="Client" />
    <xs:simpleType name="Header">
        <xs:restriction base="xs:string">
            <xs:pattern value="test"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="Footer">
        <xs:restriction base="xs:string">
            <xs:pattern value="test_two"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            assert_equal(
                mocked_methods["_verwerk_en_voeg_complex_type_toe"].call_count,
                2,
            )
            assert_equal(
                mocked_methods["_verwerk_en_voeg_simple_type_toe"].call_count,
                2,
            )
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()

        # 7. Geen xs:complexType opgenomen in het xs:schema, één xs:simpleType
        #    opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="Header">
        <xs:restriction base="xs:string">
            <xs:pattern value="test"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            mocked_methods[
                "_verwerk_en_voeg_complex_type_toe"
            ].assert_not_called()
            mocked_methods[
                "_verwerk_en_voeg_simple_type_toe"
            ].assert_called_once()
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()

        # 8. Geen xs:complexType opgenomen in het xs:schema, meerdere
        #    xs:simpleType opgenomen
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="Header">
        <xs:restriction base="xs:string">
            <xs:pattern value="test"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="Footer">
        <xs:restriction base="xs:string">
            <xs:pattern value="testee"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]
        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_en_voeg_complex_type_toe=DEFAULT,
            _verwerk_en_voeg_simple_type_toe=DEFAULT,
            _voeg_elementen_aan_complex_types_toe=DEFAULT,
        ) as mocked_methods:
            import_bizz_xsds._verwerk_data_typen(fake_xsd, {}, [])
            mocked_methods[
                "_verwerk_en_voeg_complex_type_toe"
            ].assert_not_called()
            assert_equal(
                mocked_methods["_verwerk_en_voeg_simple_type_toe"].call_count,
                2,
            )
            mocked_methods[
                "_voeg_elementen_aan_complex_types_toe"
            ].assert_called_once()

    def test_verwerk_en_voeg_complex_type_toe(self):
        """Unittest _verwerk_en_voeg_complex_type_toe
        Deze methode doet niet heel veel: geef de vertaalde xs:complexType xsd
        door aan _verwerk_complex_type; deze maakt er een IStdDataType van.
        Dit IStdDataType wordt dan toegevoegd aan de lijst met datatypen.
        Dus: we kunnen alleen testen dat _verwerk_complex_type wordt aange-
        roepen, en dat de lijst met datatypen met één gegroeid is."""

        fake_datatypen: Dict[str, IStdDataType] = {}
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="fake_type" />
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        with patch.object(
            import_bizz_xsds,
            "_verwerk_complex_type",
            autospec=True,
            return_value=IStdDataType("fake_type"),
        ) as mocked_verwerk_complex_type:
            import_bizz_xsds._verwerk_en_voeg_complex_type_toe(
                fake_xsd, {}, fake_datatypen
            )
            mocked_verwerk_complex_type.assert_called_once()
        assert_equal(len(fake_datatypen), 1)
        assert "fake_type" in fake_datatypen

        fake_datatypen = {"existing_fake": IStdDataType("existing_fake")}
        with patch.object(
            import_bizz_xsds,
            "_verwerk_complex_type",
            autospec=True,
            return_value=IStdDataType("fake_type"),
        ) as mocked_verwerk_complex_type:
            import_bizz_xsds._verwerk_en_voeg_complex_type_toe(
                fake_xsd, {}, fake_datatypen
            )
            mocked_verwerk_complex_type.assert_called_once()
        assert_equal(len(fake_datatypen), 2)
        assert "fake_type" in fake_datatypen

    def test_verwerk_complex_type(self):
        """Unittest _verwerk_complex_type
        Deze method verwacht een xs:complexType die via xmltodict is vertaald
        naar een dictionary, en de xs:schema->xs:annotation->xs:appinfo die
        via xmltodict is vertaald naar een dictionary. Het resultaat is een
        IStdDataType object.
        1. Geen xs:complexType dictionary meegegeven -> ArgumentError
        2. Wel xs:complexType dictionary, maar geen xs:appinfo dictionary
           meegegeven -> GEEN foutmelding
        3. Wel xs:appinfo dictionary meegegeven EN hierin zit een "bericht",
           dan moet de naam van het IStdDataType vooraf gegaan worden door
           de waarde uit "bericht", gevolgd door een dubbele punt, gevolgd door
           het name attribuut van xs:complexType
        4. Wel xs:appinfo dictionary meegegeven maar hierin zit GEEN "bericht",
           dan moet de naam van het IStdDataType gelijk zijn aan het name
           attribuut van xs:complexType
        5. Geen xs:annotation onder xs:complexType aanwezig, dan is
           IStdDataType.beschrijving leeg
        6. Wel xs:annotation onder xs:complexType aanwezig, maar hieronder is
           geen xs:documentation aanwezig, dan is IStdDataType.beschrijving
           leeg
        7. Wel xs:annotation onder xs:complexType aanwezig EN hieronder is
           xs:documentation aanwezig, dan is IStdDataType.beschrijving gevuld
           met de tekst uit deze xs:documentation

        Resulterende IStdDataType.isComplex moet de waarde True bevatten"""

        # 1. Geen xs:complexType dictionary meegegeven -> ArgumentError
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_complex_type(
                xs_complex_type=None, app_info=None
            )
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_complex_type(
                xs_complex_type={}, app_info=None
            )

        # 2. Wel xs:complexType dictionary, maar geen xs:appinfo dictionary
        #   meegegeven -> GEEN foutmelding
        fake_complex_type = {"@name": "fake_name"}
        import_bizz_xsds._verwerk_complex_type(fake_complex_type, None)
        import_bizz_xsds._verwerk_complex_type(fake_complex_type, {})

        # 3. Wel xs:appinfo dictionary meegegeven EN hierin zit een "bericht",
        #    dan moet de naam van het IStdDataType vooraf gegaan worden door
        #    de waarde uit "bericht", gevolgd door een dubbele punt, gevolgd door
        #    het name attribuut van xs:complexType
        fake_appinfo = {"bericht": "test"}
        testee: IStdDataType = import_bizz_xsds._verwerk_complex_type(
            fake_complex_type, fake_appinfo
        )
        assert_equal(testee.naam, "test:fake_name")
        fake_appinfo = {"bericht": "TeST"}
        testee: IStdDataType = import_bizz_xsds._verwerk_complex_type(
            fake_complex_type, fake_appinfo
        )
        assert_equal(testee.naam, "test:fake_name")

        # 4. Wel xs:appinfo dictionary meegegeven maar hierin zit GEEN "bericht",
        #    dan moet de naam van het IStdDataType gelijk zijn aan het name
        #    attribuut van xs:complexType
        fake_appinfo = {"blah": "blah"}
        testee: IStdDataType = import_bizz_xsds._verwerk_complex_type(
            fake_complex_type, fake_appinfo
        )
        assert_equal(testee.naam, "fake_name")

        # 5. Geen xs:annotation onder xs:complexType aanwezig, dan is
        #    IStdDataType.beschrijving leeg
        fake_complex_type = {"@name": "fake_name"}
        testee: IStdDataType = import_bizz_xsds._verwerk_complex_type(
            fake_complex_type, None
        )
        assert not testee.beschrijving

        # 6. Wel xs:annotation onder xs:complexType aanwezig, maar hieronder is
        #    geen xs:documentation aanwezig, dan is IStdDataType.beschrijving
        #    leeg
        fake_complex_type = {
            "@name": "fake_name",
            "xs:annotation": {"xs:appinfo": {}},
        }
        testee: IStdDataType = import_bizz_xsds._verwerk_complex_type(
            fake_complex_type, None
        )
        assert not testee.beschrijving

        # 7. Wel xs:annotation onder xs:complexType aanwezig EN hieronder is
        #    xs:documentation aanwezig, dan is IStdDataType.beschrijving gevuld
        #    met de tekst uit deze xs:documentation
        fake_complex_type = {
            "@name": "fake_name",
            "xs:annotation": {
                "xs:appinfo": {},
                "xs:documentation": "Dit is een test",
            },
        }
        testee: IStdDataType = import_bizz_xsds._verwerk_complex_type(
            fake_complex_type, None
        )
        assert_equal(testee.beschrijving, "Dit is een test")

    def test_verwerk_simple_type(self):
        """Unittest _verwerk_simple_type
        1. xs:simpleType met xs:annotation maar zonder xs:documentation leidt
           NIET tot beschrijving op resulterend IStdDataType
        2. xs:simpleType zonder xs:annotation leidt NIET tot beschrijving op
           resulterend IStdDataType
        3. xs:simpleType met xs:annotation en met xs:documentation leidt tot
           beschrijving op resulterend IStdDataType
        4. xs:simpleType zonder xs:restriction leidt NIET tot basis_type,
           max_lengte, min_lengte, min_waarde, max_waarde en patroon op resul-
           terend IStdDataType
        5. xs:simpleType met xs:restriction, met hierop base attribuut leidt
           tot basis_type op resulterend IStdDataType, waarbij de waarde ge-
           vuld wordt met de waarde uit het base attribuut
        6. xs:simpleType met xs:restriction en hieronder xs:maxLength element
           leidt tot max_lengte op resulterend IStdDataType, waarbij de waarde
           gevuld wordt met de waarde uit het value attribuut
        7. xs:simpleType met xs:restriction en hieronder xs:minLength element
           leidt tot min_lengte op resulterend IStdDataType, waarbij de waarde
           gevuld wordt met de waarde uit het value attribuut
        8. xs:simpleType met xs:restriction en hieronder xs:minInclusive element
           leidt tot min_waarde op resulterend IStdDataType, waarbij de waarde
           gevuld wordt met de waarde uit het value attribuut
        9. xs:simpleType met xs:restriction en hieronder xs:maxInclusive element
           leidt tot max_waarde op resulterend IStdDataType, waarbij de waarde
           gevuld wordt met de waarde uit het value attribuut
        10. xs:simpleType met xs:restriction en hieronder xs:pattern element
            leidt tot patroon op resulterend IStdDataType, waarbij de waarde
            gevuld wordt met de waarde uit het value attribuut
        11. Lege app_info dict leidt tot ValueError
        12. app_info dict zonder "bericht" key leidt tot ValueError
        13. xs:simpleType zonder name attribuut leidt tot IStdException
        """

        # 1. xs:simpleType met xs:annotation maar zonder xs:documentation leidt
        #    NIET tot beschrijving op resulterend IStdDataType
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:annotation>
            <xs:appinfo><fake /></xs:appinfo>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="0"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert not testee.beschrijving
        del testee

        # 2. xs:simpleType zonder xs:annotation leidt NIET tot beschrijving op
        #    resulterend IStdDataType
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string">
            <xs:pattern value="0"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert not testee.beschrijving
        del testee

        # 3. xs:simpleType met xs:annotation en met xs:documentation leidt tot
        #    beschrijving op resulterend IStdDataType
        fake_beschrijving = "Beschrijving van het simpleType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:annotation>
            <xs:documentation>{fake_beschrijving}</xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:pattern value="0"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert_equal(testee.beschrijving, fake_beschrijving)
        del testee

        # 4. xs:simpleType zonder xs:restriction leidt NIET tot basis_type,
        #    max_lengte, min_lengte, min_waarde, max_waarde en patroon op resul-
        #    terend IStdDataType
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:union memberTypes="xs:string xs:int" />
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert not testee.restriction
        del testee

        # 5. xs:simpleType met xs:restriction, met hierop base attribuut leidt
        #    tot basis_type op resulterend IStdDataType, waarbij de waarde ge-
        #    vuld wordt met de waarde uit het base attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string" />
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert testee.restriction is not None
        assert_equal(testee.restriction.basis_type, "xs:string")
        del testee

        # 6. xs:simpleType met xs:restriction en hieronder xs:maxLength element
        #    leidt tot max_lengte op resulterend IStdDataType, waarbij de waarde
        #    gevuld wordt met de waarde uit het value attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string">
            <xs:maxLength value="10" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert testee.restriction is not None
        assert_equal(testee.restriction.basis_type, "xs:string")
        assert_equal(str(testee.restriction.max_lengte), "10")
        del testee

        # 7. xs:simpleType met xs:restriction en hieronder xs:minLength element
        #    leidt tot min_lengte op resulterend IStdDataType, waarbij de waarde
        #    gevuld wordt met de waarde uit het value attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string">
            <xs:maxLength value="10" />
            <xs:minLength value="1" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert testee.restriction is not None
        assert_equal(testee.restriction.basis_type, "xs:string")
        assert_equal(str(testee.restriction.max_lengte), "10")
        assert_equal(str(testee.restriction.min_lengte), "1")
        del testee

        # 8. xs:simpleType met xs:restriction en hieronder xs:minInclusive element
        #    leidt tot min_waarde op resulterend IStdDataType, waarbij de waarde
        #    gevuld wordt met de waarde uit het value attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:int">
            <xs:minInclusive value="10" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert testee.restriction is not None
        assert_equal(testee.restriction.basis_type, "xs:int")
        assert_equal(testee.restriction.min_waarde, "10")
        del testee

        # 9. xs:simpleType met xs:restriction en hieronder xs:maxInclusive element
        #    leidt tot max_waarde op resulterend IStdDataType, waarbij de waarde
        #    gevuld wordt met de waarde uit het value attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:int">
            <xs:maxInclusive value="100" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert testee.restriction is not None
        assert_equal(testee.restriction.basis_type, "xs:int")
        assert_equal(testee.restriction.max_waarde, "100")
        del testee

        # 10. xs:simpleType met xs:restriction en hieronder xs:pattern element
        #     leidt tot patroon op resulterend IStdDataType, waarbij de waarde
        #     gevuld wordt met de waarde uit het value attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string">
            <xs:pattern value="abc" />
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        testee: IStdDataType = import_bizz_xsds._verwerk_simple_type(
            fake_xsd, fake_appinfo
        )
        assert testee.restriction is not None
        assert_equal(testee.restriction.basis_type, "xs:string")
        assert_equal(testee.restriction.patroon, "abc")
        del testee

        # 11. Lege app_info dict leidt tot ValueError
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string">
            <xs:pattern value="0"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {}
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_simple_type(fake_xsd, fake_appinfo)

        # 12. app_info dict zonder "bericht" key leidt tot ValueError
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fakeType">
        <xs:restriction base="xs:string">
            <xs:pattern value="0"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"useless": "value"}
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_simple_type(fake_xsd, fake_appinfo)

        # 13. xs:simpleType zonder name attribuut leidt tot IStdException
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType>
        <xs:restriction base="xs:string">
            <xs:pattern value="0"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        fake_appinfo: dict = {"bericht": "fake"}
        with assert_raises(IStdException):
            import_bizz_xsds._verwerk_simple_type(fake_xsd, fake_appinfo)

    def test_verwerk_en_voeg_simple_type_toe(self):
        """Unittest _verwerk_en_voeg_simple_type_toe
        Deze methode doet niet heel veel: geef de vertaalde xs:simpleType xsd
        door aan _verwerk_simple_type; deze maakt er een IStdDataType van.
        Dit IStdDataType wordt dan toegevoegd aan de lijst met datatypen.
        Dus: we kunnen alleen testen dat _verwerk_simple_type wordt aange-
        roepen, en dat de lijst met datatypen met één gegroeid is.
        Wanneer _verwerk_simple_type een IStdException gooit, dan moet deze
        toegevoegd zijn aan foutrapport."""

        fake_datatypen: Dict[str, IStdDataType] = {}
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:simpleType name="fake_type" />
</xs:schema>"""
        )["xs:schema"]["xs:simpleType"]
        with patch.object(
            import_bizz_xsds,
            "_verwerk_simple_type",
            autospec=True,
            return_value=IStdDataType("fake_type"),
        ) as mocked_verwerk_simple_type:
            foutrapport: List[str] = []
            import_bizz_xsds._verwerk_en_voeg_simple_type_toe(
                fake_xsd, {}, fake_datatypen, foutrapport
            )
            mocked_verwerk_simple_type.assert_called_once()
        assert_equal(len(fake_datatypen), 1)
        assert "fake_type" in fake_datatypen

        fake_datatypen = {"existing_fake": IStdDataType("existing_fake")}
        with patch.object(
            import_bizz_xsds,
            "_verwerk_simple_type",
            autospec=True,
            return_value=IStdDataType("fake_type"),
        ) as mocked_verwerk_simple_type:
            foutrapport: List[str] = []
            import_bizz_xsds._verwerk_en_voeg_simple_type_toe(
                fake_xsd, {}, fake_datatypen, foutrapport
            )
            mocked_verwerk_simple_type.assert_called_once()
        assert_equal(len(fake_datatypen), 2)
        assert "fake_type" in fake_datatypen

        with patch.object(
            import_bizz_xsds,
            "_verwerk_simple_type",
            autospec=True,
            side_effect=throw_istd_exception,
        ):
            foutrapport: List[str] = []
            import_bizz_xsds._verwerk_en_voeg_simple_type_toe(
                fake_xsd, {}, fake_datatypen, foutrapport
            )
            assert_equal(1, len(foutrapport))

    def test_verwerk_element(self):
        """Unittest _verwerk_element
        1.a Als xs_element een lege dictionary is, dan verwachten we ValueError
        1.b Als xs:element geen attribuut name heeft, dan verwachten we
            IStdException
        2. Als geen xs:annotation aanwezig is, dan heeft resulterend IStdElement
           geen beschrijving
        3. Als wel xs:annotation aanwezig is, maar hieronder geen
           xs:documentation aanwezig is, dan heeft resulterend IStdElement geen
           beschrijving
        4. Als wel xs:annotation met hieronder xs:documentation aanwezig is, dan
           heeft resulterend IStdElement beschrijving, die overeenkomt met de
           waarde van het xs:documentation element
        5. Als xs_element geen type attribuut heeft, dan heeft resulterend
           IStdElement geen datatype
        6. Als xs_element type attribuut heeft, en de waarde hiervan begint met
           xs: dan heeft resulterend IStdElement als datatype de waarde van het
           type attribuut
        7. Als xs_element type attribuut heeft, en de waarde hiervan begint NIET
           met xs: maar de waarde hiervan komt niet als sleutel voor in
           datatypes, dan heeft resulterend IStdElement als datatype de waarde
           van het type attribuut
        8. Als xs_element type attribuut heeft, en de waarde hiervan begint NIET
           met xs: en de waarde hiervan komt als sleutel voor in datatypes, dan
           heeft resulterend IStdElement als datatype de naam van het IStdDataType
           de naam overeenkomt met de waarde van het type attribuut
        9. Als het IStdDataType van resulterend IStdElement een attribuut
           is_complex heeft, dan heeft resulterend IStdElement is_subklasse
           waarvan de waarde gelijk is aan IStdDataType.is_complex
        10. Als xs_element geen attribuut minOccurs bevat dan heeft resulterend
            IStdElement verplicht False
        11. Als xs_element het attribuut minOccurs bevat en de waarde van het
            attribuut minOccurs is een getal groter dan 0, dan heeft resulterend
            IStdElement verplicht True
        12. Als xs_element het attribuut minOccurs bevat en de waarde van het
            attribuut minOccurs is 0, dan heeft resulterend IStdElement
            verplicht False
        13. Als xs_element geen onderliggend element xs:pattern bevat, dan heeft
            resulterend IStdElement geen patroon
        14. Als xs_element een onderliggend element xs:pattern bevat, dan heeft
            resulterend IStdElement patroon met waarde van het attribuut value
        15. Als xs_element geen onderliggend element xs:minInclusive bevat, dan
            heeft resulterend IStdElement geen min_waarde
        16. Als xs_element een onderliggend element xs:minInclusive bevat, dan
            heeft resulterend IStdElement min_waarde met waarde van het
            attribuut value
        17. Als xs_element geen onderliggend element xs:maxInclusive bevat, dan
            heeft resulterend IStdElement geen max_waarde
        18. Als xs_element een onderliggend element xs:maxInclusive bevat, dan
            heeft resulterend IStdElement max_waarde met waarde van het
            attribuut value
        19. Als xs_element geen attribuut minOccurs bevat, dan heeft resulterend
            IStdElement geen min_occurs
        20. Als xs_element het attribuut minOccurs bevat, dan heeft resulterend
            IStdElement min_occurs waarvan de waarde gelijk is aan het attribuut
            minOccurs
        21. Als xs_element geen attribuut maxOccurs bevat, dan heeft resulterend
            IStdElement geen max_occurs
        22. Als xs_element het attribuut maxOccurs bevat, dan heeft resulterend
            IStdElement max_occurs waarvan de waarde gelijk is aan het attribuut
            maxOccurs"""

        # 1.a Als xs_element een lege dictionary is, dan verwachten we ValueError
        with assert_raises(ValueError):
            import_bizz_xsds._verwerk_element({}, {})

        # 1.b Als xs:element geen attribuut name heeft, dan verwachten we
        #     IStdException
        with assert_raises(IStdException):
            import_bizz_xsds._verwerk_element({"blah": "blah"}, {})

        # 2. Als geen xs:annotation aanwezig is, dan heeft resulterend IStdElement
        #    geen beschrijving
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.beschrijving
        del testee

        # 3. Als wel xs:annotation aanwezig is, maar hieronder geen
        #    xs:documentation aanwezig is, dan heeft resulterend IStdElement geen
        #    beschrijving
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string">
            <xs:annotation>
                <xs:appinfo><blah /></xs:appinfo>
            </xs:annotation>
        </xs:element>
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.beschrijving
        del testee

        # 4. Als wel xs:annotation met hieronder xs:documentation aanwezig is, dan
        #    heeft resulterend IStdElement beschrijving, die overeenkomt met de
        #    waarde van het xs:documentation element
        fake_beschrijving = "Dit is de beschrijving van het xs:element"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string">
            <xs:annotation>
                <xs:documentation>{fake_beschrijving}</xs:documentation>
            </xs:annotation>
        </xs:element>
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.beschrijving, fake_beschrijving)
        del testee

        # 5. Als xs_element geen type attribuut heeft, dan heeft resulterend
        #    IStdElement geen datatype
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.datatype
        del testee

        # 6. Als xs_element type attribuut heeft, en de waarde hiervan begint met
        #    xs: dan heeft resulterend IStdElement als datatype de waarde van het
        #    type attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.datatype, "xs:string")
        del testee

        # 7. Als xs_element type attribuut heeft, en de waarde hiervan begint NIET
        #    met xs: maar de waarde hiervan komt niet als sleutel voor in
        #    datatypes, dan heeft resulterend IStdElement als datatype de waarde
        #    van het type attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="fake" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.datatype, "fake")
        del testee

        testee: IStdElement = import_bizz_xsds._verwerk_element(
            fake_xsd, {"blah_type": IStdDataType("blah_type")}
        )
        del testee

        # 8. Als xs_element type attribuut heeft, en de waarde hiervan begint NIET
        #    met xs: en de waarde hiervan komt als sleutel voor in datatypes, dan
        #    heeft resulterend IStdElement als datatype de naam van het IStdDataType
        #    de naam overeenkomt met de waarde van het type attribuut
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="fake" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        fake_datatype: IStdDataType = IStdDataType("fake")
        testee: IStdElement = import_bizz_xsds._verwerk_element(
            fake_xsd, {"fake": fake_datatype}
        )
        assert_equal(testee.datatype, fake_datatype.naam)
        del testee

        # 9. Als het IStdDataType van resulterend IStdElement een attribuut
        #    is_complex heeft, dan heeft resulterend IStdElement is_subklasse
        #    waarvan de waarde gelijk is aan IStdDataType.is_complex
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="fake" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        fake_datatype: IStdDataType = IStdDataType("fake")
        fake_datatype.is_complex = True
        testee: IStdElement = import_bizz_xsds._verwerk_element(
            fake_xsd, {"fake": fake_datatype}
        )
        assert_equal(testee.is_subklasse, fake_datatype.is_complex)
        del testee

        fake_datatype.is_complex = False
        testee: IStdElement = import_bizz_xsds._verwerk_element(
            fake_xsd, {"fake": fake_datatype}
        )
        assert_equal(testee.is_subklasse, fake_datatype.is_complex)
        del testee

        # 10. Als xs_element geen attribuut minOccurs bevat dan heeft resulterend
        #     IStdElement verplicht False
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.verplicht, False)
        del testee

        # 11. Als xs_element het attribuut minOccurs bevat en de waarde van het
        #     attribuut minOccurs is een getal groter dan 0, dan heeft resulterend
        #     IStdElement verplicht True
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" minOccurs="1" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.verplicht, True)
        del testee

        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" minOccurs="11" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.verplicht, True)
        del testee

        # 12. Als xs_element het attribuut minOccurs bevat en de waarde van het
        #     attribuut minOccurs is 0, dan heeft resulterend IStdElement
        #     verplicht False
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" minOccurs="0" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.verplicht, False)
        del testee

        # 13. Als xs_element geen onderliggend element xs:pattern bevat, dan heeft
        #     resulterend IStdElement geen patroon
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.patroon
        del testee

        # 14. Als xs_element een onderliggend element xs:pattern bevat, dan heeft
        #     resulterend IStdElement patroon met waarde van het attribuut value
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string">
            <xs:pattern value="abc" />
        </xs:element>
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.patroon, "abc")
        del testee

        # 15. Als xs_element geen onderliggend element xs:minInclusive bevat, dan
        #     heeft resulterend IStdElement geen min_waarde
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.min_waarde
        del testee

        # 16. Als xs_element een onderliggend element xs:minInclusive bevat, dan
        #     heeft resulterend IStdElement min_waarde met waarde van het
        #     attribuut value
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string">
            <xs:minInclusive value="1" />
        </xs:element>
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.min_waarde, "1")
        del testee

        # 17. Als xs_element geen onderliggend element xs:maxInclusive bevat, dan
        #     heeft resulterend IStdElement geen max_waarde
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.max_waarde
        del testee

        # 18. Als xs_element een onderliggend element xs:maxInclusive bevat, dan
        #     heeft resulterend IStdElement max_waarde met waarde van het
        #     attribuut value
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string">
            <xs:maxInclusive value="100" />
        </xs:element>
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.max_waarde, "100")
        del testee

        # 19. Als xs_element geen attribuut minOccurs bevat, dan heeft resulterend
        #     IStdElement geen min_occurs
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.min_occurs
        del testee

        # 20. Als xs_element het attribuut minOccurs bevat, dan heeft resulterend
        #     IStdElement min_occurs waarvan de waarde gelijk is aan het attribuut
        #     minOccurs
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" minOccurs="0" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.min_occurs, "0")
        del testee

        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" minOccurs="123" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.min_occurs, "123")
        del testee

        # 21. Als xs_element geen attribuut maxOccurs bevat, dan heeft resulterend
        #     IStdElement geen max_occurs
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert not testee.max_occurs
        del testee

        # 22. Als xs_element het attribuut maxOccurs bevat, dan heeft resulterend
        #     IStdElement max_occurs waarvan de waarde gelijk is aan het attribuut
        #     maxOccurs
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    	<xs:element name="Fake" type="xs:string" maxOccurs="2"/>
</xs:schema>"""
        )["xs:schema"]["xs:element"]
        testee: IStdElement = import_bizz_xsds._verwerk_element(fake_xsd, {})
        assert_equal(testee.max_occurs, "2")
        del testee

    # pylint: disable=[W0703]
    @patch.object(
        import_bizz_xsds,
        "_verwerk_element",
        autospec=True,
        return_value=IStdElement("fakeElement"),
    )
    def test_voeg_elementen_aan_complex_type_toe(self, mocked_verwerk_element):
        """Unittest _voeg_elementen_aan_complex_type_toe
        1. xs:complexType bevat geen name attribuut -> IStdException met
           melding die start met "Geen 'name' attribuut gevonden in complexType"
        2. xs:complexType bevat wel name attribuut -> geen IStdException met
           melding die start met "Geen 'name' attribuut gevonden in complexType"
        3. app_info bevat geen bericht attribuut, en datatypes bevat geen key
           die gelijk is aan de name attribuut van xs:complexType ->
           IStdException met melding die start met "Geen datatype '<naam>'
           gevonden"
        4. app_info bevat geen bericht attribuut, en datatypes bevat wel een
           key die gelijk is aan de name attribuut van xs:complexType -> GEEN
           IStdException met melding die start met "Geen datatype '<naam>'
           gevonden
        5. app_info bevat bericht attribuut, en datatypes bevat geen key die
           gelijk is aan <bericht>:<name> -> IStdException met melding die
           start met "Geen datatype '<bericht>:<name>' gevonden
        6. app_info bevat bericht attribuut, en datatypes bevat WEL key die
           gelijk is aan <bericht>:<name> -> GEEN IStdException met melding die
           start met "Geen datatype '<bericht>:<name>' gevonden
        7. xs:complexType bevat geen xs:sequence element -> foutrapportage met
           melding "complexType '<xs:complexType.name>' bevat geen
           gegevenselementen"
        8. xs:complexType bevat xs:sequence element -> GEEN foutrapportage met
           melding "complexType '<xs:complexType.name>' bevat geen
           gegevenselementen"
        9. xs:sequence element onder xs:complexType bevat geen xs:element
           items -> foutrapportage met melding "complexType
           '<xs:complexType.name>' bevat geen gegevenselementen"
        10. xs:sequence element onder xs:complexType bevat een of meer
            xs:element items -> GEEN foutrapportage met melding "complexType
            '<xs:complexType.name>' bevat geen gegevenselementen"
        11. xs:sequence element onder xs:complexType bevat een xs:element item
            -> _verwerk_element is 1 maal aangeroepen, en 'elementen' property
               van het IStdDataType heeft lengte 1
        12. xs:sequence element onder xs:complexType bevat meer xs:element items
            -> _verwerk_element is even vaak als het aantal xs:element items
               aangeroepen, en 'elementen' property van het IStdDataType heeft
               lengte gelijk aan het aantal xs:element items"""

        # 1. xs:complexType bevat geen name attribuut -> IStdException met
        #    melding die start met "Geen 'name' attribuut gevonden in complexType"
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType>
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        with assert_raises(IStdException) as context:
            import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
                fake_xsd, {}, {}, []
            )
            assert isinstance(context.exception, IStdException) and str(
                context.exception
            ).startswith("Geen 'name' attribuut gevonden in complexType")

        # 2. xs:complexType bevat wel name attribuut -> geen IStdException met
        #    melding die start met "Geen 'name' attribuut gevonden in complexType"
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="FakeType">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        try:
            import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
                fake_xsd, {}, {}, []
            )
        except Exception as caught:
            assert not (
                isinstance(caught, IStdException)
                and str(caught).startswith(
                    "Geen 'name' attribuut gevonden in complexType"
                )
            )

        # 3. app_info bevat geen bericht attribuut, en datatypes bevat geen key
        #    die gelijk is aan de name attribuut van xs:complexType ->
        #    IStdException met melding die start met "Geen datatype '<naam>'
        #    gevonden"
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        with assert_raises(IStdException) as context:
            import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
                fake_xsd, {}, {}, []
            )
            assert str(context.exception).startswith(
                f"Geen datatype '{fake_type_name}' gevonden"
            )

        # 4. app_info bevat geen bericht attribuut, en datatypes bevat wel een
        #    key die gelijk is aan de name attribuut van xs:complexType -> GEEN
        #    IStdException met melding die start met "Geen datatype '<naam>'
        #    gevonden
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        try:
            import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
                fake_xsd,
                {fake_type_name: IStdDataType(fake_type_name)},
                {},
                [],
            )
        except Exception as caught:
            assert not (
                isinstance(caught, IStdException)
                and str(caught).startswith(
                    f"Geen datatype '{fake_type_name}' gevonden"
                )
            )

        # 5. app_info bevat bericht attribuut, en datatypes bevat geen key die
        #    gelijk is aan <bericht>:<name> -> IStdException met melding die
        #    start met "Geen datatype '<bericht>:<name>' gevonden
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <bericht>fake</bericht>
        </xs:appinfo>
    </xs:annotation>
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        with assert_raises(IStdException) as context:
            import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
                fake_xsd, {}, {"bericht": "fake"}, []
            )
            assert str(context.exception).startswith(
                f"Geen datatype 'fake:{fake_type_name}' gevonden"
            )

        # 6. app_info bevat bericht attribuut, en datatypes bevat WEL key die
        #    gelijk is aan <bericht>:<name> -> GEEN IStdException met melding die
        #    start met "Geen datatype '<bericht>:<name>' gevonden
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <bericht>fake</bericht>
        </xs:appinfo>
    </xs:annotation>
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        try:
            import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
                fake_xsd,
                {
                    f"fake:{fake_type_name}": IStdDataType(
                        f"fake:{fake_type_name}"
                    )
                },
                {"bericht": "fake"},
                [],
            )
        except Exception as caught:
            assert not (
                isinstance(caught, IStdException)
                and str(caught).startswith(
                    f"Geen datatype 'fake:{fake_type_name}' gevonden"
                )
            )

        # 7. xs:complexType bevat geen xs:sequence element -> foutrapportage met
        #    melding "complexType '<xs:complexType.name>' bevat geen
        #    gegevenselementen"
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}" />
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        foutrapport: List[str] = []
        import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
            fake_xsd,
            {fake_type_name: IStdDataType(fake_type_name)},
            {},
            foutrapport,
        )
        assert_equal(1, len(foutrapport))
        assert_equal(
            f"complexType '{fake_type_name}' bevat geen gegevenselementen",
            foutrapport[0],
        )

        # 8. xs:complexType bevat xs:sequence element -> GEEN foutrapport met
        #    melding "complexType '<xs:complexType.name>' bevat geen
        #    gegevenselementen"
        del foutrapport[:]
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
            fake_xsd,
            {f"{fake_type_name}": IStdDataType(f"{fake_type_name}")},
            {},
            foutrapport,
        )
        assert (
            f"complexType '{fake_type_name}' bevat geen gegevenselementen"
            not in foutrapport
        )

        # 9. xs:sequence element onder xs:complexType bevat geen xs:element
        #    items -> foutrapport met melding "complexType
        #    '<xs:complexType.name>' bevat geen gegevenselementen"
        del foutrapport[:]
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence />
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
            fake_xsd,
            {fake_type_name: IStdDataType(fake_type_name)},
            {},
            foutrapport,
        )
        assert_equal(1, len(foutrapport))
        assert_equal(
            f"complexType '{fake_type_name}' bevat geen gegevenselementen",
            foutrapport[0],
        )

        # 10. xs:sequence element onder xs:complexType bevat een of meer
        #     xs:element items -> GEEN foutrapport met melding "complexType
        #     '<xs:complexType.name>' bevat geen gegevenselementen"
        del foutrapport[:]
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
            <xs:element name="fakeElementTwo" type="xs:int" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
            fake_xsd,
            {f"{fake_type_name}": IStdDataType(f"{fake_type_name}")},
            {},
            foutrapport,
        )
        assert (
            f"complexType '{fake_type_name}' bevat geen gegevenselementen"
            not in foutrapport
        )

        # 11. xs:sequence element onder xs:complexType bevat een xs:element item
        #     -> _verwerk_element is 1 maal aangeroepen, en 'elementen' property
        #        van het IStdDataType heeft lengte 1
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        mocked_verwerk_element.reset_mock()
        datatypes: Dict[str, IStdDataType] = {
            fake_type_name: IStdDataType(fake_type_name)
        }
        import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
            fake_xsd, datatypes, {}, []
        )
        mocked_verwerk_element.assert_called_once()
        assert_equal(len(datatypes[fake_type_name].elementen), 1)

        # 12. xs:sequence element onder xs:complexType bevat meer xs:element items
        #     -> _verwerk_element is even vaak als het aantal xs:element items
        #        aangeroepen, en 'elementen' property van het IStdDataType heeft
        #        lengte gelijk aan het aantal xs:element items
        fake_type_name = "FakeType"
        fake_xsd = xmltodict.parse(
            f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:complexType name="{fake_type_name}">
        <xs:sequence>
            <xs:element name="fakeElement" type="xs:string" />
            <xs:element name="fakeElementTwo" type="xs:int" />
        </xs:sequence>
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]["xs:complexType"]
        mocked_verwerk_element.reset_mock()
        mocked_verwerk_element.side_effect = [
            IStdElement("fakeElement"),
            IStdElement("fakeElementTwo"),
        ]
        datatypes: Dict[str, IStdDataType] = {
            fake_type_name: IStdDataType(fake_type_name)
        }
        import_bizz_xsds._voeg_elementen_aan_complex_type_toe(
            fake_xsd, datatypes, {}, []
        )
        assert_equal(mocked_verwerk_element.call_count, 2)
        assert_equal(len(datatypes[fake_type_name].elementen), 2)

    @patch.object(
        import_bizz_xsds, "_voeg_elementen_aan_complex_type_toe", autospec=True
    )
    def test_voeg_elementen_aan_complex_types_toe(
        self, mocked_voeg_elementen_aan_complex_type_toe
    ):
        """Unittest _voeg_elementen_aan_complex_types_toe
        1. Geen xs:complexTypes direct onder xs_schema -> geen calls naar
           _voeg_elementen_aan_complex_type_toe
        2. Eén xs:complexType voorkomen direct onder xs_schema -> 1 call naar
           _voeg_elementen_aan_complex_type_toe
        3. Meerdere xs:complexType voorkomens direct onder xs_schema -> voor
           elk voorkomen van xs:complexType direct onder xs_schema, 1 call naar
           _voeg_elementen_aan_complex_type_toe"""

        # 1. Geen xs:complexTypes direct onder xs_schema -> geen calls naar
        #    _voeg_elementen_aan_complex_type_toe
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root" type="xs:string" />
</xs:schema>"""
        )["xs:schema"]
        import_bizz_xsds._voeg_elementen_aan_complex_types_toe(
            fake_xsd, {}, {}, []
        )
        mocked_voeg_elementen_aan_complex_type_toe.assert_not_called()
        mocked_voeg_elementen_aan_complex_type_toe.reset_mock()

        # 2. Eén xs:complexType voorkomen direct onder xs_schema -> 1 call naar
        #    _voeg_elementen_aan_complex_type_toe
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root" type="FakeType" />
    <xs:complexType name="FakeType">
        <xs:element type="xs:string" name="fakeElement" />
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        import_bizz_xsds._voeg_elementen_aan_complex_types_toe(
            fake_xsd, {}, {}, []
        )
        mocked_voeg_elementen_aan_complex_type_toe.assert_called_once()
        mocked_voeg_elementen_aan_complex_type_toe.reset_mock()

        # 3. Meerdere xs:complexType voorkomens direct onder xs_schema -> voor
        #    elk voorkomen van xs:complexType direct onder xs_schema, 1 call naar
        #    _voeg_elementen_aan_complex_type_toe
        fake_xsd = xmltodict.parse(
            """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:element name="Root" type="FakeType" />
    <xs:complexType name="FakeType">
        <xs:sequence>
            <xs:element type="FakeTypeTwo" name="fakeElement" />
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="FakeTypeTwo">
        <xs:element type="xs:int" name="fakeElementTwo" />
    </xs:complexType>
</xs:schema>"""
        )["xs:schema"]
        import_bizz_xsds._voeg_elementen_aan_complex_types_toe(
            fake_xsd, {}, {}, []
        )
        assert_equal(mocked_voeg_elementen_aan_complex_type_toe.call_count, 2)
        mocked_voeg_elementen_aan_complex_type_toe.reset_mock()

    def test_verwerk_berichtdefinitie(self):
        """Unittest voor _verwerk_berichtdefinitie
        Eigenlijk weinig te testen, behalve dat wanneer de method
        _verwerk_root_element wordt aangeroepen, en deze een IStdException
        gooit, we willen zien dat deze netjes in foutrapport is opgenomen"""

        with patch.multiple(
            import_bizz_xsds,
            autospec=True,
            _verwerk_appinfo=DEFAULT,
            _maak_bericht=DEFAULT,
            _verwerk_bericht_documentatie=DEFAULT,
            _verwerk_data_typen=DEFAULT,
            _valideer_datatypes_gebruik=DEFAULT,
        ):
            with patch.object(
                import_bizz_xsds,
                "_verwerk_root_element",
                side_effect=throw_istd_exception,
            ):
                foutrapport: List[str] = []
                import_bizz_xsds._verwerk_berichtdefinitie({}, foutrapport)
                assert_equal(1, len(foutrapport))
