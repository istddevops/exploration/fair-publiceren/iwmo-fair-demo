"""Unittests for checking appinfo annotations are correctly processed in the
various istandaard classes"""


import unittest
from unittest.mock import mock_open, patch

from nose.tools import assert_equal

from src import import_bizz_xsds
from src.istandaard.classes import IStdBericht


class AppInfoNaarIStdBericht(unittest.TestCase):
    """De technische informatie over de berichtstandaard dient te worden
    overgenomen in het IStdBericht object.

    De technische informatie van een berichtstandaard staat in de XSD
    opgenomen in de xs:annotation/xs:appinfo elementen op het hoogste niveau"""

    def test_standaard(self):
        """Stel vast dat het appinfo element "standaard" correct wordt
        overgenomen in het IStdBericht.standaard attribuut"""

        expected = "iDgPi"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:standaard>
                {expected}
            </test:standaard>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.standaard, expected)

    def test_release(self):
        """Stel vast dat het appinfo element "release" correct wordt
        overgenomen in het IStdBericht.release attribuut"""

        expected = "0.9.3"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:release>
                {expected}
            </test:release>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.release, expected)

    def test_bericht_xsd_versie(self):
        """Stel vast dat het appinfo element "BerichtXsdVersie" correct wordt
        overgenomen in het IStdBericht.bericht_xsd_versie attribuut"""

        expected = "0.7.378"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:BerichtXsdVersie>
                {expected}
            </test:BerichtXsdVersie>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.bericht_xsd_versie, expected)

    def test_bericht_xsd_min_versie(self):
        """Stel vast dat het appinfo element "BerichtXsdMinVersie" correct
        wordt overgenomen in het IStdBericht.bericht_xsd_min_versie
        attribuut"""

        expected = "0.6.0"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:BerichtXsdMinVersie>
                {expected}
            </test:BerichtXsdMinVersie>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.bericht_xsd_min_versie, expected)

    def test_bericht_xsd_max_versie(self):
        """Stel vast dat het appinfo element "BerichtXsdMaxVersie" correct
        wordt overgenomen in het IStdBericht.bericht_xsd_max_versie
        attribuut"""

        expected = "0.7.378"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:BerichtXsdMaxVersie>
                {expected}
            </test:BerichtXsdMaxVersie>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.bericht_xsd_max_versie, expected)

    def test_basisschema_xsd_versie(self):
        """Stel vast dat het appinfo element "BasisschemaXsdVersie" correct
        wordt overgenomen in het IStdBericht.basisschema_xsd_versie
        attribuut"""

        expected = "1.2.0"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:BasisschemaXsdVersie>
                {expected}
            </test:BasisschemaXsdVersie>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.basisschema_xsd_versie, expected)

    def test_basisschema_xsd_min_versie(self):
        """Stel vast dat het appinfo element "BasisschemaXsdMinVersie" correct
        wordt overgenomen in het IStdBericht.basisschema_xsd_min_versie
        attribuut"""

        expected = "1.0.0"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:BasisschemaXsdMinVersie>
                {expected}
            </test:BasisschemaXsdMinVersie>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.basisschema_xsd_min_versie, expected)

    def test_basisschema_xsd_max_versie(self):
        """Stel vast dat het appinfo element "BasisschemaXsdMaxVersie" correct
        wordt overgenomen in het IStdBericht.basisschema_xsd_max_versie
        attribuut"""

        expected = "1.2.0"
        fake_xsd = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <test:BasisschemaXsdMaxVersie>
                {expected}
            </test:BasisschemaXsdMaxVersie>
        </xs:appinfo>
    </xs:annotation>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.basisschema_xsd_max_versie, expected)
