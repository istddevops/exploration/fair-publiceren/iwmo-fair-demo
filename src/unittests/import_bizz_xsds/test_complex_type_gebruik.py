"""Unittests for import_bizz_xsds module of the istandaard_naar_hugo package"""

import unittest
from unittest.mock import mock_open, patch

from nose.tools import assert_equal, assert_raises

from src import import_bizz_xsds
from src.istandaard.classes import IStdBericht, IStdException


# pylint:disable=[W0212]
class ComplexTypeTestCase(unittest.TestCase):
    """Unittest die aftesten dat het gebruik van complexType definities
    correct werkt."""

    def test_gebruik_complex_type(self):
        """Unittest die controleert of een niet gebruikt complexType leidt
        tot de juiste IStdException
        1. Meerdere complexTypes niet gebruikt, deze dienen allemaal opgelijst
           te worden in de foutmelding.
        2. Eén complexType niet gebruikt; deze dient genoemd te worden in de
           foutmelding.
        3. Alle complexTypes worden gebruikt, geen foutmelding over niet ge-
           bruikte complexTypes"""

        # 1. Meerdere complexTypes niet gebruikt, deze dienen allemaal
        #    opgelijst te worden in de foutmelding.
        fake_xsd: str = """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <bericht>idgpi</bericht>
        </xs:appinfo>
    </xs:annotation>
    <xs:complexType name="FakeComplexType">
        <xs:sequence>
            <xs:element name="Placeholder" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="YetAnotherFakeComplexType">
        <xs:sequence>
            <xs:element name="YetAnotherPlaceholder" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""

        expected_exc: str = (
            "Deze gedefinieerde complexTypen worden niet gebruikt in deze "
            + "berichtstandaard; ['idgpi:FakeComplexType', "
            + "'idgpi:YetAnotherFakeComplexType']"
        )

        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            with assert_raises(IStdException) as caught:
                import_bizz_xsds.import_bericht_xsd("fake.xsd")
                assert_equal(str(caught), expected_exc)

        # 2. Eén complexType niet gebruikt; deze dient genoemd te worden in de
        #    foutmelding.
        fake_xsd: str = """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <bericht>idgpi</bericht>
        </xs:appinfo>
    </xs:annotation>
    <xs:complexType name="FakeComplexType">
        <xs:sequence>
            <xs:element name="Placeholder" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
    <xs:element name="Test" type="xs:element"/>
</xs:schema>"""

        expected_exc: str = (
            "Deze gedefinieerde complexTypen worden niet gebruikt in deze "
            + "berichtstandaard; ['idgpi:FakeComplexType']"
        )

        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            with assert_raises(IStdException) as caught:
                import_bizz_xsds.import_bericht_xsd("fake.xsd")
                assert_equal(str(caught), expected_exc)

        fake_xsd = """<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <bericht>idgpi</bericht>
        </xs:appinfo>
    </xs:annotation>
    <xs:complexType name="FakeComplexType">
        <xs:sequence>
            <xs:element name="Placeholder" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
    <xs:element name="Test" type="FakeComplexType"/>
</xs:schema>"""

        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            import_bizz_xsds.import_bericht_xsd("fake.xsd")

    def test_annotatie_toevoegen_aan_complex_type_beschrijving(self):
        """Unittest die controleert dat een annotatie op een complexType wordt
        overgenomen in de beschrijving van het resulterend IStdDataType"""

        expected: str = "Dit is de beschrijving van het fake complex type"
        fake_xsd: str = f"""<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           elementFormDefault="qualified">
    <xs:annotation>
        <xs:appinfo>
            <bericht>idgpi</bericht>
        </xs:appinfo>
    </xs:annotation>
    <xs:complexType name="FakeComplexType">
        <xs:annotation>
            <xs:documentation>{expected}</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="Placeholder" type="xs:string" />
        </xs:sequence>
    </xs:complexType>
    <xs:element name="Bericht" type="FakeComplexType" />
</xs:schema>
"""
        with patch("builtins.open", mock_open(read_data=fake_xsd)):
            output: IStdBericht = import_bizz_xsds.import_bericht_xsd(
                "fake.xsd"
            )
            assert_equal(output.klassen["Bericht"].beschrijving, expected)
