"""Unittests for istandaard.transform module of the istandaard_naar_hugo
package"""


import unittest
from typing import List

from nose.tools import assert_equal

import src.istandaard.transform


# pylint:disable=[W0212]
class TransformeerNaarYamlTestCase(unittest.TestCase):
    """Testcase for transformeer_bericht_naar_yaml"""

    def test_members_geinstantieerd_via_constructor(self):
        """Test dat omzetten van members naar een yaml-achtige opmaak goed
        werkt"""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                # Deze zou niet afgedrukt moeten worden (want private /
                # protected)
                self._a = "a"
                self.b = "b"

            def c(self):
                """Dummy"""

        testee: TestClass = TestClass()
        expected: str = """b: 'b'"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )

        assert_equal(actual, expected)

    def test_meerdere_regels(self):
        """Test dat omzetten van string met meerdere regels leidt tot een
        lijst met strings in de resulterende yaml"""

        class TestClass:  # pylint: disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self.beschrijving = "Regel 1\nRegel 2\nRegel 3"

        testee: TestClass = TestClass()
        expected: str = """beschrijving:
- 'Regel 1'
- 'Regel 2'
- 'Regel 3'"""

        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

        # Nu nog testen dat de beschrijving ook netjes wordt ingesprongen
        # wanneer het niveau niet 0 is, maar bijv. 2
        # En dat het geen lijst is, wanneer er maar 1 regel in zit
        expected: str = """    beschrijving: 'Regel 1'"""
        testee.beschrijving = "Regel 1"
        actual = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 2
        )

        assert_equal(actual, expected)

    def test_subclass(self):
        """Test dat omzetten van member die zelf ook weer een class is
        leidt tot een ingesprongen member, met hieronder de eigenschappen
        van de class weer uitgewerkt."""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self.beschrijving = (
                    "Deze klasse wordt gebruikt als attribuut van een andere\n"
                    + "klasse."
                )

        class TestClass2:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self.a = "a"
                self.testee = TestClass()

        testee: TestClass2 = TestClass2()
        expected: str = """a: 'a'
testee:
  beschrijving:
  - 'Deze klasse wordt gebruikt als attribuut van een andere'
  - 'klasse.'"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_list(self):
        """Test dat omzetten van member die een List is leidt tot een member
        met daaronder een lijst van items. Indien de items classes zijn dan
        de eigenschappen van deze class uitgewerkt per lijst item."""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self, value=None):
                self.a = "a"
                if None is not value:
                    self.b = 1 + value
                else:
                    self.b = 1

        class TestClass2:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self.a = "a2"
                self.b = [TestClass(), TestClass(1)]

        testee: TestClass2 = TestClass2()
        expected: str = """a: 'a2'
b:
-
  a: 'a'
  b: 1
-
  a: 'a'
  b: 2"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_dict(self):
        """Test dat omzetten van member die een Dictionary is, leidt tot een
        member met daaronder een lijst van key / value items. Indien de value
        een class is, dan de eigenschappen van deze class uitgewerkt per lijst
        item."""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self, value=None):
                self.a = "a"
                if None is not value:
                    self.b = 1 + value
                else:
                    self.b = 1

        class TestClass2:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self.a = "a3"
                self.b = {"Fake1": TestClass(), "Fake2": ["a", "b", "c"]}

        testee: TestClass2 = TestClass2()
        expected: str = """a: 'a3'
b:
- Fake1:
  a: 'a'
  b: 1
- Fake2:
  - 'a'
  - 'b'
  - 'c'"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_alleen_afdrukken_wanneer_waarde_gezet(self):
        """Test dat omzetten van member die niet gezet is (None) ook niet wordt
        opgenomen in de output yaml."""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self.a = None
                self.b = 1

        testee: TestClass = TestClass()
        expected: str = """b: 1"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

        # Lege string is nog steeds WEL een gezette waarde, dus (leeg) opnemen
        # in de yaml
        testee.a = ""
        # pylint:disable=[C0303]
        # flake8:noqa(W291)
        expected: str = """a: ''
b: 1"""
        actual = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_decorator_yaml_name(self):
        """Test om vast te stellen dat een attribuut dat is gedecoreerd met
        yaml_name, ook daadwerkelijk niet met de attribuutnaam, maar de naam
        zoals opgegeven bij de decorator, in de YAML land."""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self._a = "a"
                self._b = 1

            @property
            def a(self):
                """Dummy"""
                return self._a

            @property
            @src.istandaard.transform.yaml_name("Test")
            def b(self):
                """Dummy"""
                return self._b

            @b.setter
            def b(self, value):
                if None is not value:
                    self._b = 1 + value
                else:
                    self._b = 1

        testee: TestClass = TestClass()
        expected: str = """a: 'a'
Test: 1"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_decorator_no_yaml_output(self):
        """Test om vast te stellen dat een attribuut dat is gedecoreerd met
        no_yaml_output, ook daadwerkelijk niet in de YAML land."""

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self._a = "a"
                self._b = 1

            @property
            def a(self):
                """Dummy"""
                return self._a

            @property
            @src.istandaard.transform.no_yaml_output
            def b(self):
                """Dummy"""
                return self._b

            @b.setter
            def b(self, value):
                if None is not value:
                    self._b = 1 + value
                else:
                    self._b = 1

        testee: TestClass = TestClass()
        expected: str = "a: 'a'"
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_decorator_no_yaml_use_class_attribute_for_list_item(self):
        """Test om vast te stellen dat een attribuut dat is gedecoreerd met
        yaml_use_class_attribute_for_list_item, ook daadwerkelijk ervoor zorgt
        dat voor elk object in de lijst, niet het hele object wordt afgedrukt,
        maar alleen het genoemde attribuut."""

        class TestClass2:  # pylint:disable=[C0115, C0103, R0903]
            """Dummy"""

            def __init__(self):
                self.c = "c"
                self.d = "d"

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self._a = "a"
                self._b = [TestClass2(), TestClass2()]

            @property
            def a(self):
                """Dummy"""
                return self._a

            @property
            @src.istandaard.transform.yaml_use_class_attribute_for_list_item(
                "d"
            )
            def b(self) -> List[TestClass2]:
                """Dummy"""
                return self._b

        testee: TestClass = TestClass()
        expected: str = """a: 'a'
b:
- 'd'
- 'd'"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_decorator_no_yaml_use_class_attribute_for_list_item_and_yaml_name(
        self,
    ):
        """Test om vast te stellen dat een attribuut dat is gedecoreerd met
        yaml_use_class_attribute_for_list_item EN yaml_name, ook daadwerkelijk
        ervoor zorgt dat voor elk object in de lijst, niet het hele object
        wordt afgedrukt, maar alleen het genoemde attribuut. Daarnaast moet de
        naam van het attribuut waarin de lijst staat, ook zijn aangepast over-
        eenkomstig het predicaat yaml_name"""

        class TestClass2:  # pylint:disable=[C0115, C0103, R0903]
            """Dummy"""

            def __init__(self):
                self.c = "c"
                self.d = "d"

        class TestClass:  # pylint:disable=[C0115, C0103]
            """Dummy"""

            def __init__(self):
                self._a = "a"
                self._b = [TestClass2(), TestClass2()]

            @property
            def a(self):
                """Dummy"""
                return self._a

            @property
            @src.istandaard.transform.yaml_use_class_attribute_for_list_item(
                "d"
            )
            @src.istandaard.transform.yaml_name("Test")
            def b(self) -> List[TestClass2]:
                """Dummy"""
                return self._b

        testee: TestClass = TestClass()
        expected: str = """a: 'a'
Test:
- 'd'
- 'd'"""
        actual: str = src.istandaard.transform.transformeer_object_naar_yaml(
            testee, 0
        )
        assert_equal(actual, expected)

    def test_transformeer_builtin_naar_yaml(self):
        """Stel vast dat wanneer een builtin type wordt aangeboden, deze op de
        juiste wijze wordt omgezet naar YAML:
        - Wanneer het een string betreft:
          - Wanneer deze string bestaat uit meerdere regels, dan deze string
            als een array weergeven, waarbij per regeleinde wordt gesplitst.
          - Elke (regel uit de) string dient te worden omgeven door single
            quotes.
        - Wanneer het een ander builtin type betreft, de string representatie
          van het object, zonder single quotes, naar de YAML printen."""

        # String
        testee: str = (
            "Dit is een string die bestaat uit meerdere regels.\n"
            + "Dit is de tweede regel uit de string!"
        )
        expected: str = """
- 'Dit is een string die bestaat uit meerdere regels.'
- 'Dit is de tweede regel uit de string!'"""
        assert_equal(
            src.istandaard.transform._transformeer_builtin_naar_yaml(testee),
            expected,
        )

        # String met 1 regel
        testee = "Dit is een string die bestaat uit een enkele regel."
        expected = f"'{testee}'"
        assert_equal(
            src.istandaard.transform._transformeer_builtin_naar_yaml(testee),
            expected,
        )

        # Ander type, bijv. int
        testee_int = 385
        expected = "385"
        assert_equal(
            src.istandaard.transform._transformeer_builtin_naar_yaml(
                testee_int
            ),
            expected,
        )
