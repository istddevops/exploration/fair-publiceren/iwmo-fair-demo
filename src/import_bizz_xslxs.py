"""iStandaard BizzDesign Output XLSX-bestand (Excel) conversie naar Python
Class Formaat (istd_klassen)

Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts) gepubli-
ceerde XSDs

Auteur: A. Haldar (Onno)"""

# package imports
from enum import Enum
from typing import List, Tuple

from humps import camelize
from openpyxl import load_workbook

# local imports
from .istandaard.classes import (
    IStdBericht,
    IStdCodeItem,
    IStdCodeLijst,
    IStdDataType,
    IStdElement,
    IStdException,
    IStdKlasse,
    IStdRegel,
)


class ColumnType(Enum):
    """Duiding voor type Kolom"""

    BERICHT = 0
    KLASSE = 1
    ELEMENT = 2
    DATATYPE = 3
    SLEUTEL = 4
    CODELIJST = 5
    REGEL = 6


def merge_datatype(
    datatypen: IStdDataType,
    datatype_naam: str = None,
    element_datatype: str = None,
):
    """Zoek datatype via naam in datatypen-lijst (en voeg nieuwe toe indien
    niet aanwezig).
    Geeft index laatst gemuteerde datatype-item terug."""
    # initialisatie
    if None is datatypen:
        datatypen = []
    # Zoek datatype
    datatype = None
    datatype_index = 0
    while datatype_index < len(datatypen) and None is datatype:
        if datatypen[datatype_index].naam == datatype_naam:
            datatype = datatypen[datatype_index]
        else:
            datatype_index = datatype_index + 1

    if None is datatype:
        # Voeg nieuw datatype toe indien niet gevonden
        datatypen.append(IStdDataType(datatype_naam))
        datatype_index = len(datatypen) - 1

    if None is not element_datatype:
        # Zoek element
        element_datatype_list = element_datatype.split("\\")
        element_naam = element_datatype_list[0]
        element = None
        element_index = 0
        while (
            element_index < len(datatypen[datatype_index].elementen)
            and None is element
        ):
            if datatype.elementen[element_index].naam == element_naam:
                element = datatype.elementen[element_index]
            else:
                element_index = element_index + 1

        if None is element:
            # Voeg nieuw element toe aan datatype indien niet gevonden
            datatypen[datatype_index].elementen.append(
                IStdElement(element_naam)
            )
            element_index = len(datatypen[datatype_index].elementen) - 1

        if (
            len(element_datatype_list) == 2
            and None
            is datatypen[datatype_index].elementen[element_index].dataType
        ):
            # Voeg nieuwe datatype samen
            datatype_naam = element_datatype_list[1]
            datatypen[datatype_index].elementen[
                element_index
            ].dataType = datatype_naam
            merge_datatype(datatypen, datatype_naam)

    return datatype_index


def __verwerk_bericht_kolom(
    berichten: List[IStdBericht],
    bericht_worker: IStdBericht,
    waarde: str,
):
    """Verwerk een cel uit de kolom 'bericht'"""
    if 0 == len(berichten):
        # eerste bericht
        bericht_worker = IStdBericht(waarde)
        berichten.append(bericht_worker)
    elif bericht_worker.naam != waarde:
        # volgende bericht; dus alleen wanneer deze een andere naam heeft
        bericht_worker = IStdBericht(waarde)
        berichten.append(bericht_worker)


def __verwerk_berichtklasse_kolom(
    bericht_worker: IStdBericht,
    klasse_worker: IStdKlasse,
    waarde: str,
):
    """Verwerk een cel uit de kolom 'berichtklasse'"""
    if 0 == len(bericht_worker.klassen):
        # eerste klasse
        klasse_worker = IStdKlasse(waarde)
        bericht_worker.klassen.append(klasse_worker)
    elif klasse_worker.naam != waarde:
        # volgende klasse
        klasse_worker = IStdKlasse(waarde)
        bericht_worker.klassen.append(klasse_worker)


def __verwerk_berichtelement_datatype(
    klasse_worker: IStdKlasse,
    element_worker: IStdElement,
    last_datatype_index: int,
    datatypen: List[IStdDataType],
    waarde: str,
) -> Tuple[ColumnType, int]:
    """Verwerkt een cel uit de kolom 'berichtelementDatatype'"""
    column_value_list = waarde.split("\\")
    element_value = column_value_list[0]

    if 0 == len(klasse_worker.elementen):
        # eerste element
        element_worker = IStdElement(element_value)
        klasse_worker.elementen.append(element_worker)
    elif element_worker.naam != element_value:
        # volgende element
        element_worker = IStdElement(element_value)
        klasse_worker.elementen.append(element_worker)

    if len(column_value_list) > 1:
        # bevat tevens datatype
        datatype_naam = column_value_list[1]
        # voeg toe als klasse element datatype
        element_worker.datatype = datatype_naam
        # samenvoegen met lijst voor datatypen
        last_datatype_index = merge_datatype(datatypen, datatype_naam)
        return ColumnType.DATATYPE, last_datatype_index

    return ColumnType.ELEMENT, last_datatype_index


def __verwerk_regel_rij(
    ws_rij,
    workers: Tuple[
        List[IStdBericht],
        List[IStdDataType],
        IStdBericht,
        IStdKlasse,
        IStdElement,
        IStdDataType,
    ],
    last_datatype_index: int,
    column_names: List[str],
) -> Tuple[
    List[IStdBericht],
    List[IStdDataType],
    IStdBericht,
    IStdKlasse,
    IStdElement,
    IStdDataType,
]:
    """Verwerkt een rij met hierin een regel uit het excel werksheet"""
    column_nr: int = 0
    last_column_type: ColumnType = None
    (
        berichten,
        datatypen,
        bericht_worker,
        klasse_worker,
        element_worker,
        datatype_worker,
    ) = workers

    # process cell value for every column in the row
    for ws_cel in ws_rij:
        # mapping van kolomcellen
        column_name: str = column_names[column_nr]

        if None is not ws_cel.value:
            # kolomcel-waarde gevonden om te mappen
            column_value: str = ws_cel.value

            match column_name:
                case "bericht":
                    __verwerk_bericht_kolom(
                        berichten, bericht_worker, column_value
                    )
                    last_column_type = ColumnType.BERICHT
                case "berichtklasse":
                    __verwerk_berichtklasse_kolom(
                        bericht_worker, klasse_worker, column_value
                    )
                    last_column_type = ColumnType.KLASSE
                case "berichtelementDatatype":
                    (
                        last_column_type,
                        last_datatype_index,
                    ) = __verwerk_berichtelement_datatype(
                        klasse_worker,
                        element_worker,
                        last_datatype_index,
                        datatypen,
                        column_value,
                    )
                case "CDTElementDatatype":
                    last_datatype_index = merge_datatype(
                        datatypen, element_worker.datatype, column_value
                    )
                    datatype_worker = datatypen[last_datatype_index]
                    last_column_type = ColumnType.DATATYPE
                case "sleutelelement":
                    element_worker.sleutel = column_value == "ja"
                    last_column_type = ColumnType.SLEUTEL
                case "codelijst":
                    datatype_worker.codeLijst = column_value
                    last_column_type = ColumnType.CODELIJST
                case "regelcode":
                    # het voorgaande type kolom bepaald het niveau waarop
                    # regel is gekoppeld
                    match last_column_type:
                        case ColumnType.BERICHT:
                            if column_value not in bericht_worker.regels:
                                bericht_worker.regels.append(column_value)
                        case ColumnType.KLASSE:
                            if column_value not in klasse_worker.regels:
                                klasse_worker.regels.append(column_value)
                        case ColumnType.ELEMENT:
                            if column_value not in element_worker.regels:
                                element_worker.regels.append(column_value)
                        case ColumnType.DATATYPE:
                            if column_value not in datatype_worker.regels:
                                datatype_worker.regels.append(column_value)
                    last_column_type = ColumnType.REGEL

        # process next column
        column_nr = column_nr + 1

    return (
        berichten,
        datatypen,
        bericht_worker,
        klasse_worker,
        element_worker,
        datatype_worker,
    )


def __verwerk_header_rij(
    werkboek_path: str, werksheet_naam: str, ws_rij, column_names
):
    """Verwerkt de eerste rij uit het excel werksheet"""
    # add column names from the first row
    print(f'Importeer uit Work Book: "{werkboek_path}"')
    print(f"In Work Sheet: [{werksheet_naam}] volgende kolommen gevonden:")
    print("=============================================================")
    for ws_cel in ws_rij:
        column_name: str = camelize(ws_cel.value).replace("\\", "")
        print(f"- {column_name}")
        column_names.append(column_name)


def __verwerk_werksheet(
    werksheet,
    berichten: List[IStdBericht],
    datatypen: List[IStdDataType],
    regels_wb_file_path: str,
    regels_per_berichtelement_ws: str,
):
    """Verwerk één regel uit de Excel"""

    column_names = []
    rij_nummer: int = 0
    last_datatype_index = -1

    bericht_worker: IStdBericht = None
    klasse_worker: IStdKlasse = None
    element_worker: IStdElement = None
    datatype_worker: IStdDataType = None

    for ws_rij in werksheet:
        if rij_nummer == 0:
            __verwerk_header_rij(
                regels_wb_file_path,
                regels_per_berichtelement_ws,
                ws_rij,
                column_names,
            )

        else:
            (
                berichten,
                datatypen,
                bericht_worker,
                klasse_worker,
                element_worker,
                datatype_worker,
            ) = __verwerk_regel_rij(
                ws_rij,
                (
                    berichten,
                    datatypen,
                    bericht_worker,
                    klasse_worker,
                    element_worker,
                    datatype_worker,
                ),
                last_datatype_index,
                column_names,
            )

        # process next row
        rij_nummer = rij_nummer + 1


def _valideer_regels_gebruik(
    berichten: List[IStdBericht],
    datatypen: List[IStdDataType],
    regels_ws: List[IStdRegel],
):
    """Controleer dat alle bedrijfsregels uit {regels_ws} in minstens een
    bericht of datatype worden gebruikt. Zo niet, dan foutmelding."""
    niet_gebruikte_regels = ""
    niet_gebruikte_regels_counter = 0

    for regel in regels_ws:
        regel_gebruikt = False
        for bericht in berichten:
            if regel.code in bericht.regels:
                regel_gebruikt = True
                break
        if not regel_gebruikt:
            for datatype in datatypen:
                if regel.code in datatype.regels:
                    regel_gebruikt = True
                    break
        if not regel_gebruikt:
            niet_gebruikte_regels_counter += 1
            if len(niet_gebruikte_regels) > 0:
                niet_gebruikte_regels += ", "
            niet_gebruikte_regels += regel.code

    if len(niet_gebruikte_regels) > 0:
        vervoeging_regelcode = "Regelcode"
        vervoeging_actie = "word"
        if niet_gebruikte_regels_counter > 1:
            vervoeging_regelcode += "s"
            vervoeging_actie += "en"
        else:
            vervoeging_actie += "t"
        raise IStdException(
            f'{vervoeging_regelcode} "{niet_gebruikte_regels}" '
            + f"{vervoeging_actie} niet gebruikt in enig bericht(element)."
        )


def import_regels_per_bericht_element_ws(
    regels_wb_file_path: str,
    regels_per_berichtelement_ws: str = "Regels per bericht(element)",
    regels_ws: List[IStdRegel] = None,
):
    """Converteer Sheet 'Regels per bericht(element)' uit 'Regelrapport-Excel'
    naar Berichten (List / Class Structuur)."""
    werkboek = load_workbook(filename=regels_wb_file_path)
    werksheet = werkboek[regels_per_berichtelement_ws]
    berichten = []
    datatypen = []

    __verwerk_werksheet(
        werksheet,
        berichten,
        datatypen,
        regels_wb_file_path,
        regels_per_berichtelement_ws,
    )

    _valideer_regels_gebruik(berichten, datatypen, regels_ws)

    return berichten, datatypen


def _verwerk_regels_cel_waarde(column_name: str, regel: IStdRegel, waarde):
    """Verwerkt de waarde uit een cel van het lijst met alle regels en verwerkt
    dat dan in de IStdRegel"""

    match column_name:
        case "type":
            regel.type = waarde
        case "regelcode":
            regel.code = waarde
        case "titel":
            regel.titel = waarde
        case "documentatie":
            regel.documentatie = waarde
        case "XSDrestrictie":
            regel.xsd_restrictie = waarde
        case "retourcode":
            regel.retourcode = waarde
        case "referentieregel":
            regel.referentieregel = waarde
        case _:
            if column_name.endswith("niveau"):
                regel.controleniveau = waarde
            else:
                print(f"ColumnName Not Mapped: ==> {column_name}")


def _verzamel_dubbele_regelcodes(
    werksheet,
    regelcode_index: int,
    regelcode: str,
    dubbele_regelcodes: List[str],
):
    """Controleer of de regelcode uit deze regel nog vaker voorkomt in deze
    werksheet; zo ja, voeg 'm dan toe aan {dubbele_regelcodes}"""

    if (
        len(
            [
                index
                for index, item in enumerate(werksheet)
                if item[regelcode_index].value == regelcode
            ]  # Statement returns a list of items for which the condition (if
            # item[...].value == regelcode) is true
        )
        > 1
    ):
        # Make sure the same duplicate does not get added multiple
        # times
        if regelcode not in dubbele_regelcodes:
            dubbele_regelcodes.append(regelcode)


def _controleer_dubbele_regelcodes(
    dubbele_regelcodes: List[str], tabblad: str
):
    """Wanneer in {dubbele_regelcodes} items staan, dan zorgen dat de juiste
    IStdException gegooid wordt (enkelvoud bij 1 dubbeling, meervoud bij >1
    dubbeling)"""

    if len(dubbele_regelcodes) > 0:
        if 1 == len(dubbele_regelcodes):
            raise IStdException(
                f'Regelcode "{dubbele_regelcodes[0]}" is meermalen '
                + f'gedefinieerd op tabblad "{tabblad}".'
            )

        # Meer dan 1 dubbele regelcode
        builder = ""
        for index, item in enumerate(dubbele_regelcodes):
            if 0 == index:
                builder = item
            else:
                builder = f"{builder}, {item}"
        raise IStdException(
            f'Regelcodes "{builder}" zijn meermalen gedefinieerd op '
            + f'tabblad "{tabblad}".'
        )


def import_regels_ws(
    regels_wb_file_path: str, regels_ws: str = "Regels"
) -> List[IStdRegel]:
    """convert Regel Rapport Excel File to HUGO Markdown Content Files"""
    # Load excel worksheet to process
    werksheet = load_workbook(filename=regels_wb_file_path)[regels_ws]
    column_names = []
    regels = []
    dubbele_regelcodes: List[str] = []
    regelcode_index: int = -1

    # process every row in the worksheet
    for row_index, ws_row in enumerate(werksheet):
        if 0 == row_index:
            # add column names from the first row
            for ws_cell in ws_row:
                column_names.append(camelize(ws_cell.value))
                if "regelcode" == camelize(ws_cell.value):
                    regelcode_index = len(column_names) - 1
        else:
            # First, check to see if the current "Regelcode" has duplicates in
            # this sheet
            _verzamel_dubbele_regelcodes(
                werksheet,
                regelcode_index,
                ws_row[regelcode_index].value,
                dubbele_regelcodes,
            )

            # get data row values for each Regel
            # create new iStandaard regel instance
            regel = IStdRegel()
            # process cell value for every column in the row
            for column_nr, ws_cell in enumerate(ws_row):
                # map column cell values to regel instance
                _verwerk_regels_cel_waarde(
                    column_names[column_nr], regel, ws_cell.value
                )

            # add processed regel instance
            regels.append(regel)

    _controleer_dubbele_regelcodes(dubbele_regelcodes, regels_ws)

    return regels


def __process_codelijsten_header(ws_rij) -> List[str]:
    """Process the first row in the codelijsten excel worksheet"""
    result: List[str] = []
    for ws_cell in ws_rij:
        result.append(camelize(ws_cell.value))

    return result


def __process_codelijsten_row(
    ws_rij,
    codelijst_worker: IStdCodeLijst,
    column_names: List[str],
    codelijsten: List[IStdCodeLijst],
) -> Tuple[IStdCodeItem, IStdCodeLijst]:
    """Process a data row from the codelijsten excel worksheet"""
    code_item = IStdCodeItem()

    for column_nr, ws_cell in enumerate(ws_rij):
        # map column cell values to regel instance
        column_name: str = column_names[column_nr]
        column_value: str = ws_cell.value

        match column_name:
            case "codelijst":
                codelijst_id_list = column_value.split(":")
                naam = str(codelijst_id_list[0]).strip()
                beschrijving = str(codelijst_id_list[1]).strip()

                if None is codelijst_worker:
                    # eerste gevonden codelijst
                    codelijst_worker = IStdCodeLijst(naam, beschrijving)
                    codelijsten.append(codelijst_worker)
                elif codelijst_worker.naam != naam:
                    # overgang naar volgende codelijst
                    codelijst_worker = IStdCodeLijst(naam, beschrijving)
                    codelijsten.append(codelijst_worker)
            case "code":
                code_item.code = column_value
            case "documentatie":
                code_item.documentatie = column_value
            case "mutatiedatum":
                if "" != column_value:
                    code_item.mutatiedatum = column_value
            case "mutatie":
                if "" != column_value:
                    code_item.mutatie = column_value
            case "ingangsdatum":
                if "" != column_value:
                    code_item.ingangsdatum = column_value
            case "expiratiedatum":
                if "" != column_value:
                    code_item.expiratiedatum = column_value
            case _:
                print(f"ColumnName Not Mapped: ==> {column_name}")
                print(f"columnValue Not Mapped: ==> {column_value}")

    return (code_item, codelijst_worker)


def import_codelijsten_ws(
    codelijst_wb_file_path: str, codelijsten_ws: str = None
):
    """convert Regel Rapport Excel File to HUGO Markdown Content Files"""
    # Load excel worksheet to process
    werkboek = load_workbook(filename=codelijst_wb_file_path)

    if None is codelijsten_ws:
        for wsx in werkboek:
            if wsx.title.startswith("Codelijsten"):
                codelijsten_ws = wsx.title

    werksheet = werkboek[codelijsten_ws]
    column_names: List[str] = []
    codelijsten: List[IStdCodeLijst] = []
    codelijst_worker: IStdCodeLijst = None

    # process every row in the worksheet
    for counter, ws_rij in enumerate(werksheet):
        if 0 == counter:
            # add column names from the first row
            column_names = __process_codelijsten_header(ws_rij)
        else:
            # create new iStandaard regel instance
            code_item = IStdCodeItem()
            code_item, codelijst_worker = __process_codelijsten_row(
                ws_rij,
                codelijst_worker,
                column_names,
                codelijsten,
            )

            if (
                f"{codelijst_worker.naam}: {codelijst_worker.beschrijving}"
                == code_item.code
            ):
                # first code lijst item
                codelijst_worker.documentatie = code_item.documentatie
            else:
                # next code lijst item
                codelijst_worker.code_items.append(code_item)

    return codelijsten
