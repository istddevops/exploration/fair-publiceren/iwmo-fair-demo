"""Test dat het omzetten van een XSD naar yaml formaat zonder fouten werkt."""
# package imports
import os

# local imports
from export_hugo_data import export_bericht_yaml
from import_bizz_xsds import import_bericht_xsd

# Interne Bouw test
test_input_path = os.path.join("iwmo-bronnen", "Release 3.0.4")
TEST_OUTPUT_PATH = "test-output"

# Interne Bouw test
BERICHTNAAM = "WMO305"
testBerichtImportFilePath = os.path.join(
    test_input_path, "xsds", f"{BERICHTNAAM}.xsd"
)
testExportPath = os.path.join("test-output", "data")
bericht = import_bericht_xsd(testBerichtImportFilePath)
print("Test " + BERICHTNAAM + " XSD Import")
print(
    "========================================================================"
)
print("testBerichtImportFilePath = " + testBerichtImportFilePath)
print("testExportPath = " + testExportPath)
print(
    "========================================================================"
)
export_bericht_yaml(bericht, testExportPath)
