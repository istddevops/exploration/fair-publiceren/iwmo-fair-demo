"""Maken van HUGO Markdown-formaat Content vanuit iStandaard-klassen
(istd_klassen)

Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts)
gepubliceerde XSDs

Auteur: A. Haldar (Onno)
"""

# package imports
import os
from datetime import date

# local imports
from istd_klassen import (
    IStdBericht,
    IStdCodeLijst,
    IStdDataType,
    IStdElement,
    IStdKlasse,
    IStdRegel,
)

from yaml_utils import build_yaml_str


def generatie_stempel():
    """Datum Stempel Generatie Content."""
    stempel = "\n** Generereerd door `"
    stempel = stempel + os.path.basename(__file__)
    stempel = stempel + "` op " + date.today().strftime("%d %B, %Y") + "**\n"
    return stempel


def build_front_matter_string(fromt_matter_object: any):
    """Markdown FrontMatter Object Parsing."""
    return "---\n" + build_yaml_str(fromt_matter_object) + "---\n"


def build_section_index_md_str(
    book_collapse_section: bool = True,
    title: str = "(titel is leeg)",
    description: str = "(beschrijving is leeg)",
):
    """HUGO Section Index String Parsing."""
    section_index_md: str = build_front_matter_string(
        {
            "bookCollapseSection": book_collapse_section,
            "title": title,
            "description": description,
        }
    )
    section_index_md += generatie_stempel() + "\n{{< sectiontable >}}\n"
    return section_index_md


def default_content_docs_path(content_path: str = "content"):
    """Standaard HUGO SSG Content Locatie."""
    return os.path.join(content_path, "docs")


def create_istd_codelijsten_hugo_book_content(
    codelijsten: IStdCodeLijst,
    content_path: str = default_content_docs_path(),
):
    """Maak Content voor CodeLijsten in Markdown-formaat."""

    # initialisatie
    if None is codelijsten:
        codelijsten = []
    codelijsten_content_path = os.path.join(content_path, "codelijsten")
    os.makedirs(codelijsten_content_path, exist_ok=True)
    # CodeLijsten index-MD
    codelijsten_content_index_file_path = os.path.join(
        codelijsten_content_path, "_index.md"
    )
    with open(
        codelijsten_content_index_file_path, "w", encoding="UTF-8"
    ) as codelijsten_content_index_file:
        print(
            build_section_index_md_str(
                title="CodeLijsten", description="iStandaard CodeLijsten"
            ),
            file=codelijsten_content_index_file,
        )
    # CodeLijst MD-bestanden
    for codelijst in codelijsten:
        codelijst_content_path = os.path.join(
            codelijsten_content_path, codelijst.naam
        )
        if not os.path.exists(codelijst_content_path):
            os.makedirs(codelijst_content_path, exist_ok=False)
            # CodeLijst index-bestand
            codelijst_content_index_file_path = os.path.join(
                codelijst_content_path, "_index.md"
            )
            with open(
                codelijst_content_index_file_path, "w", encoding="UTF-8"
            ) as codelijst_content_index_file:
                print(
                    build_front_matter_string(
                        {
                            "codeLijst": codelijst.naam,
                            "bookCollapseSection": True,
                        }
                    ),
                    file=codelijst_content_index_file,
                )
                print(generatie_stempel(), file=codelijst_content_index_file)
            # CodeItem md-bestanden
            for code_item in codelijst.codeItems:
                code_item_file_path = os.path.join(
                    codelijst_content_path, code_item.code + ".md"
                )
                with open(
                    code_item_file_path, "w", encoding="UTF-8"
                ) as code_item_content_file:
                    print(
                        build_front_matter_string(
                            {
                                "codeItem": {
                                    "codeLijst": codelijst.naam,
                                    "code": code_item.code,
                                }
                            }
                        ),
                        file=code_item_content_file,
                    )
                    print(generatie_stempel(), file=code_item_content_file)


def create_istd_regels_hugo_book_content(
    regels: IStdRegel, content_path: str = default_content_docs_path()
):
    """Maak Content voor Regels in Markdown-formaat."""

    # initialisatie
    if None is regels:
        regels = []
    regel_content_path = os.path.join(content_path, "regels")
    os.makedirs(regel_content_path, exist_ok=True)
    # Regel index-MD
    regel_content_index_file_path = os.path.join(
        regel_content_path, "_index.md"
    )
    with open(
        regel_content_index_file_path, "w", encoding="UTF-8"
    ) as regel_content_index_file:
        print(
            build_section_index_md_str(
                title="Regels", description="iStandaard Regels"
            ),
            file=regel_content_index_file,
        )
    # Type Regel MD-bestanden
    for regel in regels:
        regel_type_content_path = os.path.join(regel_content_path, regel.type)
        if not os.path.exists(regel_type_content_path):
            os.makedirs(regel_type_content_path, exist_ok=False)
            # Regel Type index-bestand
            regel_type_content_index_file_path = os.path.join(
                regel_type_content_path, "_index.md"
            )
            with open(
                regel_type_content_index_file_path, "w", encoding="UTF-8"
            ) as regel_type_content_index_file:
                print(
                    build_section_index_md_str(
                        title=regel.type, description="iStandaard Regeltype"
                    ),
                    file=regel_type_content_index_file,
                )
        # Regel MD-bestand
        regel_content_file_path = os.path.join(
            regel_type_content_path, regel.code + ".md"
        )
        with open(
            regel_content_file_path, "w", encoding="UTF-8"
        ) as regel_content_file:
            print(
                build_front_matter_string(
                    {
                        "regel": {
                            "type": regel.type,
                            "code": regel.code,
                            "documentatie": regel.documentatie,
                        }
                    }
                ),
                file=regel_content_file,
            )
            print(generatie_stempel(), file=regel_content_file)


def create_istd_datatypen_hugo_book_content(
    datatypen: IStdDataType, content_path: str = default_content_docs_path()
):
    """Maak HUGO Book Content voor Data Typen."""

    # initialisatie
    if None is datatypen:
        datatypen = []
    datatype_content_path = os.path.join(content_path, "datatypen")
    os.makedirs(datatype_content_path, exist_ok=True)
    # index-bestand
    datatype_content_index_file_path = os.path.join(
        datatype_content_path, "_index.md"
    )
    with open(
        datatype_content_index_file_path, "w", encoding="UTF-8"
    ) as datatype_content_index_file:
        print(
            build_section_index_md_str(
                title="Datatypen",
                description="iStandaard Gegevensbeschrijvingen",
            ),
            file=datatype_content_index_file,
        )
        for datatype in datatypen:
            datatype_content_file_path = os.path.join(
                datatype_content_path, datatype.naam + ".md"
            )
            with open(
                datatype_content_file_path, "w", encoding="UTF-8"
            ) as content_type_data_file:
                print(
                    build_front_matter_string({"dataType": datatype.naam}),
                    file=content_type_data_file,
                )
                print(generatie_stempel(), file=content_type_data_file)


def create_istd_element_hugo_book_content(
    berichtnaam: str,
    klassenaam: str,
    element: IStdElement,
    elementen_content_path: str = None,
):
    """Maak HUGO Book Theme Content voor Element."""
    # initialisatie
    element_content_file_path = os.path.join(
        elementen_content_path, element.naam.lower() + ".md"
    )
    # element-bestand
    with open(
        element_content_file_path, "w", encoding="UTF-8"
    ) as element_content_file:
        print(
            build_front_matter_string(
                {
                    "element": {
                        "bericht": berichtnaam,
                        "klasse": klassenaam,
                        "naam": element.naam,
                    }
                }
            ),
            file=element_content_file,
        )
        print(generatie_stempel(), file=element_content_file)


def create_istd_klasse_hugo_book_content(
    berichtnaam: str, klasse: IStdKlasse, klassen_content_path: str = None
):
    """Maak HUGO Book Theme Content voor Klassen en onderliggende Elementen."""
    # initialisatie
    klasse_content_dir_path = os.path.join(klassen_content_path, klasse.naam)
    os.makedirs(klasse_content_dir_path, exist_ok=True)
    elementen_content_dir_path = os.path.join(
        klasse_content_dir_path, "elementen"
    )
    os.makedirs(elementen_content_dir_path, exist_ok=True)
    # klasse-index-bestand
    klasse_content_index_file_path = os.path.join(
        klasse_content_dir_path, "_index.md"
    )
    with open(
        klasse_content_index_file_path, "w", encoding="UTF-8"
    ) as klasse_content_index_file:
        print(
            build_front_matter_string(
                {
                    "klasse": {"bericht": berichtnaam, "naam": klasse.naam},
                    "bookCollapseSection": True,
                }
            ),
            file=klasse_content_index_file,
        )
        print(generatie_stempel(), file=klasse_content_index_file)
    # elementen-index-bestand
    elementen_content_index_file_path = os.path.join(
        elementen_content_dir_path, "_index.md"
    )
    with open(
        elementen_content_index_file_path, "w", encoding="UTF-8"
    ) as elementen_content_indexfile:
        print(
            build_section_index_md_str(
                title="Elementen",
                description=f"{berichtnaam.upper()} / {klasse.naam}",
            ),
            file=elementen_content_indexfile,
        )
    # element-bestanden
    for element in klasse.elementen:
        create_istd_element_hugo_book_content(
            berichtnaam, klasse.naam, element, elementen_content_dir_path
        )


def create_istd_bericht_hugo_book_content(
    bericht: IStdBericht, bericht_content_path: str = None
):
    """Maak HUGO Book Theme Content voor Bericht en onderliggende Klassen en
    Elementen."""
    # initialisatie
    bericht_content_dir_path = os.path.join(bericht_content_path, bericht.naam)
    os.makedirs(bericht_content_dir_path, exist_ok=True)
    klassen_content_dir_path = os.path.join(
        bericht_content_dir_path, "klassen"
    )
    os.makedirs(klassen_content_dir_path, exist_ok=True)
    # bericht-index-bestand
    bericht_content_indexfile_path = os.path.join(
        bericht_content_dir_path, "_index.md"
    )
    with open(
        bericht_content_indexfile_path, "w", encoding="UTF-8"
    ) as bericht_content_index_file:
        print(
            build_front_matter_string(
                {"bericht": bericht.naam.upper(), "bookCollapseSection": True}
            ),
            file=bericht_content_index_file,
        )
        print(generatie_stempel(), file=bericht_content_index_file)
    # klassen-index-bestand
    klassen_content_index_file_path = os.path.join(
        klassen_content_dir_path, "_index.md"
    )
    with open(
        klassen_content_index_file_path, "w", encoding="UTF-8"
    ) as klassen_content_index_file:
        print(
            build_section_index_md_str(
                title="Klassen", description=bericht.naam.upper()
            ),
            file=klassen_content_index_file,
        )
    # klassen-bestanden
    for klasse in bericht.klassen:
        create_istd_klasse_hugo_book_content(
            bericht.naam, klasse, klassen_content_dir_path
        )


def create_istd_berichten_hugo_book_content(
    berichten: IStdBericht, content_path: str = default_content_docs_path()
):
    """Maak HUGO Book Theme Content voor iStandaard Berichten."""

    # initialisatie
    if None is berichten:
        berichten = []
    berichten_content_path = os.path.join(content_path, "berichten")
    os.makedirs(berichten_content_path, exist_ok=True)
    # berichten-index-bestand
    berichten_content_index_file_path = os.path.join(
        berichten_content_path, "_index.md"
    )
    with open(
        berichten_content_index_file_path, "w", encoding="UTF-8"
    ) as berichten_content_index_file:
        print(
            build_section_index_md_str(title="Berichten", description="PM"),
            file=berichten_content_index_file,
        )
    # bericht-bestanden
    for bericht in berichten:
        create_istd_bericht_hugo_book_content(bericht, berichten_content_path)
