"""Exporteren van HUGO Data in YAML-formaat vanuit
iStandaard-klassen (istd_klassen)

Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts)
gepubliceerde XSDs

Auteur: A. Haldar (Onno)
"""

# package imports
import os

# local imports
from istandaard.classes import (
    IStdBasisSchema,
    IStdBericht,
    IStdCodeLijst,
    IStdDataType,
    IStdRegel,
)
from yaml_utils import build_yaml_str


def default_data_path(root_path: str = ""):
    """Standaard HUGO SSG Data Locatie."""
    return os.path.join(root_path, "data")


def export_codelijst_yamls(
    codelijsten: IStdCodeLijst, export_path: str = default_data_path()
):
    """Exporteer codelijsten in YAML-formaat."""
    # initialisatie
    codelijst_path = os.path.join(export_path, "codelijst")
    os.makedirs(codelijst_path, exist_ok=True)
    if None is codelijsten:
        codelijsten = []
    # schrijf YAML-bestand
    for codelijst in codelijsten:
        codelijst_file_path = os.path.join(
            codelijst_path, f"{codelijst.naam}.yml"
        )
        with open(
            codelijst_file_path, "w", encoding="UTF-8"
        ) as codelijst_yaml:
            print(build_yaml_str(codelijst), file=codelijst_yaml)


def export_regel_yamls(
    regels: IStdRegel, export_path: str = default_data_path()
):
    """Exporteer regels in YAML-formaat."""
    # initialisatie
    regel_path = os.path.join(export_path, "regel")
    os.makedirs(regel_path, exist_ok=True)
    if None is regels:
        regels = []
    # schrijf YAML-bestand
    for regel in regels:
        # schrijf regel directory "type" / bestand "code"
        regel_type_path = os.path.join(regel_path, regel.type)
        os.makedirs(regel_type_path, exist_ok=True)
        regel_file_path = os.path.join(regel_type_path, regel.code + ".yml")
        with open(regel_file_path, "w", encoding="UTF-8") as regel_yaml:
            print(build_yaml_str(regel), file=regel_yaml)


def export_basisschema_yaml(
    basisschema: IStdBasisSchema, export_path: str = default_data_path()
):
    """Exporteer Basisschema Release-informatie over de Data Typen in
    YAML-formaat."""

    # initialisatie
    basisschema_path = os.path.join(export_path, "basisschema.yml")
    os.makedirs(export_path, exist_ok=True)
    # schrijf YAM-bestand
    with open(basisschema_path, "w", encoding="UTF-8") as basisschema_yaml:
        print(build_yaml_str(basisschema), file=basisschema_yaml)


def export_datatypen_yamls(
    datatypen: IStdDataType, export_path: str = default_data_path()
):
    """Exporteer datatypen in YAML-formaat."""
    # initialisatie
    datatype_path = os.path.join(export_path, "datatype")
    os.makedirs(datatype_path, exist_ok=True)
    if None is datatypen:
        datatypen = []
    # schrijf YAML-bestand
    for datatype in datatypen:
        datatype_file_path = os.path.join(
            datatype_path, f"{datatype.naam}.yml"
        )
        with open(datatype_file_path, "w", encoding="UTF-8") as datatype_yaml:
            print(build_yaml_str(datatype), file=datatype_yaml)


def export_bericht_yaml(
    bericht: IStdBericht, export_path: str = default_data_path()
):
    """Exporteer Bericht in YAML-formaat."""
    # initialisatie
    berichtdata_path = os.path.join(export_path, "bericht")
    os.makedirs(berichtdata_path, exist_ok=True)
    berichtdata_file_path = os.path.join(
        berichtdata_path, f"{bericht.naam}.yml"
    )
    # schrijf YAML-bestand
    with open(berichtdata_file_path, "w", encoding="UTF-8") as bericht_yaml:
        print(build_yaml_str(bericht), file=bericht_yaml)


def export_berichten_yamls(
    berichten: IStdBericht, export_path: str = default_data_path()
):
    """Exporteer Berichten in YAML-formaat."""
    # initialisatie
    if None is berichten:
        berichten = []
    # Exporteer Berichten
    for bericht in berichten:
        export_bericht_yaml(bericht, export_path)
