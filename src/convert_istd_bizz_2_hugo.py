"""Doel: iStandaard BizzDesign Output (Excels en XSDs) naar YMAL en Markdown

Formaat Conversie om:

- Daarmee te komen tot een basis Data-Beheer waarmee diverse outputs kunnen

  worden gegenereerd.

- Uiteindelijk te komen tot uitfasering van de huidige BizzDesign (legacy)

  Applicatie


Status: Verkenning op basis vanSSG HUGO (YAML-Data en Markdown-Content) output


Auteur: A. Haldar (Onno)

"""


# Package imports
import os
import sys

from istd_klassen import (
    IStdBasisSchema,
    IStdBericht,
    IStdCodeLijst,
    IStdDataType,
    IStdRegel,
)

from create_hugo_content import (
    create_istd_berichten_hugo_book_content,
    create_istd_codelijsten_hugo_book_content,
    create_istd_datatypen_hugo_book_content,
    create_istd_regels_hugo_book_content,
    default_content_docs_path,
)
from export_hugo_data import (
    default_data_path,
    export_basisschema_yaml,
    export_berichten_yamls,
    export_codelijst_yamls,
    export_datatypen_yamls,
    export_regel_yamls,
)
from import_bizz_xsds import import_basisschema_xsd, import_bericht_xsd
from import_bizz_xslxs import (
    import_codelijsten_ws,
    import_regels_per_bericht_element_ws,
    import_regels_ws,
)

# Initialisatie
IMPORT_PATH = sys.argv[1]

print("Import Path: " + IMPORT_PATH)

OUTPUT_PATH = ""

if len(sys.argv) > 2:
    OUTPUT_PATH = sys.argv[2]

print("Output Path: " + IMPORT_PATH)

berichten: IStdBericht = []

basisSchema: IStdBasisSchema
datatypen: IStdDataType = []
regels: IStdRegel = []
codeLijsten: IStdCodeLijst = []


# Importeer XSDs

importPathXsds = os.path.join(IMPORT_PATH, "xsds")

print("importPathXsds: " + importPathXsds)

for xsdFile in os.listdir(importPathXsds):
    xsdFileTst = xsdFile.lower()

    if xsdFileTst.endswith(".xsd"):
        # Alleen XSD-bestanden verwerken

        importXsdFilePath = os.path.join(importPathXsds, xsdFile)

        if not xsdFileTst == "basisschema.xsd":
            print("Importeer Berichtschema: " + xsdFile)

            berichten.append(import_bericht_xsd(importXsdFilePath))

        else:
            print("Importeer BasissSchema en Data Typen")

            basisSchema, datatypen = import_basisschema_xsd(importXsdFilePath)


# Importeer Excels
berichten_regels: IStdBericht = []
datatypen_regels: IStdDataType = []

import_path_excels = os.path.join(IMPORT_PATH, "excels")


for excel_file in os.listdir(import_path_excels):
    if excel_file.lower().endswith(".xlsx"):
        # Alleen Excel-bestanden

        excelFilePath = os.path.join(import_path_excels, excel_file)

        if excel_file.lower().startswith("regelrapport"):
            print("Importeer Regelrapport")
            (
                berichten_regels,
                datatypen_regels,
            ) = import_regels_per_bericht_element_ws(excelFilePath)

            regels = import_regels_ws(excelFilePath)

        elif excel_file.lower().startswith("codelijsten"):
            print("Importeer CodeLijsten")

            codeLijsten = import_codelijsten_ws(excelFilePath)

        else:
            print("Onbekende Import-Excel ==> " + excel_file)


# Voeg Regels toe aan Berichten

for bericht in berichten:
    for berichtRegels in berichten_regels:
        if bericht.naam == berichtRegels.naam:
            # Voeg regels toe op Bericht Niveau

            bericht.regels.extend(berichtRegels.regels)

            for klasse in bericht.klassen:
                for klasseRegels in berichtRegels.klassen:
                    if klasse.naam == klasseRegels.naam:
                        # Voeg regels toe op Klasse Niveau

                        klasse.regels.extend(klasseRegels.regels)

                        for element in klasse.elementen:
                            for elementRegels in klasseRegels.elementen:
                                if element.naam == elementRegels.naam:
                                    # Voeg regels toe op Element Niveau

                                    element.regels.extend(elementRegels.regels)


# Voeg Regels en Codelijst toe aan Data Typen

for datatype in datatypen:
    for datatype_regels in datatypen_regels:
        if datatype.naam == datatype_regels.naam:
            datatype.regels.extend(datatype_regels.regels)
            datatype.codeLijst = datatype_regels.codeLijst


# Exporteer YAML-Data

dataPath = default_data_path(OUTPUT_PATH)

print("dataPath = " + dataPath)

export_berichten_yamls(berichten, dataPath)

export_basisschema_yaml(basisSchema, dataPath)

export_datatypen_yamls(datatypen, dataPath)

export_regel_yamls(regels, dataPath)

export_codelijst_yamls(codeLijsten, dataPath)


# Maak Markdown-Content

contentPath = default_content_docs_path(os.path.join(OUTPUT_PATH, "content"))

print("contentPath = " + contentPath)

create_istd_berichten_hugo_book_content(berichten, contentPath)

create_istd_datatypen_hugo_book_content(datatypen, contentPath)

create_istd_regels_hugo_book_content(regels, contentPath)

create_istd_codelijsten_hugo_book_content(codeLijsten, contentPath)
