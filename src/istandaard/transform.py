"""Transformeert de istandaard klassen zoals gedefinieerd in classes.py, naar
het gewenste yml formaat."""


import inspect
from functools import wraps
from typing import List

YAML_NAMES = {}
SKIP_YAML_OBJECT = {}
YAML_CLASS_ATTRIBUTE_FOR_LIST_ITEM = {}


def _get_yaml_decorator_key(obj, decoratee) -> str:
    """Geeft een sleutel terug die gebruikt kan worden om gedecoreerde
    attributen te bewaren of op te zoeken.
    Wanneer decoratee geen __name__ attribuut heeft, dan gaan we er vanuit dat
    decoratee zelf de naam bevat."""

    decoratee_name: str = ""
    if hasattr(decoratee, "__name__"):
        decoratee_name = decoratee.__name__
    else:
        decoratee_name = decoratee
    return f"{type(obj).__qualname__}::{decoratee_name}"


def yaml_name(name: str):
    """Decorator waarmee je de naam waarmee een object in de YAML output wordt
    afgedrukt, kunt sturen. Dit wanneer de naam in YAML moet afwijken van de
    naam in de code."""

    def _yaml_name(decoratee):
        @wraps(decoratee)
        def __yaml_name(*args, **kwargs):
            yaml_key = _get_yaml_decorator_key(args[0], decoratee)
            if yaml_key not in YAML_NAMES:
                YAML_NAMES[yaml_key] = name
            return decoratee(*args, **kwargs)

        return __yaml_name

    return _yaml_name


def yaml_use_class_attribute_for_list_item(attribute: str):
    """Decorator waarmee je kunt zorgen dat in de YAML output niet een klasse,
    maar een attribuut van deze klasse wordt afgedrukt."""

    def _yaml_use_class_attribute_for_list_item(decoratee):
        @wraps(decoratee)
        def __yaml_use_class_attribute_for_list_item(*args, **kwargs):
            yaml_key = _get_yaml_decorator_key(args[0], decoratee)
            if yaml_key not in YAML_CLASS_ATTRIBUTE_FOR_LIST_ITEM:
                YAML_CLASS_ATTRIBUTE_FOR_LIST_ITEM[yaml_key] = attribute
            return decoratee(*args, **kwargs)

        return __yaml_use_class_attribute_for_list_item

    return _yaml_use_class_attribute_for_list_item


def no_yaml_output(decoratee):
    """Decorator die ervoor zorgt dat een object NIET in de YAML output wordt
    afgedrukt."""

    @wraps(decoratee)
    def _no_yaml_output(*args, **kwargs):
        yaml_key = _get_yaml_decorator_key(args[0], decoratee)
        if yaml_key not in SKIP_YAML_OBJECT:
            SKIP_YAML_OBJECT[yaml_key] = ""
        return decoratee(*args, **kwargs)

    return _no_yaml_output


def _genereer_filler(niveau: int) -> str:
    """Genereert voorloopspaties t.b.v. inspringen in yaml"""

    result: str = ""

    for counter in range(0, niveau):  # pylint:disable=[W0612]
        result += "  "

    return result


def _transformeer_builtin_naar_yaml(data, niveau: int = 0) -> str:
    """Transformeert builtin typed data naar yaml.
    - Wanneer het een string betreft:
      - Wanneer deze string bestaat uit meerdere regels, dan deze string als
        een array weergeven, waarbij per regeleinde wordt gesplitst.
      - Elke (regel uit de) string dient te worden omgeven door single quotes.
    - Wanneer het een ander builtin type betreft, de string representatie van
      het object, zonder single quotes, naar de YAML printen."""

    result: str = ""

    if isinstance(data, str):
        if "\n" in data:
            result = _transformeer_list_naar_yaml(data.splitlines(), niveau)
        else:
            result = f"'{str(data)}'"
    else:
        result = str(data)

    return result


def _transformeer_list_naar_yaml(
    data: list,
    niveau: int = 0,
    yaml_decorator_key: str = None,
) -> str:
    """Transformeert list met data naar yaml. Elke regel wordt ingesprongen op
    het juiste {niveau}"""

    filler: str = _genereer_filler(niveau)

    result = ""

    forced_attribute: str = None
    if (
        yaml_decorator_key is not None
        and yaml_decorator_key in YAML_CLASS_ATTRIBUTE_FOR_LIST_ITEM
    ):
        forced_attribute = YAML_CLASS_ATTRIBUTE_FOR_LIST_ITEM[
            yaml_decorator_key
        ]

    for list_item in data:
        if "builtins" == list_item.__class__.__module__.lower():
            worker = _transformeer_builtin_naar_yaml(list_item, niveau)
        elif forced_attribute is not None:
            worker = _transformeer_builtin_naar_yaml(
                getattr(list_item, forced_attribute), niveau
            )
        else:
            worker = _transformeer_object_van_type_naar_yaml(
                list_item, None, niveau + 1
            )
        spacer = ""
        if not worker.startswith("\n"):
            spacer = " "
        result = f"{result}\n{filler}-{spacer}{worker}"

    return result


def _transformeer_dict_naar_yaml(data: dict, niveau: int = 0) -> str:
    """Transformeert een dictionary naar yaml. Elke regel wordt ingesprongen op
    het juiste {niveau}"""

    filler: str = _genereer_filler(niveau)
    result = ""

    for key, value in data.items():
        result = f"{result}\n{filler}- {key}:"
        dict_item_yaml: str = _transformeer_object_van_type_naar_yaml(
            value, None, niveau + 1
        )
        spacer = ""
        if not dict_item_yaml.startswith("\n"):
            spacer = " "
        result = f"{result}{spacer}{dict_item_yaml}"

    return result


def _transformeer_object_van_type_naar_yaml(
    waarde,
    naam: str = None,
    niveau: int = 0,
    yaml_decorator_key: str = None,
) -> str:
    """Transformeert {obj} naar yaml, en retourneert deze yaml als string.
    Hierbij wordt ingesprongen met {niveau} * 2 spaties.
    Afhankelijk van het type van obj wordt de yaml op een andere manier gegene-
    reert."""

    filler: str = _genereer_filler(niveau)
    worker = ""

    actual_type = type(waarde).__qualname__.lower()
    match actual_type:
        case "list":
            worker = _transformeer_list_naar_yaml(
                waarde, niveau, yaml_decorator_key
            )
        case "dict":
            worker = _transformeer_dict_naar_yaml(waarde, niveau)
        case _:
            if "builtins" != waarde.__class__.__module__.lower():
                new_niveau = niveau
                if None is not naam and len(naam) > 0:
                    new_niveau += 1
                klasse = transformeer_object_naar_yaml(waarde, new_niveau)
                worker = f"{worker}\n{klasse}"
            else:
                worker = _transformeer_builtin_naar_yaml(waarde, niveau)

    if None is naam:
        return worker
    else:
        spacer = ""
        if not worker.startswith("\n"):
            spacer = " "

        return f"{filler}{naam}:{spacer}{worker}"


def transformeer_object_naar_yaml(obj, niveau: int = 0) -> str:
    """Transformeert {obj} naar yaml, en retourneert deze yaml als string.
    Hierbij wordt ingesprongen met {niveau} * 2 spaties"""

    result_list: List[str] = []
    # Alle members van bericht doorlopen om ze al dan niet naar yml om te
    # zetten
    for found_member in inspect.getmembers(obj):
        # Geen members verwerken die beginnen met een underscore.
        # Geen members verwerken wanneer het methods zijn
        member_name = found_member[0]
        member_instance = found_member[1]
        if (
            not member_name.startswith("_")
            and not inspect.ismethod(member_instance)
            and member_instance is not None
        ):
            yaml_decorator_key = _get_yaml_decorator_key(obj, member_name)
            # Skip, when decorated with no_yaml_output
            if yaml_decorator_key not in SKIP_YAML_OBJECT:
                naam = member_name
                if yaml_decorator_key in YAML_NAMES:
                    naam = YAML_NAMES[yaml_decorator_key]

                result_list.append(
                    _transformeer_object_van_type_naar_yaml(
                        member_instance, naam, niveau, yaml_decorator_key
                    )
                )

    return "\n".join(result_list)
