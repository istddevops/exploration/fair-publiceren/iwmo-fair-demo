"""iStandaard Klassen voor de conversie van het Informatiemodel van BizzDesign
naar Open Formaten

Status: Verkenning op basis van huidige (vanuit BizzDesign-scripts)
gepubliceerde XSDs

Auteur: A. Haldar (Onno)
"""

from typing import Dict, List

from src.istandaard.transform import (
    no_yaml_output,
    yaml_name,
    yaml_use_class_attribute_for_list_item,
)


class IStdException(Exception):
    """Exception wanneer het XSD niet overeenkomt met de verwachte export
    vanuit Bizzdesign"""


# pylint: disable=[R0902]
class IStdBasisSchema:
    """Basisschema
    - beschrijving van release DataTypen"""

    def __init__(self, standaard: str):
        self._standaard = standaard
        self._release = "x.y.z"
        self._xsd_versie = "x.y.z"
        self._xsd_min_versie = "x.y.z"
        self._xsd_max_versie = "x.y.z"

    def get_standaard(self) -> str:
        """Get standaard property value"""
        return self._standaard

    def set_standaard(self, value: str):
        """Set standaard property value"""
        self._standaard = value

    def get_release(self) -> str:
        """Get release property value"""
        return self._release

    def set_release(self, value: str):
        """Set release property value"""
        self._release = value

    def get_xsd_versie(self) -> str:
        """Get xsd_versie property value"""
        return self._xsd_versie

    def set_xsd_versie(self, value: str):
        """Set xsd_versie property value"""
        self._xsd_versie = value

    def get_xsd_min_versie(self) -> str:
        """Get xsd_min_versie property value"""
        return self._xsd_min_versie

    def set_xsd_min_versie(self, value: str):
        """Set xsd_min_versie property value"""
        self._xsd_min_versie = value

    def get_xsd_max_versie(self) -> str:
        """Get xsd_max_versie property value"""
        return self._xsd_versie

    def set_xsd_max_versie(self, value: str):
        """Set xsd_max_versie property value"""
        self._xsd_versie = value

    standaard: str = property(fget=get_standaard, fset=set_standaard)
    release: str = property(fget=get_release, fset=set_release)
    xsd_versie: str = property(fget=get_xsd_versie, fset=set_xsd_versie)
    xsd_min_versie: str = property(
        fget=get_xsd_min_versie, fset=set_xsd_min_versie
    )
    xsd_max_versie: str = property(
        fget=get_xsd_max_versie, fset=set_xsd_max_versie
    )


class XsdRestriction:
    """Restriction op simple type"""

    def __init__(self):
        self._basis_type: str = None
        self._max_lengte: int = None
        self._min_lengte: int = None
        self._min_waarde = None
        self._max_waarde = None
        self._patroon: str = None

    def get_basis_type(self) -> str:
        """Get _basis_type value"""
        return self._basis_type

    def set_basis_type(self, value: str):
        """Set _basis_type value"""
        self._basis_type = value

    def get_max_lengte(self) -> int:
        """Get _max_lengte value"""
        return self._max_lengte

    def set_max_lengte(self, value: int):
        """Set _max_lengte value"""
        self._max_lengte = value

    def get_min_lengte(self) -> int:
        """Get _min_lengte value"""
        return self._min_lengte

    def set_min_lengte(self, value: int):
        """Set _min_lengte value"""
        self._min_lengte = value

    def get_min_waarde(self):
        """Get _min_waarde value"""
        return self._min_waarde

    def set_min_waarde(self, value):
        """Set _min_waarde value"""
        self._min_waarde = value

    def get_max_waarde(self):
        """Get _max_waarde value"""
        return self._max_waarde

    def set_max_waarde(self, value):
        """Set _max_waarde value"""
        self._max_waarde = value

    def get_patroon(self) -> str:
        """Get _patroon value"""
        return self._patroon

    def set_patroon(self, value: str):
        """Set _patroon value"""
        self._patroon = value

    basis_type: str = property(fget=get_basis_type, fset=set_basis_type)
    """XSD => ['xs:simpleType']['xs:restriction']['@base']"""

    max_lengte: int = property(fget=get_max_lengte, fset=set_max_lengte)
    """XSD =>
    ['xs:simpleType']['xs:restriction']['xs:maxLength']['@value']"""

    min_lengte: int = property(fget=get_min_lengte, fset=set_min_lengte)
    """XSD =>
    ['xs:simpleType']['xs:restriction']['xs:minLength']['@value']"""

    min_waarde = property(fget=get_min_waarde, fset=set_min_waarde)
    """XSD =>
    ['xs:simpleType']['xs:restriction']['xs:minInclusive']['@value']"""

    max_waarde = property(fget=get_max_waarde, fset=set_max_waarde)
    """XSD =>
    ['xs:simpleType']['xs:restriction']['xs:maxInclusive']['@value']"""

    patroon: str = property(fget=get_patroon, fset=set_patroon)
    """XSD =>
    ['xs:simpleType']['xs:restriction']['xs:pattern']['@value']"""


# pylint: disable=["R0902"]
class IStdRegel:
    """Regel:
    - is uniek binnen de verzameling regels
    - is optioneel gekoppeld aan referentie-regel"""

    def __init__(self):
        self._type: str = None
        self._code: str = None
        self._titel: str = None
        self._documentatie: str = None
        self._xsd_restrictie: str = None
        self._retourcode: str = None
        self._controleniveau: str = None
        self._referentieregel: str = None

    def get_type(self) -> str:
        """Get _type value"""
        return self._type

    def set_type(self, value: str):
        """Set _type value"""
        self._type = value

    def get_code(self) -> str:
        """Get _code value"""
        return self._code

    def set_code(self, value: str):
        """Set _code value"""
        self._code = value

    def get_titel(self) -> str:
        """Get _titel value"""
        return self._titel

    def set_titel(self, value: str):
        """Set _titel value"""
        self._titel = value

    def get_documentatie(self) -> str:
        """Get _documentatie value"""
        return self._documentatie

    def set_documentatie(self, value: str):
        """Set _documentatie value"""
        self._documentatie = value

    def get_xsd_restrictie(self) -> str:
        """Get _xsd_restrictie value"""
        return self._xsd_restrictie

    def set_xsd_restrictie(self, value: str):
        """Set _xsd_restrictie value"""
        self._xsd_restrictie = value

    def get_retourcode(self) -> str:
        """Get _retourcode value"""
        return self._retourcode

    def set_retourcode(self, value: str):
        """Set _retourcode value"""
        self._retourcode = value

    def get_controleniveau(self) -> str:
        """Get _controleniveau value"""
        return self._controleniveau

    def set_controleniveau(self, value: str):
        """Set _controleniveau value"""
        self._controleniveau = value

    def get_referentieregel(self) -> str:
        """Get _referentieregel value"""
        return self._referentieregel

    def set_referentieregel(self, value: str):
        """Set _referentieregel value"""
        self._referentieregel = value

    type: str = property(fget=get_type, fset=set_type)
    code: str = property(fget=get_code, fset=set_code)
    titel: str = property(fget=get_titel, fset=set_titel)
    documentatie: str = property(fget=get_documentatie, fset=set_documentatie)
    xsd_restrictie: str = property(
        fget=get_xsd_restrictie, fset=set_xsd_restrictie
    )
    retourcode: str = property(fget=get_retourcode, fset=set_retourcode)
    controleniveau: str = property(
        fget=get_controleniveau, fset=set_controleniveau
    )
    referentieregel: str = property(
        fget=get_referentieregel, fset=set_referentieregel
    )


class IStdDataType:
    """iStandaard Bericht Data Type Conversie Klasse

    DataType:
    - kan meerdere elementen bevatten
    - kan worden beperkt door een codelijst
    - en er kunnen meerdere regels voor gelden"""

    def __init__(self, naam: str):
        self._naam: str = naam
        self._beschrijving: str = None
        self._elementen: Dict[str, IStdElement] = {}
        self._is_complex = False
        self._codelijst: str = None
        self._regels: List[IStdRegel] = []
        self._restriction: XsdRestriction = None

    def get_naam(self) -> str:
        """Get _naam value"""
        return self._naam

    def set_naam(self, value: str):
        """Set _naam value"""
        self._naam = value

    def get_beschrijving(self) -> str:
        """Get _beschrijving value"""
        return self._beschrijving

    def set_beschrijving(self, value: str):
        """Set _beschrijving value"""
        self._beschrijving = value

    def get_elementen(self) -> dict:
        """Get _elementen value"""
        return self._elementen

    def set_elementen(self, value: dict):
        """Set _elementen value"""
        if not isinstance(value, Dict[str, IStdElement]):
            raise TypeError(
                "value must be of type Dict[str, IStdElement], "
                f"but was {type(value)}"
            )
        self._elementen = value

    def get_is_complex(self) -> bool:
        """Get _is_complex value"""
        return self._is_complex

    def set_is_complex(self, value: bool):
        """Set _is_complex value"""
        self._is_complex = value

    def get_codelijst(self) -> str:
        """Get _codelijst value"""
        return self._codelijst

    def set_codelijst(self, value: str):
        """Set _codelijst value"""
        self._codelijst = value

    def get_regels(self) -> List[IStdRegel]:
        """Get _regels value"""
        return self._regels

    def set_regels(self, value: List[IStdRegel]):
        """Set _regels value"""
        self._regels = value

    def get_restriction(self) -> XsdRestriction:
        """Get _restriction value"""
        return self._restriction

    def set_restriction(self, value: XsdRestriction):
        """Set _restriction value"""
        self._restriction = value

    naam = property(fget=get_naam, fset=set_naam)
    """XSD => ['xs:...Type']['@name']"""
    beschrijving = property(fget=get_beschrijving, fset=set_beschrijving)
    """XSD => ['xs:...Type']['xs:annotation']['xs:documentation']"""
    elementen = property(fget=get_elementen, fset=set_elementen)
    """XSD => [...]['xs:sequence']['xs:element']"""
    is_complex = property(fget=get_is_complex, fset=set_is_complex)
    """Geeft aan of het een complexType is of een simpleType"""
    codelijst = property(fget=get_codelijst, fset=set_codelijst)
    """De codelijst die van toepassing is op dit datatype"""
    regels = property(fget=get_regels, fset=set_regels)
    """De lijst met regels die van toepassing zijn op dit datatype"""
    restriction = property(fget=get_restriction, fset=set_restriction)
    """De Xsd restriction die van toepassing is op dit datatype"""


class IStdElement:
    """iStandaard Bericht Element Conversie Klasse

    Element:
    - heeft een datatype
    - kan wel of geen sleutel zijn
    - en er kunnen meerdere regels voor gelden"""

    def __init__(self, naam: str):
        self._naam: str = naam
        self._beschrijving: str = None
        self._datatype: str = None
        self._verplicht: bool = False
        self._min_occurs = None
        self._max_occurs = None
        self._patroon: str = None
        self._min_waarde = None
        self._max_waarde = None
        self._waarde: str = None
        self._is_subklasse: bool = False
        self._sleutel: bool = None
        self._regels: List[IStdRegel] = None

    @property
    def naam(self) -> str:
        """XSD => ['xs:element']['@name']"""
        return self._naam

    @naam.setter
    def naam(self, value: str):
        """Set _naam"""
        self._naam = value

    @property
    def beschrijving(self) -> str:
        """XSD => ['xs:element']['xs:annotation']['xs:documentation']"""
        return self._beschrijving

    @beschrijving.setter
    def beschrijving(self, value: str):
        """Set _beschrijving"""
        self._beschrijving = value

    @property
    @yaml_name("dataType")
    def datatype(self) -> str:
        """XSD => ['xs:element']['@type']"""
        return self._datatype

    @datatype.setter
    def datatype(self, value: str):
        """Set _datatype"""
        self._datatype = value

    @property
    @no_yaml_output
    def verplicht(self) -> bool:
        """XSD => ['xs:element']['@minOccurs'] != '0'"""
        return None is not self.min_occurs and int(self.min_occurs) > 0

    @property
    @yaml_name("minOccurs")
    def min_occurs(self):
        """XSD => ['xs:element']['@minOccurs']"""
        return self._min_occurs

    @min_occurs.setter
    def min_occurs(self, value: str):
        """Set _min_occurs"""
        self._min_occurs = value

    @property
    @yaml_name("maxOccurs")
    def max_occurs(self):
        """XSD => ['xs:element']['@maxOccurs']"""
        return self._max_occurs

    @max_occurs.setter
    def max_occurs(self, value: str):
        """Set _max_occurs"""
        self._max_occurs = value

    @property
    def patroon(self) -> str:
        """XSD => ['xs:element']['xs:pattern']['@value']"""
        return self._patroon

    @patroon.setter
    def patroon(self, value: str):
        """Set _patroon"""
        self._patroon = value

    @property
    @yaml_name("minValue")
    def min_waarde(self):
        """XSD => ['xs:element']['xs:minInclusive']['@value']"""
        return self._min_waarde

    @min_waarde.setter
    def min_waarde(self, value: str):
        """Set _min_waarde"""
        self._min_waarde = value

    @property
    @yaml_name("maxValue")
    def max_waarde(self):
        """XSD => ['xs:element']['xs:maxInclusive']['@value']"""
        return self._max_waarde

    @max_waarde.setter
    def max_waarde(self, value: str):
        """Set _max_waarde"""
        self._max_waarde = value

    @property
    @no_yaml_output
    def is_subklasse(self) -> bool:
        """Get _is_subklasse"""
        return self._is_subklasse

    @is_subklasse.setter
    def is_subklasse(self, value: bool):
        """Set _is_subklasse"""
        self._is_subklasse = value

    @property
    def sleutel(self) -> bool:
        """Get _sleutel"""
        return self._sleutel

    @sleutel.setter
    def sleutel(self, value: bool):
        """Set _sleutel"""
        self._sleutel = value

    @property
    @yaml_use_class_attribute_for_list_item("code")
    def regels(self) -> List[IStdRegel]:
        """Get _regels"""
        return self._regels

    @regels.setter
    def regels(self, value: List[IStdRegel]):
        """Set _regels"""
        self._regels = value

    @property
    def waarde(self) -> str:
        """Get _waarde"""
        return self._waarde

    @waarde.setter
    def waarde(self, value: str):
        """Set _waarde"""
        self._waarde = value


class IStdKlasse:
    """Klasse:
    - bestaat uit meerdere elementen
    - en er kunnen meerdere regels voor gelden"""

    def __init__(self, naam: str):
        self._naam: str = naam
        self._beschrijving: str = None
        self._regels: List[IStdRegel] = []
        self._elementen: Dict[str, IStdElement] = {}
        # Toegevoegd door Koen Schonhoven:
        # Een klasse kan ook subklassen hebben.
        # Dit was opgelost in IStdAggregatieRelatie, maar
        # nu gerefactored in IStdKlasse
        self._subklassen: Dict[str, IStdKlasse] = {}
        self._datatype: IStdDataType = None

    @property
    def naam(self) -> str:
        """Get _naam"""
        return self._naam

    @naam.setter
    def naam(self, value: str):
        """Set _naam"""
        self._naam = value

    @property
    def beschrijving(self) -> str:
        """Get _beschrijving"""
        return self._beschrijving

    @beschrijving.setter
    def beschrijving(self, value: str):
        """Set _beschrijving"""
        self._beschrijving = value

    @property
    @yaml_use_class_attribute_for_list_item("code")
    def regels(self) -> List[IStdRegel]:
        """Get _regels"""
        return self._regels

    @regels.setter
    def regels(self, value: List[IStdRegel]):
        """Set _regels"""
        self._regels = value

    @property
    def elementen(self) -> Dict[str, IStdElement]:
        """Get _elementen"""
        return self._elementen

    @elementen.setter
    def elementen(self, value: Dict[str, IStdElement]):
        """Set _elementen"""
        self._elementen = value

    @property
    @no_yaml_output
    def subklassen(self) -> dict:
        """Get _subklassen"""
        return self._subklassen

    @subklassen.setter
    def subklassen(self, value: dict):
        """Set _subklassen"""
        self._subklassen = value

    @property
    @no_yaml_output
    def datatype(self) -> IStdDataType:
        """Get _datatype"""
        return self._datatype

    @datatype.setter
    def datatype(self, value: IStdDataType):
        """Set _datatype"""
        self._datatype = value


class IStdAggregatieRelatie:
    """Aggregatie Relatie
    - Is van parent-Klasse naar child-Klasse
    - Geeft Min - Max voorkomens van de child-Klasse aan (default = 1 - 1)"""

    def __init__(self, naam: str):
        self._naam: str = naam
        self._parent_klasse: str = None
        self._child_klasse: str = None
        self._child_min: str = "1"
        self._child_max: str = "1"

    def get_naam(self) -> str:
        """Get _naam"""
        return self._naam

    def set_naam(self, value: str):
        """Set _naam"""
        self._naam = value

    def get_parent_klasse(self) -> str:
        """Get _parent_klasse"""
        return self._parent_klasse

    def set_parent_klasse(self, value: str):
        """Set _parent_klasse"""
        self._parent_klasse = value

    def get_child_klasse(self) -> str:
        """Get _child_klasse"""
        return self._child_klasse

    def set_child_klasse(self, value: str):
        """Set _child_klasse"""
        self._child_klasse = value

    def get_child_min(self) -> str:
        """Get _child_min"""
        return self._child_min

    def set_child_min(self, value: str):
        """Set _child_min"""
        self._child_min = value

    def get_child_max(self) -> str:
        """Get _child_max"""
        return self._child_max

    def set_child_max(self, value: str):
        """Set _child_max"""
        self._child_max = value

    naam: str = property(fget=get_naam, fset=set_naam)
    parent_klasse: str = property(
        fget=get_parent_klasse, fset=set_parent_klasse
    )
    child_klasse: str = property(fget=get_child_klasse, fset=set_child_klasse)
    child_min: str = property(fget=get_child_min, fset=set_child_min)
    child_max: str = property(fget=get_child_max, fset=set_child_max)


class IStdBericht:
    """Bericht:
    - Bestaat uit meerdere klassen
    - Kunnen meerdere regels voor gelden
    - Meerdere aggregatie-relaties tussen klassen"""

    def __init__(self, naam: str):
        self._naam: str = naam
        self._standaard: str = "istd"
        self._release: str = "x.y.z"
        self._bericht_xsd_versie: str = "x.y.z"
        self._bericht_xsd_min_versie: str = "x.y.z"
        self._bericht_xsd_max_versie: str = "x.y.z"
        self._basisschema_xsd_versie: str = "x.y.z"
        self._basisschema_xsd_min_versie: str = "x.y.z"
        self._basisschema_xsd_max_versie: str = "x.y.z"
        self._beschrijving: str = None
        self._regels: List[str] = []
        self._klassen: Dict[str, IStdKlasse] = {}
        self._relaties: Dict[str, IStdAggregatieRelatie] = {}

    @property
    @no_yaml_output
    def naam(self) -> str:
        """Get _naam"""
        return self._naam

    @naam.setter
    def naam(self, value: str):
        """Set _naam"""
        self._naam = value

    @property
    @yaml_name("naam")
    def naam_upper(self) -> str:
        """Get _naam, to upper case"""
        return self._naam.upper()

    @property
    def standaard(self) -> str:
        """Get _standaard"""
        return self._standaard

    @standaard.setter
    def standaard(self, value: str):
        """Set _standaard"""
        self._standaard = value

    @property
    def release(self) -> str:
        """Get _release"""
        return self._release

    @release.setter
    def release(self, value: str):
        """Set _release"""
        self._release = value

    @property
    @yaml_name("berichtXsdVersie")
    def bericht_xsd_versie(self) -> str:
        """Get _bericht_xsd_versie"""
        return self._bericht_xsd_versie

    @bericht_xsd_versie.setter
    def bericht_xsd_versie(self, value: str):
        """Set _bericht_xsd_versie"""
        self._bericht_xsd_versie = value

    @property
    @yaml_name("berichtXsdMinVersie")
    def bericht_xsd_min_versie(self) -> str:
        """Get _bericht_xsd_min_versie"""
        return self._bericht_xsd_min_versie

    @bericht_xsd_min_versie.setter
    def bericht_xsd_min_versie(self, value: str):
        """Set _bericht_xsd_min_versie"""
        self._bericht_xsd_min_versie = value

    @property
    @yaml_name("berichtXsdMaxVersie")
    def bericht_xsd_max_versie(self) -> str:
        """Get _bericht_xsd_max_versie"""
        return self._bericht_xsd_max_versie

    @bericht_xsd_max_versie.setter
    def bericht_xsd_max_versie(self, value: str):
        """Set _bericht_xsd_max_versie"""
        self._bericht_xsd_max_versie = value

    @property
    @yaml_name("basisschemaXsdVersie")
    def basisschema_xsd_versie(self) -> str:
        """Get _basisschema_xsd_versie"""
        return self._basisschema_xsd_versie

    @basisschema_xsd_versie.setter
    def basisschema_xsd_versie(self, value: str):
        """Set _basisschema_xsd_versie"""
        self._basisschema_xsd_versie = value

    @property
    @yaml_name("basisschemaXsdMinVersie")
    def basisschema_xsd_min_versie(self) -> str:
        """Get _basisschema_xsd_min_versie"""
        return self._basisschema_xsd_min_versie

    @basisschema_xsd_min_versie.setter
    def basisschema_xsd_min_versie(self, value: str):
        """Set _basisschema_xsd_min_versie"""
        self._basisschema_xsd_min_versie = value

    @property
    @yaml_name("basisschemaXsdMaxVersie")
    def basisschema_xsd_max_versie(self) -> str:
        """Get _basisschema_xsd_max_versie"""
        return self._basisschema_xsd_max_versie

    @basisschema_xsd_max_versie.setter
    def basisschema_xsd_max_versie(self, value: str):
        """Set _basisschema_xsd_max_versie"""
        self._basisschema_xsd_max_versie = value

    @property
    def beschrijving(self) -> str:
        """Get _beschrijving"""
        return self._beschrijving

    @beschrijving.setter
    def beschrijving(self, value: str):
        """Set _beschrijving"""
        self._beschrijving = value

    @property
    def regels(self) -> List[str]:
        """Get _regels"""
        return self._regels

    @regels.setter
    def regels(self, value: List[str]):
        """Set _regels"""
        self._regels = value

    @property
    def klassen(self) -> Dict[str, IStdKlasse]:
        """Get _klassen"""
        return self._klassen

    @klassen.setter
    def klassen(self, value: Dict[str, IStdKlasse]):
        """Set _klassen"""
        self._klassen = value

    @property
    def relaties(self) -> Dict[str, IStdAggregatieRelatie]:
        """Get _relaties"""
        return self._relaties

    @relaties.setter
    def relaties(self, value: Dict[str, IStdAggregatieRelatie]):
        """Set _relaties"""
        self._relaties = value


class IStdCodeItem:
    """Codelijst Item"""

    def __init__(self):
        self._code: str = None
        self._documentatie: str = None
        self._mutatiedatum: str = None
        self._mutatie: str = None
        self._ingangsdatum: str = None
        self._expiratiedatum: str = None

    def get_code(self) -> str:
        """Get _code"""
        return self._code

    def set_code(self, value: str):
        """Set _code"""
        self._code = value

    code: str = property(fget=get_code, fset=set_code)

    def get_documentatie(self) -> str:
        """Get _documentatie"""
        return self._documentatie

    def set_documentatie(self, value: str):
        """Set _documentatie"""
        self._documentatie = value

    documentatie: str = property(fget=get_documentatie, fset=set_documentatie)

    def get_mutatiedatum(self) -> str:
        """Get _mutatiedatum"""
        return self._mutatiedatum

    def set_mutatiedatum(self, value: str):
        """Set _mutatiedatum"""
        self._mutatiedatum = value

    mutatiedatum: str = property(fget=get_mutatiedatum, fset=set_mutatiedatum)

    def get_mutatie(self) -> str:
        """Get _mutatie"""
        return self._mutatie

    def set_mutatie(self, value: str):
        """Set _mutatie"""
        self._mutatie = value

    mutatie: str = property(fget=get_mutatie, fset=set_mutatie)

    def get_ingangsdatum(self) -> str:
        """Get _ingangsdatum"""
        return self._ingangsdatum

    def set_ingangsdatum(self, value: str):
        """Set _ingangsdatum"""
        self._ingangsdatum = value

    ingangsdatum: str = property(fget=get_ingangsdatum, fset=set_ingangsdatum)

    def get_expiratiedatum(self) -> str:
        """Get _expiratiedatum"""
        return self._expiratiedatum

    def set_expiratiedatum(self, value: str):
        """Set _expiratiedatum"""

    expiratiedatum: str = property(
        fget=get_expiratiedatum, fset=set_expiratiedatum
    )


class IStdCodeLijst:
    """Codelijst"""

    def __init__(self, naam: str, beschrijving: str):
        self._naam: str = naam
        self._beschrijving: str = beschrijving
        self._documentatie: str = None
        self._code_items: List[IStdCodeItem] = []

    def get_naam(self) -> str:
        """Get _naam"""
        return self._naam

    def set_naam(self, value: str):
        """Set _naam"""
        self._naam = value

    naam: str = property(fget=get_naam, fset=set_naam)

    def get_beschrijving(self) -> str:
        """Get _beschrijving"""
        return self._beschrijving

    def set_beschrijving(self, value: str):
        """Set _beschrijving"""
        self._beschrijving = value

    beschrijving: str = property(fget=get_beschrijving, fset=set_beschrijving)

    def get_documentatie(self) -> str:
        """Get _documentatie"""
        return self._documentatie

    def set_documentatie(self, value: str):
        """Set _documentatie"""
        self._documentatie = value

    documentatie: str = property(fget=get_documentatie, fset=set_documentatie)

    def get_code_items(self) -> List[IStdCodeItem]:
        """Get _code_items"""
        return self._code_items

    def set_code_items(self, value: List[IStdCodeItem]):
        """Set _code_items"""
        self._code_items = value

    code_items: List[IStdCodeItem] = property(
        fget=get_code_items, fset=set_code_items
    )
