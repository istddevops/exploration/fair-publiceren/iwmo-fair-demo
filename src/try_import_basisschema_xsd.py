"""Test dat het omzetten van een basisschema XSD naar yaml formaat zonder
fouten werkt."""
# package imports
import os

# local imports
from export_hugo_data import export_basisschema_yaml, export_datatypen_yamls
from import_bizz_xsds import import_basisschema_xsd

# Interne Bouw test
test_input_path = os.path.join("iwmo-bronnen", "Release 3.0.4")
TEST_OUTPUT_PATH = "test-output"

test_basisschema_import_file_path = os.path.join(
    test_input_path, "xsds", "basisschema.xsd"
)
test_export_path = os.path.join("test-output", "data")
basisschema, datatypen = import_basisschema_xsd(
    test_basisschema_import_file_path
)
print("Test BasisSchema XSD Import")
print(
    "========================================================================"
)
print("testBasisSchemaImportFilePath = " + test_basisschema_import_file_path)
print("testExportPath = " + test_export_path)
print(
    "========================================================================"
)
export_basisschema_yaml(basisschema, test_export_path)
export_datatypen_yamls(datatypen, test_export_path)
