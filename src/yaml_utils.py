"""Helper methodes om het interne objectmodel te vertalen naar yaml"""

from yaml import dump


def build_yaml_str(te_vertalen_object: any):
    """Zet te_vertalen_object om naar YAML"""
    yaml_dump: str = dump(te_vertalen_object)
    yaml_lines: str = ""

    for line in yaml_dump.split("\n"):
        next_line: str = str(line)
        test = next_line.strip()

        if test.startswith("!!") or test.startswith("- !!"):
            # Volgende regel is een Commentaar-regel
            yaml_lines = yaml_lines + next_line.replace("!!", "# ", 1) + "\n"

        elif not (test.endswith("null") or test.endswith("[]")):
            # Volgende regels is NIET Leeg
            yaml_lines = yaml_lines + next_line + "\n"

    return yaml_lines
