"""Test dat het omzetten van codelijsten uit een XSD naar yaml formaat zonder
fouten werkt."""
# package imports
import os

# local imports
from export_hugo_data import export_codelijst_yamls
from import_bizz_xslxs import import_codelijsten_ws

# Interne Bouw test
test_input_path = os.path.join("iwmo-bronnen", "Release 3.0.4")
TEST_OUTPUT_PATH = "test-output"

test_codelijst_import_file_path = os.path.join(
    test_input_path, "excels", "codelijsten-iwmo-3.0.4.xlsx"
)
test_export_path = os.path.join("test-output", "data")
codelijsten = import_codelijsten_ws(test_codelijst_import_file_path)
print("Test Codelijsten Excel Import")
print(
    "========================================================================"
)
print("testCodeLijstImportFilePath = " + test_codelijst_import_file_path)
print("testExportPath = " + test_export_path)
print(
    "========================================================================"
)
export_codelijst_yamls(codelijsten, test_export_path)
