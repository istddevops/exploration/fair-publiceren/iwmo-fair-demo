"""CLI voor istandaard_naar_Hugo. Hiermee kunnen de functies
- import_basisschema_xsd
- import_bericht_xsd
- import_codelijsten_ws
- import_regels_ws
aangeroepen worden."""

import os
import shlex
import sys
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import List

from src.import_bizz_xsds import import_bericht_xsd
from src.import_bizz_xslxs import import_codelijsten_ws, import_regels_ws
from src.istandaard.classes import (
    IStdBericht,
    IStdCodeLijst,
    IStdException,
    IStdRegel,
)
from src.istandaard.transform import transformeer_object_naar_yaml

foutrapport: List[str] = []

# Methods doing stuff with arguments


def _setup_arguments(parser: ArgumentParser) -> ArgumentParser:
    """Voeg de command-line argumenten toe voor istandaard_naar_hugo"""
    # Of --istandaard òf --istandaard-input-folder mag gevuld zijn, niet beiden
    group_istandaard = parser.add_mutually_exclusive_group()
    group_istandaard.add_argument(
        "--istandaard",
        nargs="*",
        help="Het volledige pad naar de xsd(s) van de berichtdefinitie",
    )
    group_istandaard.add_argument(
        "--istandaard-input-folder",
        action="store",
        help="Het volledige pad naar de folder waar alle xsd's van de "
        + "iStandaard staan",
    )
    parser.add_argument(
        "--basisschema",
        action="store",
        help="Het volledige pad naar de xsd van het basisschema",
    )
    parser.add_argument(
        "--codelijsten",
        action="store",
        help="Het volledige pad naar de excel (.xlsx) van de codelijsten",
    )
    parser.add_argument(
        "--berichtregels",
        action="store",
        help="Het volledige pad naar de excel (.xlsx) met de bedrijfsregels "
        + "die horen bij een specifiek bericht",
    )
    parser.add_argument(
        "--regels",
        action="store",
        help="Het volledige pad naar de excel (.xlsx) met de generieke "
        + "bedrijfsregels",
    )
    parser.add_argument(
        "--yaml-output-folder",
        action="store",
        help="Het volledige pad naar de folder waar de machine-leesbare "
        + "YAML bestanden geplaatst moeten worden",
    )

    return parser


def _check_arguments(args: Namespace):
    """Controleer of er input argumenten meegegeven zijn. Indien dit niet zo
    is, eindig dan het programma met code -1"""
    if not (
        hasattr(args, "istandaard")
        or hasattr(args, "basisschema")
        or hasattr(args, "codelijsten")
        or hasattr(args, "berichtregels")
        or hasattr(args, "regels")
    ):
        sys.exit(-1)
    if (
        args.istandaard is None
        and args.basisschema is None
        and args.codelijsten is None
        and args.berichtregels is None
        and args.regels is None
    ):
        sys.exit(-1)


def _process_argv_arguments() -> str:
    """Process command line arguments, using "old-skool" sys.argv array"""
    result = ""
    for argv in sys.argv[1:]:
        if argv.startswith("--"):
            result = f"{result} {argv}"
        else:
            result = f'{result} "{argv}"'
    return result


# Methods doing stuff for a given iStandaard xsd file / folder of xsd files


def _check_xsd_file(xsd_path: str):
    """Controleert of de meegegeven xsd pad verwijst naar een bestaand en
    valide pad en xsd bestand."""
    xsd_file = Path(xsd_path)

    if not xsd_file.exists() or not xsd_file.is_file():
        raise FileNotFoundError(f"{xsd_path} bestaat niet of is geen bestand")
    if not xsd_path.lower().endswith(".xsd"):
        raise ValueError(f"{xsd_path} is geen .xsd file")


def _check_and_process_istandaarden_folder(
    bericht_xsd_folder,
) -> List[IStdBericht]:
    """Voor de folder --istandaard-input-folder; controleert dat het een geldig
    en bestaand pad is, en verwerkt elk .xsd bestand in deze folder tot een
    IStdBericht"""

    result: List[IStdBericht] = []

    if os.path.exists(bericht_xsd_folder):
        # pylint:disable=[W0612]
        for dirpath, directories, files in iter(os.walk(bericht_xsd_folder)):
            for file in files:
                if file.endswith(".xsd") and not file.startswith(
                    "basisschema"
                ):
                    result.append(
                        _check_and_process_istandaard(f"{dirpath}/{file}")
                    )

    return result


def _check_and_process_istandaarden(bericht_xsd_arg) -> List[IStdBericht]:
    """Voor elk voorkomen van --istandaard; controleert dat het verwijst
    naar een geldig pad en xsd file, en verwerkt deze dan tot een
    IStdBericht"""

    result: List[IStdBericht] = []

    if None is not bericht_xsd_arg:
        if len(bericht_xsd_arg) == 1:
            result.append(_check_and_process_istandaard(bericht_xsd_arg[0]))
        else:
            # Het is een lijst met meerdere waarden (meermalen --istandaard
            # opties meegegeven)
            for bericht_xsd in bericht_xsd_arg:
                result.append(_check_and_process_istandaard(bericht_xsd))
    return result


def _check_and_process_istandaard(bericht_xsd_arg: str) -> IStdBericht:
    """Controleert bericht_xsd_arg, en wanneer deze valide is, verwerk deze
    tot een IStdBericht"""

    result: IStdBericht = None

    try:
        _check_xsd_file(bericht_xsd_arg)
    except FileNotFoundError as file_not_found_error:
        foutrapport.append(f"{str(file_not_found_error)}")
    except ValueError as value_error:
        foutrapport.append(f"{str(value_error)}")

    try:
        result = import_bericht_xsd(bericht_xsd_arg)
    except IStdException as std_ex:
        foutrapport.append(f"{str(std_ex)}")

    return result


# Methods doing stuff for a regelrapport Excel file


def _check_xslx_file(xslx_path: str):
    """Controleert of het meegegeven xslx pad verwijst naar een bestaand en
    valide pad en xslx bestand."""
    xslx_file = Path(xslx_path)

    if not xslx_file.exists() or not xslx_file.is_file():
        raise FileNotFoundError(f"{xslx_path} bestaat niet of is geen bestand")
    if not xslx_path.lower().endswith(".xlsx"):
        raise ValueError(f"{xslx_path} is geen .xslx file")


def _check_and_process_excel(file_path, process_function) -> list:
    """Voor het gegeven bestand {file_path}; controleert dat het verwijst naar
    een geldig pad en xslx file, en roept dan de verwerkende methode aan"""

    result = []

    if None is not file_path and len(file_path) > 0:
        try:
            _check_xslx_file(file_path)
        except FileNotFoundError as file_not_found_error:
            foutrapport.append(f"{str(file_not_found_error)}")
        except ValueError as value_error:
            foutrapport.append(f"{str(value_error)}")
        try:
            result = process_function(file_path)
        except IStdException as std_ex:
            foutrapport.append(f"# {file_path}  \n\n- {str(std_ex)}")

    return result


def _check_and_process_berichtregels(berichtregel_arg):
    """Voor de --berichtregels; controleert dat het verwijst naar een geldig
    pad en xslx file, en verwerkt deze dan tot IStdRegel voorkomens."""

    return _check_and_process_excel(berichtregel_arg, import_regels_ws)


# Methods doing stuff for a codelijst Excel file


def _check_and_process_codelijst(codelijsten_arg) -> List[IStdCodeLijst]:
    """Voor de --codelijsten; controleert dat het verwijst naar een geldig pad
    en xslx file, en verwerkt deze dan tot IStdCodeLijst voorkomens."""

    return _check_and_process_excel(codelijsten_arg, import_codelijsten_ws)


# Methods doing stuff for an error report


def _generate_error_report(parser: ArgumentParser) -> str:
    """If foutrapport contains items, then build an error string and exit this
    process with error code 2, acompanied by the built error string"""

    if len(foutrapport) > 0:
        builder: str = ""
        for fout in foutrapport:
            if 0 == len(builder):
                builder = f"{fout}  "
            else:
                builder = f"{builder}\n\n{fout}  "

        parser.exit(2, builder)


# Methods doing stuff for outputting the iStandaard set of documents as YAML
# files


def _geef_subfolder_voor_regel(regel: IStdRegel) -> str:
    """Geeft, aan de hand van het soort regel, de subfolder terug waarin de
    vertaalde YAML geplaatst zou moeten worden."""

    match regel.code[0:2]:
        case "UP":
            return "Uitgangspunt"
        case "OP":
            return "Bedrijfsregel"
        case "TR":
            return "Technische regel"
        case "IV":
            return "Invulinstructie"
        case "CS":
            return "Constraint"
        case "CD":
            return "Conditie"


def _write_yaml_output(yaml: str, filename: str):
    """Schrijft de YAML weg naar opgegeven bestand"""

    directory = os.path.split(filename)[0]

    if (
        directory is not None
        and len(directory) > 0
        and not os.path.exists(directory)
    ):
        os.makedirs(directory)
    with open(filename, mode="wt", encoding="UTF-8") as output_file:
        output_file.writelines(yaml.splitlines(keepends=True))


def _generate_and_write_yaml_output(
    args: Namespace,
    berichten: List[IStdBericht],
    regels: List[IStdRegel],
    codelijst: List[IStdCodeLijst],
):
    """If --yaml-output-folder argument is set, then translate the list of
    IStdBericht items to YAML format."""

    if None is not args.yaml_output_folder:
        if None is not berichten:
            for bericht in berichten:
                _write_yaml_output(
                    transformeer_object_naar_yaml(bericht),
                    f"{args.yaml_output_folder}/bericht/{bericht.naam}.yml",
                )
        if None is not regels:
            for regel in regels:
                subfolder: str = _geef_subfolder_voor_regel(regel)
                _write_yaml_output(
                    transformeer_object_naar_yaml(regel),
                    (
                        f"{args.yaml_output_folder}/regels/{subfolder}/"
                        + f"{regel.code}.yml"
                    ),
                )
        if None is not codelijst:
            for code in codelijst:
                _write_yaml_output(
                    transformeer_object_naar_yaml(code),
                    f"{args.yaml_output_folder}/codelijst/{code.naam}.yml",
                )


# Main


def main(arg_strings=None):
    """Main function for CLI"""

    # Zorgen dat deze global eerst geleegd wordt, in het geval er meerdere
    # calls op dezelfde instance gedaan worden.
    del foutrapport[:]

    print(f'main called with arg_strings "{arg_strings}"')
    print(f"sys.argv: {str(sys.argv)}")
    new_arg_strings = arg_strings
    if None is arg_strings and len(sys.argv) > 1:
        new_arg_strings = _process_argv_arguments()
        print(
            "...took arguments from sys.argv: "
            + f"now called with '{new_arg_strings}'"
        )
    arg_strings = new_arg_strings

    parser = ArgumentParser(
        prog="istandaard_naar_hugo",
    )

    parser = _setup_arguments(parser)

    if None is not arg_strings:
        args = parser.parse_args(shlex.split(arg_strings))
    else:
        args = Namespace()

    _check_arguments(args)

    berichten: List[IStdBericht] = None
    if None is not args.istandaard:
        berichten: List[IStdBericht] = _check_and_process_istandaarden(
            args.istandaard
        )
    elif None is not args.istandaard_input_folder:
        berichten: List[IStdBericht] = _check_and_process_istandaarden_folder(
            args.istandaard_input_folder
        )

    regels: List[IStdRegel] = _check_and_process_berichtregels(
        args.berichtregels
    )

    codelijst: List[IStdCodeLijst] = _check_and_process_codelijst(
        args.codelijsten
    )

    _generate_error_report(parser)

    # Als er geen fouten geconstateerd zijn, dan over naar het genereren van de
    # output. Op dit moment ondersteunen we alleen YAML.
    _generate_and_write_yaml_output(args, berichten, regels, codelijst)


if __name__ == "__main__":
    print("__main__", flush=True)
    print(f"{str(sys.argv)}", flush=True)
    main()
