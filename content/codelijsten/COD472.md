---
# python/object:istd_klassen.IstdCodeLijst
beschrijving: Soort relatie
codeItems:
- # python/object:istd_klassen.IstdCodeItem
  code: '03'
  documentatie: Curator (juridisch)
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '04'
  documentatie: Financieel (gemachtigd)
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '05'
  documentatie: Financieel (toetsing)
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '06'
  documentatie: Leefeenheid
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '07'
  documentatie: Hulpverlener
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: 08
  documentatie: Specialist
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: 09
  documentatie: Anders
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '10'
  documentatie: Ouder
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '11'
  documentatie: Voogd
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '12'
  documentatie: Partner/echtgeno(o)t(e)
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '13'
  documentatie: Pleegouder
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '14'
  documentatie: Bewindvoerder
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '15'
  documentatie: Mentor
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '16'
  documentatie: Zoon/dochter
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '17'
  documentatie: Familielid
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '18'
  documentatie: Gezinslid
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '19'
  documentatie: Buur
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '20'
  documentatie: Vriend(in)/kennis
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '21'
  documentatie: Clientondersteuner
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '22'
  documentatie: Huisarts
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '23'
  documentatie: Erven van
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '24'
  documentatie: Werkgever
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '25'
  documentatie: Wijkcoach
  mutatie: niet gespecificeerd
documentatie: De soort relatie die een persoon ten opzichte van de client heeft.
naam: COD472

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

