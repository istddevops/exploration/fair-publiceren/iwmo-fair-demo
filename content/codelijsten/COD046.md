---
# python/object:istd_klassen.IstdCodeLijst
beschrijving: Geslacht
codeItems:
- # python/object:istd_klassen.IstdCodeItem
  code: '0'
  documentatie: Onbekend
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '1'
  documentatie: Mannelijk
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '2'
  documentatie: Vrouwelijk
  mutatie: niet gespecificeerd
documentatie: Unieke aanduiding van de sexe van een persoon.
naam: COD046

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

