---
# python/object:istd_klassen.IstdCodeLijst
beschrijving: Datumgebruik
codeItems:
- # python/object:istd_klassen.IstdCodeItem
  code: '1'
  documentatie: dag onbekend; alleen maand en jaar gebruiken
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '2'
  documentatie: dag en maand onbekend; alleen jaar gebruiken
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '3'
  documentatie: dag, maand en jaar onbekend; onbekende datum
  mutatie: niet gespecificeerd
documentatie: Codering om aan te geven welk deel van een datum onbekend is.
naam: COD170

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

