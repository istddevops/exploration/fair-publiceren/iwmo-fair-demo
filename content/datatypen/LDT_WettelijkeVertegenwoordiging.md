---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Wettelijke vertegenwoordiging die voor de client van toepassing is op het moment
  van beoordeling.
codeLijst: WJ003
maxLengte: '2'
naam: LDT_WettelijkeVertegenwoordiging

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

