---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De officiele door de gemeente vastgestelde naam van een straat.
maxLengte: '24'
minLengte: '1'
naam: LDT_Straatnaam
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

