---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Verzameling van een of meer voorzetsels/lidwoorden, die aan het significante deel
  van de achternaam van een persoon vooraf gaat en daar een onderdeel van is.
maxLengte: '10'
minLengte: '1'
naam: LDT_Voorvoegsel
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

