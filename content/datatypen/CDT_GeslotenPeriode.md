---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Tijdsperiode met zowel begin- als einddatum
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De begindatum van de periode.
  dataType: LDT_Datum
  naam: Begindatum
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De einddatum van de periode.
  dataType: LDT_Datum
  naam: Einddatum
naam: CDT_GeslotenPeriode
regels:
- CS023
- RS032
- CS108

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

