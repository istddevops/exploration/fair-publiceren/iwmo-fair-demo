---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De soort relatie die een persoon ten opzichte van de client heeft.
codeLijst: COD472
maxLengte: '2'
naam: LDT_SoortRelatie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

