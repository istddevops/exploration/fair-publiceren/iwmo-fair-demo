---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Identificerend nummer van een natuurlijk persoon.
maxLengte: '20'
minLengte: '1'
naam: LDT_Persoonsid
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

