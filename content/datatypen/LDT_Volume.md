---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Aanduiding van de mate van ondersteuning betreffende een product, onderdeel van
  de Omvang.
maxWaarde: '99999999'
minWaarde: '0'
naam: LDT_Volume
regels:
- RS001
- RS005

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

