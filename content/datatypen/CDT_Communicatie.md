---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Aanduiding over bijzondere vorm van communicatie die gebruikt dient te worden.
codeLijst: COD747
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Gecodeerde aanduiding van de bijzondere vorm van communicatie die gebruikt dient
    te worden.
  dataType: LDT_Communicatievorm
  naam: Vorm
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De taal van de client waarin met de client wordt gecommuniceerd.
  dataType: LDT_Taal
  naam: Taal
  verplicht: false
naam: CDT_Communicatie
regels:
- CD007
- RS021
- RS033

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

