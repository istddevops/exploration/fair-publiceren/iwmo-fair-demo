---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Een alfabetisch teken achter het huisnummer zoals dit door het gemeentebestuur is
  toegekend.
maxLengte: '1'
minLengte: '1'
naam: LDT_Huisletter
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

