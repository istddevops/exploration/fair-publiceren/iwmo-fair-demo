---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Aanduiding van de frequentie waarmee een ondersteuningsproduct geleverd wordt.
maxLengte: '1'
naam: LDT_Frequentie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

