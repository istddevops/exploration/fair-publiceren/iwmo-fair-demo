---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Aanduiding van de eenheid waarin de omvang van een ondersteuningsproduct is beschreven.
codeLijst: WJ756
maxLengte: '2'
naam: LDT_Eenheid

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

