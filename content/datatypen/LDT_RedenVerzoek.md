---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Reden waarom een verzoek wordt ingediend
codeLijst: WJ758
maxLengte: '1'
naam: LDT_RedenVerzoek

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

