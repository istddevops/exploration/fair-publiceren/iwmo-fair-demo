---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Indicatie over de status van de informatie in de berichtklasse.
codeLijst: COD467
maxLengte: '1'
naam: LDT_StatusAanlevering

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

