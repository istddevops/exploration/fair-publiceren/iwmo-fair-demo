---
# python/object:istd_klassen.IstdDataType
beschrijving:
- De achternaam van een natuurlijk persoon, aangeduid als Naam met Voorvoegsel.
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De achternaam van een persoon, indien nodig verkort weergegeven.
  dataType: LDT_Naam
  naam: Achternaam
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Verzameling van een of meer voorzetsels/lidwoorden, die aan het significante deel
    van de achternaam van een persoon vooraf gaat en daar een onderdeel van is.
  dataType: LDT_Voorvoegsel
  naam: Voorvoegsel
  verplicht: false
naam: CDT_Achternaam

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

