---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Naam of organisatienaam van de persoon of instantie die de client heeft doorverwezen
  naar ondersteuning.
maxLengte: '64'
minLengte: '1'
naam: LDT_NaamVerwijzer
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

