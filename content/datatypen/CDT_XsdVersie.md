---
# python/object:istd_klassen.IstdDataType
elementen:
- # python/object:istd_klassen.IstdElement
  dataType: LDT_Versie
  naam: BasisschemaXsdVersie
- # python/object:istd_klassen.IstdElement
  dataType: LDT_Versie
  naam: BerichtXsdVersie
naam: CDT_XsdVersie
regels:
- IV034
- CS128

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

