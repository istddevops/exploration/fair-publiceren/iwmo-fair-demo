---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Samengesteld bedrag met DebetCredit indicatie.
codeLijst: COD043
elementen:
- # python/object:istd_klassen.IstdElement
  dataType: LDT_Bedrag
  naam: Bedrag
- # python/object:istd_klassen.IstdElement
  dataType: LDT_DebetCredit
  naam: DebetCredit
naam: CDT_BedragMetDC
regels:
- IV024
- RS001
- RS005
- CS325

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

