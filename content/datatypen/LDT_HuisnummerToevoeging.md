---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Die letters of tekens die noodzakelijk zijn om, naast huisnummer en -letter, de
  brievenbus te vinden.
maxLengte: '4'
minLengte: '1'
naam: LDT_HuisnummerToevoeging
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

