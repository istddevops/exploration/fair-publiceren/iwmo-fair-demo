---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Volgnummer van de formele uitgifte van een versie.
minLengte: '1'
naam: LDT_Versie
regels:
- CS128
waarde: (0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

