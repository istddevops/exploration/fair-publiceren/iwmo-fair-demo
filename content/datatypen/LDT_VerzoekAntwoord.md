---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Het antwoord op een verzoek
codeLijst: WJ760
maxLengte: '1'
naam: LDT_VerzoekAntwoord

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

