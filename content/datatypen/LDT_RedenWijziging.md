---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De reden waarom een toewijzing wordt gewijzigd.
codeLijst: WMO002
maxLengte: '2'
naam: LDT_RedenWijziging

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

