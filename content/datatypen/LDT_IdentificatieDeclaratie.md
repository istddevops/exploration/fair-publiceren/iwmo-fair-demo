---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Identificatie van een declaratie zoals bepaald door de zorgaanbieder
maxLengte: '12'
minLengte: '1'
naam: LDT_IdentificatieDeclaratie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

