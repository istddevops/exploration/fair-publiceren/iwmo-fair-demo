---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Retour totaal bedragen.
codeLijst: COD043
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het bedrag dat in zijn totaliteit gedeclareerd of gefactureerd is.
  dataType: CDT_TotaalBedragMetDC
  naam: IngediendTotaalBedrag
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het totale bedrag dat de betaler toekent aan de declarant.
  dataType: CDT_TotaalBedragMetDC
  naam: ToegekendTotaalBedrag
naam: CDT_RetourBedragen
regels:
- IV024
- RS001
- RS040
- CS325

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

