---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Uniek nummer waaronder de gemeente een declaratiebestand van een aanbieder in zijn
  systeem heeft geregistreerd.
maxLengte: '24'
minLengte: '1'
naam: LDT_GemeenteReferentieNummer

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

