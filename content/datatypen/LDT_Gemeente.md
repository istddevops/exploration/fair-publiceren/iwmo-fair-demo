---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Identificerende code van een gemeente die betrokken is bij de uitvoering van zorg
  of ondersteuning.
minLengte: '1'
naam: LDT_Gemeente
regels:
- CS300
- RS035
waarde: '[0-9]{4}'

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

