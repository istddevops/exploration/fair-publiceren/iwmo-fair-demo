---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De sekse van een persoon, zoals bij geboorte formeel vastgesteld of nadien formeel
  gewijzigd.
codeLijst: COD046
maxLengte: '1'
naam: LDT_Geslacht

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

