---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Naam of nummer en dagtekening ter identificatie van een totale aanlevering.
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Naam of nummer, die ter identificatie aan een totale aanlevering kan worden meegegeven.
  dataType: LDT_IdentificatieBericht
  naam: Identificatie
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De datum van verzending van het bericht.
  dataType: LDT_Datum
  naam: Dagtekening
naam: CDT_BerichtIdentificatie
regels:
- TR056
- RS017
- RS033
- CS064
- RS032

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

