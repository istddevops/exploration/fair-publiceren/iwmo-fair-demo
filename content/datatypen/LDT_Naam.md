---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De naam van een persoon, indien nodig verkort weergegeven.
maxLengte: '200'
minLengte: '1'
naam: LDT_Naam
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

