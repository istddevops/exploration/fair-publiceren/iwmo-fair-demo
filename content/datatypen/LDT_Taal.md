---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De taal van de client waarin met de client wordt gecommuniceerd.
maxLengte: '25'
minLengte: '1'
naam: LDT_Taal
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

