---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Samengestelde DeclaratieFactuurgegevens over Declaratieperiode, Factuurnummer, Dagtekening
  en BTW.
codeLijst: COD363
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Eerste en laatste dag van een administratieve periode waarop de gegevensuitwisseling
    betrekking heeft.
  dataType: CDT_GeslotenPeriode
  naam: DeclaratiePeriode
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - DeclarantFactuurNummer dient uniek te zijn (uniek voor een periode van 5 jaar).
    Een factuurnummer mag hergebruikt wordt in geval van een technische afkeuring.
  dataType: LDT_IdentificatieBericht
  naam: DeclarantFactuurNummer
  waarde: ([a-zA-Z0-9])+
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Datum van ondertekening van een declaratie door de declarant.
  dataType: LDT_Datum
  naam: FactuurDagtekening
- # python/object:istd_klassen.IstdElement
  dataType: LDT_BtwIDNummer
  naam: BtwIDNummer
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Valuta waarin alle bedragen zijn uitgedrukt.
  dataType: LDT_ValutaCode
  naam: ValutaCode
naam: CDT_DeclaratieFactuurIdentificatie
regels:
- TR318
- CS023
- RS032
- CS108
- CS319
- TR324
- RS017
- RS033
- CS064
- CD055
- CS320
- RS039

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

