---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Nadere typering van het adres.
codeLijst: COD757
maxLengte: '2'
naam: LDT_AdresSoort

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

