---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Totaalbedrag in 1/100 van gehanteerde valuta. Bijvoorbeeld 10000 is tienduizend
  eurocent, is 100 Euro.
maxWaarde: '999999999999'
minWaarde: '0'
naam: LDT_TotaalBedrag

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

