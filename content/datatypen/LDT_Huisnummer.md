---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- De numerieke aanduiding zoals de gemeente die aan het object heeft toegekend.
maxWaarde: '99999'
minWaarde: '0'
naam: LDT_Huisnummer

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

