---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Vrije tekst (bijvoorbeeld een toelichting) in een bericht.
minLengte: '1'
naam: LDT_Commentaar
regels:
- RS031
waarde: ([.\s]*\S+[.\s]*)+

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

