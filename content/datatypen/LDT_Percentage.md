---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Percentage in honderdste procenten. Bijvoorbeeld 9000 is negenduizend honderdste
  procent, is 90%.
maxWaarde: '99999'
minWaarde: '0'
naam: LDT_Percentage

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

