---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Aanduiding van een specifiek huis in een straat, onderdeel van het adres.
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De numerieke aanduiding zoals de gemeente die aan het object heeft toegekend.
  dataType: LDT_Huisnummer
  naam: Huisnummer
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Een alfabetisch teken achter het huisnummer zoals dit door het gemeentebestuur
    is toegekend.
  dataType: LDT_Huisletter
  naam: Huisletter
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Die letters of tekens die noodzakelijk zijn om, naast huisnummer en -letter, de
    brievenbus te vinden.
  dataType: LDT_HuisnummerToevoeging
  naam: HuisnummerToevoeging
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De aanduiding die wordt gebruikt voor adressen die niet zijn voorzien van de gebruikelijke
    straatnaam en huisnummeraanduidingen.
  dataType: LDT_AanduidingWoonadres
  naam: AanduidingWoonadres
  verplicht: false
naam: CDT_Huis

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

