---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De reden van afwijzing van een verzoek.
codeLijst: WJ759
maxLengte: '2'
naam: LDT_RedenAfwijzingVerzoek

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

