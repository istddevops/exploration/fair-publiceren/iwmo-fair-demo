---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De reden van beeindiging van Wmo ondersteuning bij een client.
codeLijst: WMO588
maxLengte: '2'
naam: LDT_RedenBeeindiging

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

