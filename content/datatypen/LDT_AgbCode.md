---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Identificerende code van een instelling of aanbieder van zorg of ondersteuning (zie
  https://www.agbcode.nl)
maxLengte: '8'
minLengte: '1'
naam: LDT_AgbCode
regels:
- RS014
- RS036
waarde: '[0-9]{8}'

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

