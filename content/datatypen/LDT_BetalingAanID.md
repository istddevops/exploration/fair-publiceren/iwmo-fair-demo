---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Verwijzing naar degene, die de betaling dient te ontvangen.
codeLijst: COD833
maxLengte: '2'
naam: LDT_BetalingAanID

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

