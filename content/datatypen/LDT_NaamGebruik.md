---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Aanduiding naamgebruik (gecodeerd).
maxLengte: '1'
naam: LDT_NaamGebruik

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

