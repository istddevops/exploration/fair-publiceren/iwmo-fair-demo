---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De naam van een organisatie waar de client of relatie client verblijft.
maxLengte: '35'
minLengte: '1'
naam: LDT_Organisatienaam
regels:
- RS023
- RS033
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

