---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Bedrag in 1/100 van gehanteerde valuta. Bijvoorbeeld 10000 is tienduizend eurocent,
  is 100 Euro.
maxWaarde: '99999999'
minWaarde: '0'
naam: LDT_Bedrag
regels:
- RS001
- RS005

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

