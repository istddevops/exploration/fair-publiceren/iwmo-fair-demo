---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Indicator die aangeeft welk deel van de voorafgaande datum gebruikt mag worden.
maxLengte: '1'
naam: LDT_DatumGebruik

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

