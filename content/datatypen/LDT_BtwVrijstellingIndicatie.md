---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Indicatie die aangeeft of sprake is van BTW-vrijstelling.
maxLengte: '1'
naam: LDT_BtwVrijstellingIndicatie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

