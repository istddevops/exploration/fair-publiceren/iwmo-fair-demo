---
# python/object:istd_klassen.IstdRegel
code: TR307
controleniveau: berichtoverstijgend
retourcode: '9307'
titel: 'TR307: Begindatum in de Prestatie moet groter dan of gelijk zijn aan Ingangsdatum
  in het ToegewezenProduct.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

