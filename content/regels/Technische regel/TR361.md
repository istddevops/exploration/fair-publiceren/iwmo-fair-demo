---
# python/object:istd_klassen.IstdRegel
code: TR361
controleniveau: berichtoverstijgend
documentatie: 'Voor producten die zijn opgenomen in de berichtklasse TeWijzigenProduct
  of OngewijzigdProduct moet op basis van het ToewijzingNummer de overlap op de aangevraagde
  producten gecontroleerd worden.Indien de productcode niet is meegegeven dan geldt
  deze technische regel op niveau van productcategorie. Indien productcategorie leeg
  is, dan mag dit met geen enkel ander aangevraagd product overlappen.


  Echter voor Producten uit ProductCategorie 11, 12, 13 en 14 mogen zorgperiodes overlappen,
  ook op niveau van categorie. Indien ProductCategorie leeg is, mag deze toewijzing
  alleen overlappen met toewijzingen met categorie 11, 12, 13 en 14.'
retourcode: '9361'
titel: 'TR361: Een Product mag alleen vaker in verzoek om wijziging voorkomen als
  de zorgperiodes elkaar niet overlappen, of indien sprake is van hulpmiddelen.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

