---
# python/object:istd_klassen.IstdRegel
code: TR019
controleniveau: berichtoverstijgend
retourcode: '9019'
titel: 'TR019: Bij een output- of inspanningsgerichte werkwijze moet de melding van
  de start of de stop van de ondersteuning gerelateerd zijn aan een toewijzing op
  basis van het toewijzingnummer'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

