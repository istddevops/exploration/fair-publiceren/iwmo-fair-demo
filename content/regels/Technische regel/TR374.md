---
# python/object:istd_klassen.IstdRegel
code: TR374
controleniveau: berichtoverstijgend
documentatie: Een verzoek is in behandeling todat er ofwel een antwoordbericht met
  VerzoekAntwoord "Verzoek afgewezen" ofwel een toewijzingsbericht met dezelfde ReferentieAanbieder
  gestuurd is.
retourcode: '9374'
titel: 'TR374: Een verzoek mag alleen worden verstuurd als er geen onderhanden verzoek
  is voor de betreffende client van dezelfde aanbieder.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

