---
# python/object:istd_klassen.IstdRegel
code: TR376
controleniveau: berichtoverstijgend
documentatie: "Bij iedere goed of (deels) afgekeurde Declaratie is het Declaratie-antwoord\
  \ gerelateerd aan een ingediende declaratie o.b.v. DeclaratieNummer. Een goed of\
  \ (deels) afgekeurde declaratie wordt middels een Declaratie-antwoordbericht verstuurd\
  \ en is een functionele/inhoudelijke reactie. \n"
titel: 'TR376: Het DeclaratieNummer dient overeen te komen met het DeclaratieNummer
  van een eerder ontvangen Declaratie.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

