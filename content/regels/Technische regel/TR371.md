---
# python/object:istd_klassen.IstdRegel
code: TR371
controleniveau: berichtoverstijgend
titel: 'TR371: Waarde moet overeenkomen met waarde van XsdVersie uit het declaratiebericht
  dat gerelateerd is op basis van DeclaratieIdentificatie.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

