---
# python/object:istd_klassen.IstdRegel
code: TR319
controleniveau: standaard
retourcode: '9319'
titel: 'TR319: Een declaratie- of factuurbericht bevat alleen prestaties waarvan de
  ProductPeriode valt binnen de huidige, of een voorgaande declaratie- of factuurperiode.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

