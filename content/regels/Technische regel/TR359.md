---
# python/object:istd_klassen.IstdRegel
code: TR359
controleniveau: berichtoverstijgend
retourcode: '9359'
titel: 'TR359: Als een antwoordbericht wordt verstuurd, mag er niet al eerder een
  toewijzingsbericht gestuurd zijn met dezelfde ReferentieAanbieder'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

