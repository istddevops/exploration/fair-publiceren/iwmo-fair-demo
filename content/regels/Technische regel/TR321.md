---
# python/object:istd_klassen.IstdRegel
code: TR321
controleniveau: berichtoverstijgend
documentatie: Hierbij moet rekening worden gehouden met de waarden van Eenheid en
  Frequentie in het ToegewezenProduct enerzijds en de waarde van Eenheid en de duur
  van de DeclaratiePeriode in de Prestatie anderzijds.
retourcode: '9321'
titel: 'TR321: Indien in het ToegewezenProduct een Omvang is meegegeven, moet GeleverdVolume
  in de Prestatie passen binnen Volume in het ToegewezenProduct.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

