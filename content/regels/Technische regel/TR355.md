---
# python/object:istd_klassen.IstdRegel
code: TR355
controleniveau: berichtoverstijgend
documentatie: ReferentieAanbieder in het antwoordbericht moet worden gevuld met het
  ReferentieAanbieder van het oorspronkelijke verzoek om wijziging of verzoek om toewijzing.
retourcode: '9355'
titel: 'TR355: ReferentieAanbieder in het antwoordbericht komt voor in een eerder
  verzoek om toewijzing of verzoek om wijziging'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

