---
# python/object:istd_klassen.IstdRegel
code: IV034
documentatie: "De waarde voor de elementen BasisschemaXsdVersie en BerichtXsdVersie\
  \ in het datatype CDT_XsdVersie moeten overgenomen worden uit de schemadefinitie\
  \ (XSD) waarop het bericht gecre\xEBerd/gebaseerd is. Deze waarden staan in de schemadefinitie\
  \ respectievelijk in /xs:schema/xs:annotation/xs:appinfo/<namespace>:BasisschemaXsdVersie\
  \ en /xs:schema/xs:annotation/xs:appinfo/<namespace>:BerichtXsdVersie. \nVoor \"\
  <namespace>\" wordt de namespace van de desbetreffende  iStandaard ingevuld, bijv.\
  \ 'iWlz', 'iWmo', enz."
titel: 'IV034: Hoe moet XsdVersie gevuld worden?'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

