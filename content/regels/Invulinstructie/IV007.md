---
# python/object:istd_klassen.IstdRegel
code: IV007
documentatie: "Van een client of relatie worden de achternaam, voorvoegsel en voornamen\
  \ en/of voorletters gescheiden vastgelegd. Voor het vastleggen van de VolledigeNaam\
  \ van een client geldt het volgende format: \n* De Geslachtsnaam wordt altijd vastgelegd.\
  \ Deze bestaat uit de Naam en eventueel een Voorvoegsel; \n* De Partnernaam kan\
  \ worden vastgelegd. Ook deze bestaat uit de Naam en eventueel een Voorvoegsel;\
  \ \n* Voornamen kunnen worden vastgelegd, gescheiden door spaties; \n* Voorletters\
  \ kunnen worden vastgelegd, aaneengesloten, zonder punten of spaties; \n* NaamGebruik\
  \ geeft de gewenste aanspreekvorm aan. Hiermee wordt bij correspondentie de volgorde\
  \ bepaald in het gebruik van de geslachtsnaam en de naam van de partner. Het vullen\
  \ van NaamGebruik hangt dus af van hoe de client of relatie zijn/haar naam hanteert."
titel: 'IV007: Hoe wordt de naam van een client of relatie vastgelegd?`'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

