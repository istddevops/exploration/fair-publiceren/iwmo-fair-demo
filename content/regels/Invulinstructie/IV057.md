---
# python/object:istd_klassen.IstdRegel
code: IV057
documentatie: 'BerekendBedrag wordt alleen gevuld indien ProductTarief gevuld is.

  ProductTarief is niet gevuld als Eenheid in de Prestatie 83 (Euros) is. In alle
  andere gevallen is ProductTarief wel gevuld.

  Indien BerekendBedrag gevuld moet worden, wordt deze berekend door GeleverdVolume
  in de Prestatie te vermenigvuldigen met ProductTarief in de Prestatie.


  Wanneer de gemeente een ingediende prestatie op een declaratie gedeeltelijk toekent,
  vult de gemeente in het retourbericht ook GemeenteBerekendBedrag in. Dit is het
  bedrag dat volgens de gemeente het berekende bedrag is. De gemeente maakt hierbij
  gebruik van de contractafspraken die met de aanbieder zijn gemaakt (tarief per eenheid,
  maximaal te leveren volume etc.). Het verschil tussen BerekendBedrag in de ingediende
  Prestatie en GemeenteBerekendBedrag in de Prestatie in het retourbericht stelt de
  aanbieder in staat de gegevens in de eigen administratie aan te passen, bijvoorbeeld
  het te gebruiken tarief.

  Bij een factuur is het niet mogelijk om een prestatie gedeeltelijk toe te kennen.


  Wanneer het ingediende bedrag op een prestatie in een declaratie of factuurbericht
  volledig wordt afgewezen, wordt GemeenteBerekendBedrag gevuld met 0 (zie IV056).

  '
titel: 'IV057: Hoe moet BerekendBedrag in de Prestatie gevuld worden?'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

