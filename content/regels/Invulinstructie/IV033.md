---
# python/object:istd_klassen.IstdRegel
code: IV033
documentatie: 'Wanneer de ontvanger fouten constateert in een bericht op basis van
  de ter beschikking gestelde XSLTs, wordt in het retourbericht aangegeven welke XSLT-versie
  gebruikt is voor de controle. Dit versienummer is opgenomen in de output van de
  XSLTs en dient overgenomen te worden in het retourbericht.

  '
titel: 'IV033: Hoe moet XsltVersie gevuld worden?'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

