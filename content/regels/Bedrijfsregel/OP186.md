---
# python/object:istd_klassen.IstdRegel
code: OP186
documentatie: "Deze regel is van toepassing op di\xE9 situaties waarin de aanbieder\
  \ niet had kunnen weten dat hij geen ondersteuning meer mocht leveren."
titel: 'OP186: Het beeindigen van een toewijzing op een datum die in het verleden
  ligt kan alleen na overleg met de betreffende aanbieder.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

