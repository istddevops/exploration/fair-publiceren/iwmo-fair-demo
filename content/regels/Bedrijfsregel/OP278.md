---
# python/object:istd_klassen.IstdRegel
code: OP278
documentatie: Dit betekent dat een aanbieder naar dezelfde gemeente ofwel alle geleverde
  zorg of ondersteuning declareert ofwel alles factureert. Dit kan niet per product
  verschillen.
titel: "OP278: Een gemeente en aanbieder maken \xE9\xE9n afspraak om alle zorg en\
  \ ondersteuning te declareren of te factureren."
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

