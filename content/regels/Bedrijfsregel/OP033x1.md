---
# python/object:istd_klassen.IstdRegel
code: OP033x1
documentatie: "De volgende wijzigingen in de einddatum zijn toegestaan:\n\n* Intrekken:\
  \ aanpassen van de gewenste einddatum toewijzing zodat de periode korter wordt (NB\
  \ dit is alleen toegestaan in overleg met de aanbieder). \n* Oprekken: aanpassen\
  \ van de gewenste einddatum toewijzing zodat de periode langer wordt (NB dit is\
  \ alleen toegestaan in overleg met de aanbieder).\n* Corrigeren: de einddatum van\
  \ de toewijzing wordt gewijzigd zodat deze gelijk is aan de ingangsdatum van de\
  \ toewijzing. Daarbij wordt RedenWijziging gevuld met 01 (Administratieve correctie).\
  \ Een administratieve correctie betekent dat de toewijzing als niet verzonden beschouwd\
  \ moet worden. Deze toewijzingen worden niet gezien als actuele toewijzing.\n\n\
  De volgende wijzigingen in volume en maximaal budget zijn toegestaan:\n* Volume\
  \ wijzigen: Als de omvang is gespecificeerd met in Frequentie de waarde \"Totaal\
  \ binnen geldigheidsduur toewijzing\", dan mag Volume worden aangepast.  \nIndien\
  \ dit op verzoek van de aanbieder (via een verzoek om wijziging) is, mag dit zonder\
  \ overleg worden aangepast door de gemeente.  \nIndien de gemeente het initiatief\
  \ tot aanpassing van het volume neemt, mag dit alleen in overleg met en na instemming\
  \ van de aanbieder worden aangepast. \n\n* Budget wijzigen: Als Budget op verzoek\
  \ van de aanbieder (via een verzoek om wijziging) wordt gewijzigd, mag dit zonder\
  \ overleg worden aangepast door de gemeente.    \nIndien de gemeente het initiatief\
  \ tot aanpassing van het maximale budget neemt, mag dit alleen in overleg met en\
  \ na instemming van de aanbieder worden aangepast. \n\nDe meest recente toewijzing\
  \ op toewijzingsdatum en -tijd is bepalend voor de inhoud van de toewijzing. "
titel: 'OP033x1: Een afgegeven toewijzing mag gewijzigd worden waarbij de wijziging
  met een nieuw bericht moet worden doorgegeven.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

