---
# python/object:istd_klassen.IstdRegel
code: OP271
titel: 'OP271: De aanbieder verzendt een startbericht binnen vijf werkdagen na de
  daadwerkelijke datum waarop de ondersteuning gestart is of, indien de ondersteuning
  met terugwerkende kracht is toegewezen, binnen vijf werkdagen na ontvangst van het
  toewijzingbericht.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

