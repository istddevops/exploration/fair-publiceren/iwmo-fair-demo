---
# python/object:istd_klassen.IstdRegel
code: OP285
documentatie: Dit in het geval van een administratieve eenheid die voor meerdere instellingen
  declaraties/facturen indient.
titel: 'OP285: Per declarant kunnen meerdere berichten voorkomen met dezelfde declaratie-
  of factuurperiode.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

