---
# python/object:istd_klassen.IstdRegel
code: OP280
documentatie: Een declaratie- of factuurbericht over een periode kan prestaties bevatten
  uit eerdere declaratie-of factuurperioden.
titel: "OP280: Een declaratie- of factuurbericht heeft betrekking op \xE9\xE9n declaratie-\
  \ of factuurperiode."
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

