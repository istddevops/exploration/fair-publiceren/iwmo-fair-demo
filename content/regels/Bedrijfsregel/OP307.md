---
# python/object:istd_klassen.IstdRegel
code: OP307
documentatie: "Een verzoek om toewijzing bericht wordt altijd verstuurd op basis van\
  \ een (wettelijke) verwijzing of een open beschikking. \nVoor het product en de\
  \ toewijzingsperiode dat met het verzoek wordt aangevraagd is er nog geen toewijzing\
  \ voor dat product. \nEen verzoek om toewijzing bericht kan niet gebruikt worden\
  \ als er al een actuele toewijzing is voor dat product voor die periode."
titel: "OP307: Een verzoek om toewijzing bericht wordt alleen gebruikt indien een\
  \ cli\xEBnt zich met een (wettelijke) verwijzing of een open beschikking meldt bij\
  \ de zorgaanbieder"
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

