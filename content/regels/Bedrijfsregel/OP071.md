---
# python/object:istd_klassen.IstdRegel
code: OP071
documentatie: Dit nummer mag niet gewijzigd worden.
titel: 'OP071: Elke relatie krijgt een uniek nummer per gemeente per client.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

