---
# python/object:istd_klassen.IstdRegel
code: OP090x3
documentatie: De verzendende partij van het heenbericht is verantwoordelijk voor het
  signaleren van het ontbreken van een retourbericht en dient actie te ondernemen.
titel: 'OP090x3: Voor ieder ontvangen declaratie- /factuurbericht wordt binnen twintig
  werkdagen na ontvangst een retourbericht verzonden.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

