---
# python/object:istd_klassen.IstdRegel
code: OP354
titel: 'OP354: Zorg of ondersteuning geleverd aan een client tijdens een bepaalde
  declaratieperiode wordt direct in de erop volgende declaratieperiode of achteraf
  in een latere declaratieperiode gedeclareerd.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

