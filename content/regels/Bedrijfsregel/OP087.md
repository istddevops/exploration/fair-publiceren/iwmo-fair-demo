---
# python/object:istd_klassen.IstdRegel
code: OP087
documentatie: "Deze regel is van toepassing indien voor 1 client  meerdere producten\
  \ toegewezen zijn. Alle geldige toewijzingen binnen 1 client, voor 1 aanbieder worden\
  \ in 1 toewijzingsbericht geplaatst en naar de betreffende aanbieder gestuurd inclusief\
  \ de toewijzingen die gewijzigd zijn sinds het laatst verstuurde toewijzingsbericht\
  \ voor deze client en aanbieder. Denk hierbij aan intrekkingen die zijn gedaan en\
  \ waar de aanbieder nog niet van op de hoogte is.   \n\nVoor iedere client waarbij\
  \ een toewijzing verandert, wordt een toewijzingsbericht gestuurd voor alle actuele\
  \ toewijzingen voor die aanbieder behorende bij die client ."
titel: 'OP087: Een toewijzingsbericht bevat voor 1 client altijd alle toewijzingen
  voor 1 aanbieder die op of na de aanmaakdatum van het bericht geldig zijn plus alle
  toewijzingen die gewijzigd zijn ten opzichte van het voorgaande toewijzingsbericht.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

