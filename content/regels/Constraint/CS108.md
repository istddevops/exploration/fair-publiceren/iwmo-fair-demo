---
# python/object:istd_klassen.IstdRegel
code: CS108
retourcode: '0001'
titel: "CS108: Vullen met een waarde die groter is dan, of gelijk is aan de Begindatum\
  \ van de aangeduide periode \xE9n die niet groter is dan de Dagtekening van het\
  \ bericht."
type: Constraint

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

