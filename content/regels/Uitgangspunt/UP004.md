---
# python/object:istd_klassen.IstdRegel
code: UP004
titel: 'UP004: De gemeente waar de client is ingeschreven in de BRP coordineert de
  inzet van Wmo ondersteuning voor de client door middel van toewijzingen (inspannings-
  en outputgericht).'
type: Uitgangspunt

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

