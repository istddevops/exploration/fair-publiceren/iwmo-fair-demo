---
# python/object:istd_klassen.IstdRegel
code: CD066
retourcode: '0001'
titel: 'CD066: Als type verwijzer gelijk is aan 02, 03, 04 of 05 en ZorgverlenerCode
  is gevuld, dan is Naamverwijzer leeg.'
type: Conditie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

