---
# python/object:istd_klassen.IstdRegel
code: CD055
retourcode: '0001'
titel: 'CD055: Verplicht vullen indien BtwVrijstellingIndicatie de waarde 2 (Geen
  btw-vrijstelling) bevat.'
type: Conditie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

