---
# python/object:istd_klassen.IstdBericht
basisschemaXsdMaxVersie: 1.2.0
basisschemaXsdMinVersie: 1.0.0
basisschemaXsdVersie: 1.2.0
berichtXsdMaxVersie: 1.0.2
berichtXsdMinVersie: 1.0.0
berichtXsdVersie: 1.0.2
beschrijving:
- Bericht voor antwoordinformatie over het Verzoek om toewijzing of Verzoek om wijziging
  Wmo-hulp.
klassen:
- # python/object:istd_klassen.IstdKlasse
  beschrijving: (geen beschrijving gevonden)
  elementen:
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Code ter identificatie van een soort bericht.
    dataType: LDT_BerichtCode
    naam: BerichtCode
    waarde: '482'
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Volgnummer van de formele uitgifte van een major release van een iStandaard.
    dataType: LDT_BerichtVersie
    naam: BerichtVersie
    regels:
    - CS025
    waarde: '3'
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Volgnummer van de formele uitgifte van een minor release van een iStandaard.
    dataType: LDT_BerichtSubversie
    naam: BerichtSubversie
    regels:
    - CS015
    waarde: '0'
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of
      ondersteuning.
    dataType: LDT_Gemeente
    naam: Afzender
    regels:
    - IV046
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Identificerende code van een aanbieder van zorg of ondersteuning.
    dataType: LDT_AgbCode
    naam: Ontvanger
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Naam of nummer en dagtekening ter identificatie van een totale aanlevering.
    dataType: CDT_BerichtIdentificatie
    naam: BerichtIdentificatie
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet
      voor het opstellen van het heenbericht.
    dataType: CDT_XsdVersie
    naam: XsdVersie
  naam: Header
- # python/object:istd_klassen.IstdKlasse
  beschrijving:
  - Gegevens over het antwoord dat de gemeente geeft op een verzoek van de aanbieder.
  elementen:
  - # python/object:istd_klassen.IstdElement
    dataType: LDT_Referentie
    naam: ReferentieAanbieder
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Het antwoord op een verzoek.
    dataType: LDT_VerzoekAntwoord
    naam: VerzoekAntwoord
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - De reden waarom een verzoek van een zorgaanbieder wordt afgewezen door de gemeente.
    dataType: LDT_RedenAfwijzingVerzoek
    naam: RedenAfwijzingVerzoek
    regels:
    - CD085
    - CD086
  naam: Antwoord
  regels:
  - TR359
  - TR360
  - TR365
naam: WMO319
regels:
- OP003
- OP039
- OP047
- OP179
- OP192
- OP254
- OP274
- OP286
- OP344
- OP346

# BEGIN: Handmatige Fix https://gitlab.com/istd-dgpi/migration/iwmo-migration/-/issues/4
relaties:
- # python/object:istd_klassen.IstdAggregatieRelatie
  childKlasse: Header
  childMax: '1'
  childMin: '1'
  naam: Header
  parentKlasse: Root
- # python/object:istd_klassen.IstdAggregatieRelatie
  childKlasse: Antwoord
  childMax: '1'
  childMin: '1'
  naam: Antwoord
  parentKlasse: Root
# EIND: Handmatige Fix https://gitlab.com/istd-dgpi/migration/iwmo-migration/-/issues/4

release: '3.0'
standaard: iwmo

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

