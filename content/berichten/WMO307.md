---
# python/object:istd_klassen.IstdBericht
basisschemaXsdMaxVersie: 1.2.0
basisschemaXsdMinVersie: 1.0.0
basisschemaXsdVersie: 1.2.0
berichtXsdMaxVersie: 1.0.2
berichtXsdMinVersie: 1.0.0
berichtXsdVersie: 1.0.2
beschrijving:
- Bericht voor het melden van de stop van levering van Wmo-ondersteuning.
klassen:
- # python/object:istd_klassen.IstdKlasse
  beschrijving: (geen beschrijving gevonden)
  elementen:
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Code ter identificatie van een soort bericht.
    dataType: LDT_BerichtCode
    naam: BerichtCode
    waarde: '420'
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Volgnummer van de formele uitgifte van een major release van een iStandaard.
    dataType: LDT_BerichtVersie
    naam: BerichtVersie
    regels:
    - CS025
    waarde: '3'
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Volgnummer van de formele uitgifte van een minor release van een iStandaard.
    dataType: LDT_BerichtSubversie
    naam: BerichtSubversie
    regels:
    - CS015
    waarde: '0'
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Identificerende code van een aanbieder van zorg of ondersteuning.
    dataType: LDT_AgbCode
    naam: Afzender
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Identificatie van een gemeente die betrokken is bij de uitvoering van zorg of
      ondersteuning.
    dataType: LDT_Gemeente
    naam: Ontvanger
    regels:
    - IV046
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Naam of nummer en dagtekening ter identificatie van een totale aanlevering.
    dataType: CDT_BerichtIdentificatie
    naam: BerichtIdentificatie
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Volgnummer van de formele uitgifte van een versie van de XSD's die zijn ingezet
      voor het opstellen van het heenbericht.
    dataType: CDT_XsdVersie
    naam: XsdVersie
  naam: Header
- # python/object:istd_klassen.IstdKlasse
  beschrijving:
  - Persoonsgegevens van de client.
  elementen:
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Een door de overheid toegekend identificerend nummer in het kader van het vereenvoudigen
      van het contact tussen overheid en burgers en het verminderen van de administratieve
      lasten.
    dataType: LDT_BurgerServicenummer
    naam: Bsn
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Datum waarop een natuurlijk persoon geboren is.
    dataType: CDT_Geboortedatum
    naam: Geboortedatum
    regels:
    - CS023
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - De sekse van een persoon, zoals bij geboorte formeel vastgesteld of nadien formeel
      gewijzigd.
    dataType: LDT_Geslacht
    naam: Geslacht
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - De naam van een natuurlijk persoon, aangeduid als Geslachtsnaam en eventueel
      Voornamen en/of Voorletters.
    dataType: CDT_VerkorteNaam
    naam: Naam
  naam: Client
- # python/object:istd_klassen.IstdKlasse
  beschrijving:
  - Gegevens over het beeindigen van de levering van ondersteuning.
  elementen:
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Identificerend nummer van de opdracht om een zorg - of ondersteuningsproduct
      te leveren, zoals vastgesteld door de gemeente.
    dataType: LDT_Nummer
    naam: ToewijzingNummer
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Gecodeerde omschrijving van een product, dienst of resultaat ten behoeve van
      het leveren van ondersteuning aan een client.
    dataType: CDT_Product
    naam: Product
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - De datum waarop de toegewezen product voor de eerste keer geleverd dient te
      worden (gelijk aan de Ingangsdatum van het toegewezen product in het Toewijzingbericht).
    dataType: LDT_Datum
    naam: ToewijzingIngangsdatum
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Datum vanaf wanneer een ondersteuningsproduct wordt geleverd.
    dataType: LDT_Datum
    naam: Begindatum
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - De reden van beeindiging van de Wmo-ondersteuning bij de client.
    dataType: LDT_RedenBeeindiging
    naam: RedenBeeindiging
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - De datum waarop de Wmo-ondersteuning bij client wordt beeindigd.
    dataType: LDT_Datum
    naam: Einddatum
  - # python/object:istd_klassen.IstdElement
    beschrijving:
    - Indicatie over de status van de informatie in de berichtklasse.
    dataType: LDT_StatusAanlevering
    naam: StatusAanlevering
    regels:
    - CS058
    - TR063
    - IV008
  naam: StopProduct
  regels:
  - TR019
  - TR069
  - TR074
  - TR101
  - IV096
naam: WMO307
regels:
- OP003
- OP033
- OP033x2
- OP039
- OP047
- OP072
- OP079
- OP155
- OP179
- OP192
- OP252
- OP254
- OP270
- OP272
- OP286
- OP288
- OP289
- OP295
- IV008

# BEGIN: Handmatige Fix https://gitlab.com/istd-dgpi/migration/iwmo-migration/-/issues/4
relaties:
- # python/object:istd_klassen.IstdAggregatieRelatie
  childKlasse: Header
  childMax: '1'
  childMin: '1'
  naam: Header
  parentKlasse: Root
- # python/object:istd_klassen.IstdAggregatieRelatie
  childKlasse: Client
  childMax: '1'
  childMin: '1'
  naam: Client
  parentKlasse: Root
- # python/object:istd_klassen.IstdAggregatieRelatie
  childKlasse: StopProduct
  childMax: unbounded
  childMin: '1'
  naam: StopProducten
  parentKlasse: Client
# EIND: Handmatige Fix https://gitlab.com/istd-dgpi/migration/iwmo-migration/-/issues/4

release: '3.0'
standaard: iwmo

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

