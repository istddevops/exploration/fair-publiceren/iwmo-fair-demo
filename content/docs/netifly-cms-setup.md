# Netifly CMS Setup

How to setup **Netifly CMS** to maintain `Git-based` content.

## NPM Based Setup

| Source | Description |
|:-|:-|
| [HUGO mod npm pack](https://gohugo.io/commands/hugo_mod_npm_pack/) | HUGO CLI support for NPM Packages |
| [Netifly CMS / Docs / NPM](https://www.netlifycms.org/docs/add-to-your-site/#installing-with-npm) | Description how to setup using the NPM `netlify-cms-app` package |
| [HUGO JavaScript Building](https://gohugo.io/hugo-pipes/js/#import-js-code-from-assets) | How to use TypeScript with NPM Packages to build Plain JavaScript output |

**1. Initialize HUGO NPM Environment**

```template
hugo mod npm pack
```

> This should create the following files in the root of your project-directory:

- `package.hugo.json`
- `package.json`

**2. Install Netlify CMS Packages**

The CMS App package to use and customize

```template
npm install netlify-cms-app --save
```

The CMS proxy service package to run en test on your local machine

```template
npm install netlify-cms-proxy-server --save-dev
```

The Concurrently package to run en test HUGO Server and CMS Server togeher on your local machine

```template
npm install concurrently --save-dev
```

> This should add a `netlify-cms-app` *dependency* to your `package.json`

**3. Add TypeScript Assets for Netlify CMS App initialisation and customization**

Create a `assets\js\cms.ts` file and add the following `TypeScript` code to start.

```template
// Import Netlify CMS to take over current page
import CMS from 'netlify-cms-app'

// Initialize the CMS object
CMS.init()

```

> This `TypeScript` source file can be used to add [Custom Previews](https://www.netlifycms.org/docs/customization) and other configuration and customization for the CMS.

**4. Add HUGO CMS-section HTML-template**

> Pre: HUGO Book Theme configured

Create a `layouts/cms/list.html` and add the following `HUGO-template` scripting

```template
{{ define "main" }}
    {{ $cmsSourceTs := "js/cms.ts" }}
    {{ $cmsParams := (dict 
        "testProp" "testValue"    
    ) }}

    {{ $cmsDestJs := resources.Get $cmsSourceTs | js.Build (dict 
        "minify" false
        "params" $cmsParams
    ) }}
    <script src="{{ $cmsDestJs.Permalink }}"></script>
{{ end }}
```

> The HUGO Book Theme "main" `block` is used to render the CMS HTML here.

**5. Add CMS Configuration**

Create a CMS section content file `content/cms/_index.md` in your project directory

Create a CMS input configuration file `content/cms/config.yml` in your project directory with a contents like this ...

```template

``