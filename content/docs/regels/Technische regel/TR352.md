---
regel:
  code: TR352
  documentatie: "Een verzoek om wijziging bericht is gebaseerd op een situatie waarbij\
    \ er minstens 1 actuele toewijzing is. \nIn het bericht moet daarom minstens 1\
    \ product zitten dat ongewijzigd blijft of gewijzigd moet worden, aangezien alle\
    \ op Ingangsdatum geldige of in de toekomst geldige ToegewezenProducten voor die\
    \ betreffende aanbieder/client in het verzoek om wijziging bericht moeten worden\
    \ opgenomen"
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

