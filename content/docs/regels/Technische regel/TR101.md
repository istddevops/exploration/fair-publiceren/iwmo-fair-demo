---
regel:
  code: TR101
  documentatie: 'Dat betekent dat voor iedere berichtklasse de logische sleutel in
    combinatie met de logische sleutels van de bovenliggende berichtklassen uniek
    moet zijn.

    '
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

