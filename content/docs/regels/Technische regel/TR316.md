---
regel:
  code: TR316
  documentatie: 'VorigReferentieNummer mag geen waarde bevatten gelijk aan ReferentieNummer
    van een Prestatie in hetzelfde bestand.

    Creditering van een debet prestatie in hetzelfde bericht is niet toegestaan.'
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

