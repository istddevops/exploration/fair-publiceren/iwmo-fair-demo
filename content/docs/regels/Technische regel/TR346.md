---
regel:
  code: TR346
  documentatie: 'Indien tarief per uur is afgesproken en de declaratie in minuten
    wordt gedaan, vindt omrekening van het producttarief plaats. Indien het uurtarief
    niet deelbaar is door 60, zal het berekende producttarief per minuut niet op hele
    eurocenten uitkomen. Bij de berekening van IngediendBedrag moet dan met het onafgeronde
    producttarief worden gerekend. Zie IV081  '
  type: Technische regel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

