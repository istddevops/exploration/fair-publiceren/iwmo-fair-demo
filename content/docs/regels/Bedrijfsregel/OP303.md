---
regel:
  code: OP303
  documentatie: "De aanbieder die een declaratie-antwoordbericht ontvangt kan op basis\
    \ van de meegestuurde gegevens de reactie op de declaratie verwerken in haar systeem.\
    \ Dit is vooral van belang wanneer van een ingediend declaratiebericht een deel\
    \ van de ingediende prestaties niet wordt toegekend. In het declaratie-antwoordbericht\
    \ worden alleen  de prestaties meegestuurd die zijn afgewezen. Daarbij wordt met\
    \ een retourcode aangegeven wat de reden is dat de ingediende prestatie niet is\
    \ toegekend. \nToegekende prestaties\nToegekende prestaties worden niet opgenomen\
    \ in het retourbericht. Alleen de som van de ingediende bedragen van de toegekende\
    \ prestaties wordt in de berichtklasse DeclaratieAntwoord van het declaratie-antwoordbericht\
    \ opgenomen. "
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

