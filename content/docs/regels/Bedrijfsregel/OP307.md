---
regel:
  code: OP307
  documentatie: "Een verzoek om toewijzing bericht wordt altijd verstuurd op basis\
    \ van een (wettelijke) verwijzing of een open beschikking. \nVoor het product\
    \ en de toewijzingsperiode dat met het verzoek wordt aangevraagd is er nog geen\
    \ toewijzing voor dat product. \nEen verzoek om toewijzing bericht kan niet gebruikt\
    \ worden als er al een actuele toewijzing is voor dat product voor die periode."
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

