---
regel:
  code: OP076
  documentatie: Gegevens over een contactpersoon mogen alleen worden opgenomen indien
    noodzakelijk voor communicatie met de client.
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

