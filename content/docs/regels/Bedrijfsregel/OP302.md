---
regel:
  code: OP302
  documentatie: "De in de declaratie ingediende prestatie moet passen bij de toewijzing.\n\
    Dit betekent onder andere dat de prestatie hetzelfde \xF3f (een) nader gespecificeerd(e)\
    \ zorg- of ondersteuningsproduct bevat als de toewijzing.  \n\xB7    Bij een specifieke\
    \ toewijzing (productcategorie en productcode zijn beiden gevuld in de toewijzing)\
    \ wordt hetzelfde zorg- of ondersteuningsproduct gedeclareerd, ofwel dezelfde\
    \ combinatie van productcategorie en productcode. \n\xB7    Bij een aspecifieke\
    \ toewijzing (in de toewijzing is alleen productcategorie gevuld, productcode\
    \ is leeg) worden 1 of meer nader gespecificeerde zorg- of ondersteuningsproducten\
    \ gedeclareerd of gefactureerd. Dat betekent dat de aanbieder binnen de toegewezen\
    \ productcategorie 1 of meer productcode(s) declareert die volgens de gehanteerde\
    \ productcodelijst horen bij die productcategorie.\n\xB7    Bij een generieke\
    \ toewijzing (in de toewijzing is alleen het budget gevuld, productcategorie en\
    \ productcode zijn leeg), declareert de aanbieder 1  of meer productcategorie(en)\
    \ met bijpassende productcode(s) passend binnen het afgesproken contract met de\
    \ gemeente.  \n\nDaarnaast dient het geleverd volume in de prestatie passend te\
    \ zijn binnen de toegewezen omvang of het toegewezen budget.\n"
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

