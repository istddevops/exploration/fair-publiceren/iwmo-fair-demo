---
regel:
  code: OP265
  documentatie: Dit betekent dat een aanbieder altijd dezelfde declaratie-/factuurperiode
    hanteert naar dezelfde gemeente. Dit kan niet per product verschillen.
  type: Bedrijfsregel

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

