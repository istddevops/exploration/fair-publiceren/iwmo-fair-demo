---
regel:
  code: CS050
  documentatie: 'Als Partnernaam gevuld is, wordt in NaamGebruik aangegeven hoe de
    persoon zijn naam wenst te gebruiken. Hiervoor kunnen de volgende waarden gebruikt
    worden:

    1 (eigen naam), 2 (naam echtgenoot of geregistreerd partner of alternatieve naam),
    3 (naam echtgenoot of geregistreerd partner gevolgd door eigen naam) of 4 (eigen
    naam gevolgd door naam echtgenoot of geregistreerd partner). Indien geen Partnernaam
    gevuld is, wordt verplicht 1 (eigen naam) gevuld.'
  type: Constraint

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

