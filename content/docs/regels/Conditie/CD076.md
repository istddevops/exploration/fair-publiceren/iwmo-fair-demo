---
regel:
  code: CD076
  documentatie: 'Om te kunnen berekenen of bij declaratie een prestatie binnen de
    totale omvang van de toewijzing valt is het van belang de einddatum van de toewijzing
    te hebben.

    Dit heeft dus te maken met de omrekening van wekelijks toewijzen naar maandelijks
    declareren en de controle op volume van de declaraties ten opzichte van de toewijzing.'
  type: Conditie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

