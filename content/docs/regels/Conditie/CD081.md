---
regel:
  code: CD081
  documentatie: Indien eenheid de waarde 14 (etmaal), 16 (dagdeel), 83 (euro's) of
    84 (stuks (inspanning)) heeft en Einddatum van het ToegewezenProduct > 31-12-2020
    of leeg is, dan Frequentie vullen met de waarde 2 (Per week), 4 (Per maand) of
    6 (Totaal binnen geldigheidsduur toewijzing)
  type: Conditie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

