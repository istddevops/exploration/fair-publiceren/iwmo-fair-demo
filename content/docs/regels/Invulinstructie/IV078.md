---
regel:
  code: IV078
  documentatie: "Alleen voor de inspanningsgerichte uitvoeringsvariant is een uniforme\
    \ werkwijze geformuleerd. Voor de outputgerichte uitvoeringsvariant is het aan\
    \ de gemeente om gezamenlijk met de aanbieder te komen tot een afspraak hierover.\
    \ \n\nWerkwijze voor inspanningsgerichte uitvoeringsvariant, het in de gebroken\
    \ periode daadwerkelijk geleverde volume wordt gedeclareerd. Met als bovengrens\
    \ voor deze gebroken periode het maximale volume, dat in ToegewezenProduct aan\
    \ de frequentie is toegekend. \n\nVoorbeeld 1:\nfrequentie in ToegewezenProduct\
    \ = week\nToewijzing voor 3 uur per week met ingangsdatum 12-10-2021 en einddatum\
    \ 13-12-2021\nVoor de  productperiode 12-10-2021 t/m 31-10-2021 mag maximaal 9\
    \ uur gedeclareerd worden, indien deze uren ook daadwerkelijk geleverd zijn. (zie\
    \ ook rekenmethode IV077 De eerste week (12-10-2021 t/m 17-10-2021) is geen volledige\
    \ kalenderweek, maar indien geleverd mag voor deze week de volledige 3 uur worden\
    \ gedeclareerd. \n\nVoorbeeld 2:\nFrequentie in ToegewezenProduct = maand\nToewijzing\
    \ voor 15 uur per maand met ingangsdatum 12-10-2021 en einddatum 13-12-2021\n\
    Voor de productperiode 12-10-2021 t/m 31-10-2021 mag maximaal 15 uur gedeclareerd\
    \ worden, indien deze uren ook daadwerkelijk geleverd zijn. Ondanks dat de ingangsdatum\
    \ 12-10-2021 is en er dus geen hele kalendermaand is toegewezen, mag (mits geleverd)\
    \ de volledige 15 uur gedeclareerd worden.\n"
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

