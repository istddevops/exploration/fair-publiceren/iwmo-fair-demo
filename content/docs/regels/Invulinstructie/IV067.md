---
regel:
  code: IV067
  documentatie: 'Welke organisatie-identificatie gegevens gevuld moeten worden in
    het declaratie-/factuurbericht is afhankelijk van of er gebruik gemaakt wordt
    van een servicebureau.


    Geen inzet servicebureau:

    Alleen Aanbieder en Gemeente zijn gevuld. Servicebureau is leeg.


    Wel inzet servicebureau:

    Servicebureau, Aanbieder en Gemeente zijn gevuld.'
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

