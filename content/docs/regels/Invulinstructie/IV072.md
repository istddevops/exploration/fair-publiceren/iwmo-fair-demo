---
regel:
  code: IV072
  documentatie: "Als sprake is van elkaar opvolgende ToegewezenProducten binnen een\
    \ declaratie-/factuurperiode, dan volgen aparte Prestaties per ToegewezenProduct,\
    \ per product in de declaratie/factuur over die declaratie-/factuurperiode. Dit\
    \ geldt zowel voor de methode werkelijke productperiodes (zie IV062) als ook voor\
    \ de methode som over declaratie-/factuurperiode (zie IV063). Een Prestatie kan\
    \ dus nooit op meer dan \xE9\xE9n ToegewezenProduct betrekking hebben.\n\nVoorbeeld\n\
    In onderstaande voorbeeld wordt uitgegaan van de volgende situatie:\nEr wordt\
    \ per maand gedeclareerd/gefactureerd\nHet eerste ToegewezenProduct heeft Ingangsdatum\
    \ 01-03-2019 en Einddatum 09-03-2019\nHet opvolgende ToegewezenProduct heeft Ingangsdatum\
    \ 23-03-2019 en Einddatum 31-03-2019\nEr is ononderbroken zorg geleverd gedurende\
    \ beide toegewezen periodes\n\nEr worden nu 2 Prestaties ingediend met de volgende\
    \ ProductPeriodes:\nProductPeriode Prestatie 1: 01-03-2018 t/m 09-03-2018\nProductPeriode\
    \ Prestatie 2: 23-03-2018 t/m 31-03-2018"
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

