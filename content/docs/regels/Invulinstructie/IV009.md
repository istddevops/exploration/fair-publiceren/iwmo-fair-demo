---
regel:
  code: IV009
  documentatie: "Wanneer de Geboortedatum niet volledig of onbekend is, dan wordt\
    \ het deel dat wel bekend is gebruikt en wordt voor de overige delen de waarde\
    \ 01 (dag en maand) of 1900 (jaar) gebruikt. Het element DatumGebruik geeft aan\
    \ welk deel van de datum bekend is en dus te gebruiken. \n\nVoorbeelden: \n* Een\
    \ volledig onbekende geboortedatum wordt 01-01-1900 \n* Is alleen bekend dat de\
    \ geboorte in 1953 was, dan wordt de geboortedatum 01-01-1953 \n* Is alleen bekend\
    \ dat de geboorte in september 1949 was, dan wordt de geboortedatum 01-09-1949"
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

