---
regel:
  code: IV076
  documentatie: "De productperiode binnen een declaratieperiode is altijd maximaal\
    \ een kalendermaand. Als ToegewezenProduct de frequentie per week heeft, is het\
    \ mogelijk dat de week verdeeld is over meerdere productperiodes (kalendermaanden).\
    \ Bijvoorbeeld wanneer halverwege een week een nieuwe maand begint. De datum van\
    \ levering bepaalt in welke productperiode de leveringen worden ingediend. Voor\
    \ het bepalen van de juiste productperiode zijn er twee methoden (voor inspanningsgerichte\
    \ toewijzing en voor outputgerichte toewijzing).\n\nMethode bij Inspanningsgerichte\
    \ toewijzing\nDe productperiode bevat alleen leveringen van dagen die vallen in\
    \ de betreffende kalendermaand. \n\nVoorbeeld:\nLevering week 13-2021 = maandag\
    \ 29-03-2021 t/m zondag 04-04-2021\n\nDeze levering valt uiteen in 2 productperiodes.\
    \ \n* productperiode 1, waarin de levering zit van week 13 tot maximaal 31-03-2021,\n\
    * productperiode 2, waarin de levering zit van week 13 vanaf 01-04-2021.\n\nAfhankelijk\
    \ van het moment van declareren, zullen de periodes in 1 of meerdere declaratieberichten\
    \ zitten.\n\nMethode bij Outputgerichte toewijzing\nDe productperiode bevat leveringen\
    \ van de week waarvan de zondag valt in de kalendermaand \n\nVoorbeeld:\nLevering\
    \ week 13-2021 = maandag 29-03-2021 t/m zondag 04-04-2021\n\nDe zondag van week\
    \ 13 valt op 04-04-2021, dus de levering van week 13 valt in kalendermaand April.\
    \ Alle leveringen van week 13 worden opgenomen in de kalendermaand April.\n"
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

