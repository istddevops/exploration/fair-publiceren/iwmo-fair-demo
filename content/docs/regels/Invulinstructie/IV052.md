---
regel:
  code: IV052
  documentatie: "De aanbieder dient het beschikkingnummer mee te geven in het Verzoek\
    \ om Toewijzing bericht indien de client van haar gemeente een beschikking heeft\
    \ gekregen. \n\nDe gemeente kan op basis van het beschikkingnummmer eenvoudig\
    \ de bestaande beschikking koppelen aan de informatie uit het Verzoek Om Toewijzing\
    \ bericht."
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

