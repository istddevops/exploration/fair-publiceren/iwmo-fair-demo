---
regel:
  code: IV081
  documentatie: "De eenheid in de prestatie moet gelijk zijn aan de eenheid van de\
    \ toewijzing. Voor die eenheid dient in het contract een tarief voor het betreffende\
    \ product te zijn vastgelegd. Het producttarief in de prestatie wordt gevuld met\
    \ dat betreffende tarief.\n\nAlleen voor uren en minuten is er een uitzondering\
    \ mogelijk. Gemeente en aanbieder kunnen een tariefeenheid per uur afgesproken\
    \ hebben, terwijl gedeclareerd wordt in minuten. In dat geval wordt in de Prestatie\
    \ het ProductTarief omgerekend naar een tarief per minuut (tarief delen door 60\
    \ minuten) en afgerond op 2 decimalen volgens de rekenkundige regels. Dit betekent\
    \ dat 1,455 wordt afgerond naar 1,46 en dat 1,454 wordt afgerond naar 1,45. \n\
    Dit (afgeronde) tarief wordt in de Prestatie bij ProductTarief opgenomen, maar\
    \ niet gebruikt voor de berekening van IngediendBedrag.Voor de berekening van\
    \ het IngediendBedrag wordt het niet afgeronde tarief gebruikt. De afronding (op\
    \ 2 decimalen) van het IngediendeBedrag vindt pas helemaal aan het eind van de\
    \ berekening plaats.(TR346)\n"
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

