---
regel:
  code: IV030
  documentatie: "Om vast te kunnen stellen welke toewijzing het recentst is, zijn\
    \ Toewijzingsdatum en Toewijzingstijd in het toewijzingbericht opgenomen. De gemeente\
    \ vult deze velden met de datum en de tijd waarop de toewijzing definitief is\
    \ vastgesteld. Het gaat hier om het moment waarop de gemeente de toewijzing vaststelt\
    \ en niet om het moment van verzending van het bericht. De toewijzing met de meest\
    \ recente Toewijzingsdatum en Toewijzingstijd is de actuele toewijzing. \nAls\
    \ een aanbieder de gemeente verzoekt om een bestaande toewijzing nog een keer\
    \ te versturen, blijft de inhoud van Toewijzingsdatum en Toewijzingstijd onveranderd.\
    \ \nEen aanpassing van het volume of de einddatum in de toewijzing moet beschouwd\
    \ worden als een gewijzigde toewijzing. De gemeente vult Toewijzingsdatum en Toewijzingstijd\
    \ met de datum en tijd waarop de gemeente de aanpassing vaststelt. "
  type: Invulinstructie

---


** Generereerd door `create_hugo_content.py` op 16 March, 2022**

